﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;


namespace OrendaWebAPI.Models
{
    public class VideosV2Model
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrendaDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        internal static int AddVideosFromYouTubeV2(List<YouTubeChannelDetails> youtube)
        {
            var result = 0;
            try
            {
                foreach (var uTube in youtube)
                {
                    SqlConnection sqlConnection = ConnectTODB();
                    SqlCommand sqlCommand = new SqlCommand("[dbo].[AddCustomAppVideos]", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    SqlParameter vID = sqlCommand.Parameters.Add("@VideoId", System.Data.SqlDbType.VarChar, 100);
                    vID.Direction = System.Data.ParameterDirection.Input;
                    vID.Value = uTube.VideoId.Trim();
                    SqlParameter vTitle = sqlCommand.Parameters.Add("@VideoTitle", System.Data.SqlDbType.VarChar, 100);
                    vTitle.Direction = System.Data.ParameterDirection.Input;
                    vTitle.Value = uTube.VideoTitle.Trim();
                    SqlParameter vDescription = sqlCommand.Parameters.Add("@VideoDescription", System.Data.SqlDbType.VarChar, 1024);
                    vDescription.Direction = System.Data.ParameterDirection.Input;
                    vDescription.Value = uTube.VideoDescription.Trim();
                    SqlParameter vURL = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
                    vURL.Direction = System.Data.ParameterDirection.Input;
                    vURL.Value = uTube.VideoUrl.Trim();
                    SqlParameter vThumbNail = sqlCommand.Parameters.Add("@VideoThumbNail", System.Data.SqlDbType.VarChar, 1024);
                    vThumbNail.Direction = System.Data.ParameterDirection.Input;
                    vThumbNail.Value = uTube.ThumbNailUrl.Trim();
                    SqlParameter pDate = sqlCommand.Parameters.Add("@PublishedDate", System.Data.SqlDbType.DateTime);
                    pDate.Direction = System.Data.ParameterDirection.Input;
                    pDate.Value = uTube.PublishedDate;
                    SqlParameter channelId = sqlCommand.Parameters.Add("@ChannelId", System.Data.SqlDbType.VarChar, 256);
                    channelId.Direction = System.Data.ParameterDirection.Input;
                    channelId.Value = uTube.ChannelId.Trim();
                    SqlParameter channelDesc = sqlCommand.Parameters.Add("@ChannelDescription", System.Data.SqlDbType.VarChar, 1024);
                    channelDesc.Direction = System.Data.ParameterDirection.Input;
                    channelDesc.Value = uTube.ChannelTitle.Trim();
                    SqlParameter viewcount = sqlCommand.Parameters.Add("@ViewsCount", System.Data.SqlDbType.Int);
                    viewcount.Direction = System.Data.ParameterDirection.Input;
                    viewcount.Value = Convert.ToInt32(uTube.Views);
                    SqlParameter Likes = sqlCommand.Parameters.Add("@LikesCount", System.Data.SqlDbType.Int);
                    Likes.Direction = System.Data.ParameterDirection.Input;
                    Likes.Value = uTube.LikesCount;
                    SqlParameter disLikes = sqlCommand.Parameters.Add("@DisLikesCount", System.Data.SqlDbType.Int);
                    disLikes.Direction = System.Data.ParameterDirection.Input;
                    disLikes.Value = uTube.DisLikesCount;
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader != null && sqlDataReader.Read())
                    {
                        result = sqlDataReader["result"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);
                    }
                    sqlDataReader.Close();
                    sqlConnection.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<YouTubeChannelDetails> GetCustomAppVideos()
        {
            List<YouTubeChannelDetails> youtube = new List<YouTubeChannelDetails>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetCustomAppVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    YouTubeChannelDetails uTube = new YouTubeChannelDetails();
                    uTube.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    uTube.VideoId = sqlDataReader["VideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoId"]);
                    uTube.VideoTitle = sqlDataReader["VideoTitle"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoTitle"]);
                    uTube.VideoDescription = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    uTube.ChannelId = sqlDataReader["ChannelId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ChannelId"]);
                    uTube.ChannelTitle = sqlDataReader["ChannelTitle"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ChannelTitle"]);
                    uTube.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    uTube.ThumbNailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    uTube.PublishedDate = sqlDataReader["PublishedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    uTube.LikesCount = sqlDataReader["LikesCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["LikesCount"]);
                    uTube.DisLikesCount = sqlDataReader["DisLikesCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisLikesCount"]);
                    RootObject statistics = new RootObject();
                    string url = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + uTube.VideoId + "&key=AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo";
                    JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                    serializer2.MaxJsonLength = 1000000000;
                    HttpClient client1 = new HttpClient();
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response1 = client1.GetAsync(url).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        Stream st2 = response1.Content.ReadAsStreamAsync().Result;
                        StreamReader reader2 = new StreamReader(st2);
                        string content1 = reader2.ReadToEnd();
                        statistics = JsonDeserialize<RootObject>(content1);
                    }
                    if (statistics.items.Count > 0)
                    {
                        uTube.Views = Convert.ToString(statistics.items[0].statistics.viewCount) + " Views";
                    }
                    else
                    {
                        uTube.Views = string.Empty;
                    }
                    youtube.Add(uTube);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return youtube;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        internal static FirebaseDTO GetFirebaseCredentials(int AppId)
        {
            FirebaseDTO firebaseDTO = new FirebaseDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFirebaseCredentials]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AppIdPar = cmd.Parameters.Add("@AppId", SqlDbType.Int);
                    AppIdPar.Direction = ParameterDirection.Input;
                    AppIdPar.Value = AppId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        firebaseDTO.FirebaseAppId = (myData["FirebaseAPPID"] == DBNull.Value) ? "" : (string)myData["FirebaseAPPID"];
                        firebaseDTO.FirebaseSenderId = (myData["FirebaseSenderId"] == DBNull.Value) ? "" : (string)myData["FirebaseSenderId"];

                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return firebaseDTO;
        }

        internal static bool PushNotification(string Name, string Message, int appId)
        {
            bool IsSend = false;
            try
            {
                FirebaseDTO firebase = GetFirebaseCredentials(appId);
                string applicationID = firebase.FirebaseAppId;
                string senderId = firebase.FirebaseSenderId;

                string deviceId = Name;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = Message,
                        title = "",
                        sound = "Enabled"

                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                                IsSend = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return IsSend;
        }

     

        internal static List<string> GetDeviceDetails(int AppId)
        {
            List<string> deviceNames = new List<string>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetDeviceDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AppIdPar = cmd.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                    AppIdPar.Direction = System.Data.ParameterDirection.Input;
                    AppIdPar.Value = AppId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        string device = string.Empty;
                        device = (myData["DeviceToken"] == DBNull.Value) ? "" : (string)myData["DeviceToken"];
                        deviceNames.Add(device);

                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return deviceNames;
        }

      

        internal static int AddVideosFromVimeo(List<VimeoVideoDTO> videos)
        {
            var result = 0;
            try
            {
                foreach (var vimeo in videos)
                {
                    SqlConnection sqlConnection = ConnectTODB();
                    SqlCommand sqlCommand = new SqlCommand("[dbo].[AddVimeoVideos]", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vID = sqlCommand.Parameters.Add("@VimeoVideoId", System.Data.SqlDbType.VarChar, 100);
                    vID.Direction = System.Data.ParameterDirection.Input;
                    vID.Value = vimeo.VimeoVideoID.Trim();

                    SqlParameter vTitle = sqlCommand.Parameters.Add("@VideoName", System.Data.SqlDbType.VarChar, 100);
                    vTitle.Direction = System.Data.ParameterDirection.Input;
                    vTitle.Value = vimeo.VideoName.Trim();

                    SqlParameter vDescription = sqlCommand.Parameters.Add("@VideoDescription", System.Data.SqlDbType.VarChar, 1024);
                    vDescription.Direction = System.Data.ParameterDirection.Input;
                    vDescription.Value = vimeo.VideoDesc.Trim();

                    SqlParameter vUserName = sqlCommand.Parameters.Add("@VimeoUserName", System.Data.SqlDbType.VarChar, 100);
                    vUserName.Direction = System.Data.ParameterDirection.Input;
                    vUserName.Value = vimeo.VimeoUserName.Trim();

                    SqlParameter vURL = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
                    vURL.Direction = System.Data.ParameterDirection.Input;
                    vURL.Value = vimeo.VideoUrl.Trim();

                    SqlParameter vThumbNail = sqlCommand.Parameters.Add("@VideoThumbNail", System.Data.SqlDbType.VarChar, 1024);
                    vThumbNail.Direction = System.Data.ParameterDirection.Input;
                    vThumbNail.Value = vimeo.ThumbnailUrl.Trim();

                    SqlParameter vDuration = sqlCommand.Parameters.Add("@Duration", System.Data.SqlDbType.Int);
                    vDuration.Direction = System.Data.ParameterDirection.Input;
                    vDuration.Value = vimeo.Duration;


                    SqlParameter pDate = sqlCommand.Parameters.Add("@PublishedDate", System.Data.SqlDbType.DateTime);
                    pDate.Direction = System.Data.ParameterDirection.Input;
                    pDate.Value = vimeo.PublishedDate;

                    SqlParameter mDate = sqlCommand.Parameters.Add("@ModifiedDate", System.Data.SqlDbType.DateTime);
                    mDate.Direction = System.Data.ParameterDirection.Input;
                    mDate.Value = vimeo.ModifiedDate;

                    SqlParameter viewcount = sqlCommand.Parameters.Add("@ViewsCount", System.Data.SqlDbType.Int);
                    viewcount.Direction = System.Data.ParameterDirection.Input;
                    viewcount.Value = Convert.ToInt32(vimeo.ViewCount);

                    SqlParameter Likes = sqlCommand.Parameters.Add("@LikesCount", System.Data.SqlDbType.Int);
                    Likes.Direction = System.Data.ParameterDirection.Input;
                    Likes.Value = vimeo.Likes;

                    SqlParameter comments = sqlCommand.Parameters.Add("@Comments", System.Data.SqlDbType.Int);
                    comments.Direction = System.Data.ParameterDirection.Input;
                    comments.Value = vimeo.Comments;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader != null && sqlDataReader.Read())
                    {
                        result = sqlDataReader["result"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);
                    }
                    sqlDataReader.Close();
                    sqlConnection.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

       

        internal static List<VimeoVideoDTO> GetVimeoVideos()
        {
            List<VimeoVideoDTO> Vimeo = new List<VimeoVideoDTO>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetVimeoVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    VimeoVideoDTO Video = new VimeoVideoDTO();
                    Video.ID = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Video.VimeoVideoID = sqlDataReader["VimeoVideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VimeoVideoId"]);
                    Video.VideoName = sqlDataReader["VideoName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoName"]);
                    Video.VideoDesc = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    Video.VimeoUserName = sqlDataReader["VimeoUserName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VimeoUserName"]);
                    Video.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbnailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Video.Duration = sqlDataReader["Duration"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Duration"]);
                    Video.PublishedDate = sqlDataReader["PublishedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    Video.Likes = sqlDataReader["LikesCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["LikesCount"]);
                    Video.ViewCount = sqlDataReader["ViewCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ViewCount"]);
                    Video.Comments = sqlDataReader["Comments"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Comments"]);
                    Vimeo.Add(Video);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Vimeo;
        }

        internal static int InsertShareEmailaddress(string emailaddress, int appId)
        {
            int id = 0;
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[InsertShareEmailaddress]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter emailaddressPar = sqlCommand.Parameters.Add("@Emailaddress",  SqlDbType.VarChar, 100);
                emailaddressPar.Direction =  ParameterDirection.Input;
                emailaddressPar.Value = emailaddress;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                id = Convert.ToInt32(sqlCommand.ExecuteScalar());

                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return id;
        }

        internal static int AddHelpVideos(HelpVideos helpVideos)
        {
            int id = 0;
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[AddHelpVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter VideoTitle = sqlCommand.Parameters.Add("@VideoTitle", SqlDbType.VarChar, 100);
                VideoTitle.Direction = ParameterDirection.Input;
                VideoTitle.Value = helpVideos.Name;

                SqlParameter VideoUrl = sqlCommand.Parameters.Add("@VideoUrl", SqlDbType.VarChar, 1024);
                VideoUrl.Direction = ParameterDirection.Input;
                VideoUrl.Value = helpVideos.VideoURL;

                SqlParameter ThumbNailUrl = sqlCommand.Parameters.Add("@ThumbNailUrl", SqlDbType.VarChar, 1024);
                ThumbNailUrl.Direction = ParameterDirection.Input;
                ThumbNailUrl.Value = helpVideos.ThumbnailURL;

                SqlParameter Format = sqlCommand.Parameters.Add("@Format", SqlDbType.VarChar, 50);
                Format.Direction = ParameterDirection.Input;
                Format.Value = helpVideos.Format;

                SqlParameter IsActive = sqlCommand.Parameters.Add("@IsActive", System.Data.SqlDbType.Int);
                IsActive.Direction = System.Data.ParameterDirection.Input;
                IsActive.Value = helpVideos.Active;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = helpVideos.AppId;

                SqlParameter languageIdPar = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                languageIdPar.Direction = System.Data.ParameterDirection.Input;
                languageIdPar.Value = helpVideos.LanguageId;

                id = Convert.ToInt32(sqlCommand.ExecuteScalar());

                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return id;
        }

        internal static List<HelpVideos> GetHelpVideos(int AppId,int LanguageId)
        {
            List<HelpVideos> helpVideos = new List<HelpVideos>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetHelpVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = AppId;

                SqlParameter languageIdPar = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                languageIdPar.Direction = System.Data.ParameterDirection.Input;
                languageIdPar.Value =  LanguageId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    HelpVideos Video = new HelpVideos();
                    Video.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]); 
                    Video.Name = sqlDataReader["VideoTitle"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoTitle"]); 
                    Video.VideoURL = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbnailURL = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]); 
                    Video.Format = sqlDataReader["Format"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Format"]);
                    Video.Active = sqlDataReader["isActive"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["isActive"]);
                    Video.LanguageId = sqlDataReader["Language"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Language"]);
                    helpVideos.Add(Video);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return helpVideos;
        }

        internal static int UpdateActiveStatus(HelpVideos helpVideos)
        {
            int id = 0;
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[UpdateActiveStatus]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter helpVideoIdPar = sqlCommand.Parameters.Add("@helpVideoId", SqlDbType.Int);
                helpVideoIdPar.Direction = ParameterDirection.Input;
                helpVideoIdPar.Value = helpVideos.Id;

                SqlParameter statusPar = sqlCommand.Parameters.Add("@status", SqlDbType.Int);
                statusPar.Direction = ParameterDirection.Input;
                statusPar.Value = helpVideos.Active;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appIdPar.Direction =  ParameterDirection.Input;
                appIdPar.Value = helpVideos.AppId;

                SqlParameter languageIdPar = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                languageIdPar.Direction = System.Data.ParameterDirection.Input;
                languageIdPar.Value = helpVideos.LanguageId;

                id = Convert.ToInt32(sqlCommand.ExecuteScalar());

                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return id;
        }

        internal static HelpVideos GetActiveHelpVideo(int appId,int languageId)
        {
            HelpVideos Video = new HelpVideos();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetActiveHelpVideo]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                SqlParameter languageIdPar = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                languageIdPar.Direction = System.Data.ParameterDirection.Input;
                languageIdPar.Value = languageId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                   
                    Video.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Video.Name = sqlDataReader["VideoTitle"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoTitle"]);
                    Video.VideoURL = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbnailURL = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Video.Format = sqlDataReader["Format"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Format"]);
                    Video.Active = sqlDataReader["isActive"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["isActive"]);
                    Video.LanguageId = sqlDataReader["Language"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Language"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Video;
        }

     

        internal static int AddWistiaVideos(List<WistiaVideoDTO> videos)
        {
            var result = 0;
            try
            {
                foreach (var wistia in videos)
                {
                    SqlConnection sqlConnection = ConnectTODB();
                    SqlCommand sqlCommand = new SqlCommand("[dbo].[AddWistiaVideos]", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vID = sqlCommand.Parameters.Add("@WistiaVideoId", System.Data.SqlDbType.VarChar, 100);
                    vID.Direction = System.Data.ParameterDirection.Input;
                    vID.Value = wistia.WistiaVideoId.Trim();

                    SqlParameter vTitle = sqlCommand.Parameters.Add("@VideoName", System.Data.SqlDbType.VarChar, 500);
                    vTitle.Direction = System.Data.ParameterDirection.Input;
                    vTitle.Value = wistia.Name.Trim();

                    SqlParameter vDescription = sqlCommand.Parameters.Add("@VideoDescription", System.Data.SqlDbType.VarChar, 2048);
                    vDescription.Direction = System.Data.ParameterDirection.Input;
                    vDescription.Value = wistia.Description.Trim();

                    SqlParameter vhid = sqlCommand.Parameters.Add("@VideoHashId", System.Data.SqlDbType.VarChar, 50);
                    vhid.Direction = System.Data.ParameterDirection.Input;
                    vhid.Value = wistia.HashId.Trim();

                    SqlParameter vURL = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
                    vURL.Direction = System.Data.ParameterDirection.Input;
                    vURL.Value = wistia.VideoUrl.Trim();

                    SqlParameter vThumbNail = sqlCommand.Parameters.Add("@VideoThumbNail", System.Data.SqlDbType.VarChar, 1024);
                    vThumbNail.Direction = System.Data.ParameterDirection.Input;
                    vThumbNail.Value = wistia.ThumbNailUrl.Trim();

                    SqlParameter vurltype = sqlCommand.Parameters.Add("@VideoUrlType", System.Data.SqlDbType.VarChar, 50);
                    vurltype.Direction = System.Data.ParameterDirection.Input;
                    vurltype.Value = wistia.VideoUrlType.Trim();

                    SqlParameter vprojid = sqlCommand.Parameters.Add("@ProjectId", System.Data.SqlDbType.VarChar, 50);
                    vprojid.Direction = System.Data.ParameterDirection.Input;
                    vprojid.Value = wistia.ProjectId.Trim();

                    SqlParameter vprojhash = sqlCommand.Parameters.Add("@ProjectHashId", System.Data.SqlDbType.VarChar, 100);
                    vprojhash.Direction = System.Data.ParameterDirection.Input;
                    vprojhash.Value = wistia.ProjectHashId.Trim();

                    SqlParameter vprojname = sqlCommand.Parameters.Add("@ProjectName", System.Data.SqlDbType.VarChar, 100);
                    vprojname.Direction = System.Data.ParameterDirection.Input;
                    vprojname.Value = wistia.ProjectName.Trim();

                    SqlParameter vDuration = sqlCommand.Parameters.Add("@Duration", System.Data.SqlDbType.VarChar,50);
                    vDuration.Direction = System.Data.ParameterDirection.Input;
                    vDuration.Value = wistia.Duration;


                    SqlParameter pgloads = sqlCommand.Parameters.Add("@PageLoads", System.Data.SqlDbType.Int);
                    pgloads.Direction = System.Data.ParameterDirection.Input;
                    pgloads.Value = Convert.ToInt32(wistia.PageLoads);

                    SqlParameter visitors = sqlCommand.Parameters.Add("@visitors", System.Data.SqlDbType.Int);
                    visitors.Direction = System.Data.ParameterDirection.Input;
                    visitors.Value = wistia.visitors;

                    SqlParameter plays = sqlCommand.Parameters.Add("@AveragePercentWatched", System.Data.SqlDbType.Int);
                    plays.Direction = System.Data.ParameterDirection.Input;
                    plays.Value = wistia.Plays;

                    SqlParameter avgper = sqlCommand.Parameters.Add("@Plays", System.Data.SqlDbType.Int);
                    avgper.Direction = System.Data.ParameterDirection.Input;
                    avgper.Value = wistia.AveragePercentWatched;

                    SqlParameter appid = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                    appid.Direction = System.Data.ParameterDirection.Input;
                    appid.Value = wistia.AppId;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader != null && sqlDataReader.Read())
                    {
                        result = sqlDataReader["result"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);
                    }
                    sqlDataReader.Close();
                    sqlConnection.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<WistiaVideoDTO> GetWistiaVideos(int AppId)
        {
            List<WistiaVideoDTO> videos = new List<WistiaVideoDTO>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetWistiaVideoByAppId]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appid = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appid.Direction = System.Data.ParameterDirection.Input;
                appid.Value = AppId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    WistiaVideoDTO Video = new WistiaVideoDTO();
                    Video.ID = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Video.WistiaVideoId = sqlDataReader["WistiaVideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["WistiaVideoId"]);
                    Video.Name = sqlDataReader["VideoName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoName"]);
                    Video.Description = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    Video.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbNailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Video.Duration = sqlDataReader["Duration"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Duration"]);
                    Video.PageLoads = sqlDataReader["PageLoads"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["PageLoads"]);
                    Video.visitors = sqlDataReader["visitors"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["visitors"]);
                    Video.Plays = sqlDataReader["Plays"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Plays"]);
                    Video.AveragePercentWatched = sqlDataReader["AveragePercentWatched"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AveragePercentWatched"]);
                    Video.CreatedDate = sqlDataReader["CreatedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                    Video.AppId = sqlDataReader["AppId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppId"]);
                    videos.Add(Video);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static int AddPushLog(PushLogDTO pushInfo)
        {
            int res = 0;
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[AddPushLog]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter tc = sqlCommand.Parameters.Add("@TotalCount", System.Data.SqlDbType.Int);
                tc.Direction = System.Data.ParameterDirection.Input;
                tc.Value = pushInfo.TotalCount;

                SqlParameter SuccessCount = sqlCommand.Parameters.Add("@SuccessCount", System.Data.SqlDbType.Int);
                SuccessCount.Direction = System.Data.ParameterDirection.Input;
                SuccessCount.Value = pushInfo.SuccessCount;

                SqlParameter Message = sqlCommand.Parameters.Add("@Message", System.Data.SqlDbType.VarChar, 1024);
                Message.Direction = System.Data.ParameterDirection.Input;
                Message.Value = pushInfo.Message;

                SqlParameter AppId = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                AppId.Direction = System.Data.ParameterDirection.Input;
                AppId.Value = pushInfo.AppId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {

                    res = sqlDataReader["result"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return res;
        }

        internal static void DeleteWistiaVideo(string VideoName)
        {

            try
            {
                SqlConnection sqlConnection = ConnectTODB();

                SqlCommand sqlCommand = new SqlCommand("[DeleteWistiaVideo]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter vname = sqlCommand.Parameters.Add("@VideoName", System.Data.SqlDbType.VarChar, 2048);
                vname.Direction = System.Data.ParameterDirection.Input;
                vname.Value = VideoName;

                sqlCommand.ExecuteNonQuery();

                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }

        internal static List<WistiaVideoDTO> GetWistiaVideosV2(int AppId, int LanguageId)
        {
            List<WistiaVideoDTO> videos = new List<WistiaVideoDTO>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetWistiaVideoByAppIdV2]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appid = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appid.Direction = System.Data.ParameterDirection.Input;
                appid.Value = AppId;

                SqlParameter langid = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                langid.Direction = System.Data.ParameterDirection.Input;
                langid.Value = LanguageId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    WistiaVideoDTO Video = new WistiaVideoDTO();
                    Video.ID = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Video.WistiaVideoId = sqlDataReader["WistiaVideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["WistiaVideoId"]);
                    Video.Name = sqlDataReader["VideoName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoName"]);
                    Video.Description = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    Video.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbNailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Video.Duration = sqlDataReader["Duration"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Duration"]);
                    Video.PageLoads = sqlDataReader["PageLoads"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["PageLoads"]);
                    Video.visitors = sqlDataReader["visitors"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["visitors"]);
                    Video.Plays = sqlDataReader["Plays"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Plays"]);
                    Video.AveragePercentWatched = sqlDataReader["AveragePercentWatched"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AveragePercentWatched"]);
                    Video.CreatedDate = sqlDataReader["CreatedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                    Video.AppId = sqlDataReader["AppId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppId"]);
                    videos.Add(Video);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static List<WistiaVideoDTO> GetWistiaVideosFromDB()
        {
            List<WistiaVideoDTO> videos = new List<WistiaVideoDTO>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetWistiaVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    WistiaVideoDTO Video = new WistiaVideoDTO();
                    Video.ID = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Video.WistiaVideoId = sqlDataReader["WistiaVideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["WistiaVideoId"]);
                    Video.Name = sqlDataReader["VideoName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoName"]);
                    Video.Description = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    Video.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbNailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Video.Duration = sqlDataReader["Duration"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Duration"]);
                    Video.PageLoads = sqlDataReader["PageLoads"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["PageLoads"]);
                    Video.visitors = sqlDataReader["visitors"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["visitors"]);
                    Video.Plays = sqlDataReader["Plays"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Plays"]);
                    Video.AveragePercentWatched = sqlDataReader["AveragePercentWatched"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AveragePercentWatched"]);
                    Video.CreatedDate = sqlDataReader["CreatedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                    Video.AppId = sqlDataReader["AppId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppId"]);
                    videos.Add(Video);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static int AddWistiaVideosV2(List<WistiaVideoDTO> videos)
        {
            var result = 0;
            try
            {
                foreach (var wistia in videos)
                {
                    SqlConnection sqlConnection = ConnectTODB();
                    SqlCommand sqlCommand = new SqlCommand("[dbo].[AddWistiaVideosV2]", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter vID = sqlCommand.Parameters.Add("@WistiaVideoId", System.Data.SqlDbType.VarChar, 100);
                    vID.Direction = System.Data.ParameterDirection.Input;
                    vID.Value = wistia.WistiaVideoId.Trim();

                    SqlParameter vTitle = sqlCommand.Parameters.Add("@VideoName", System.Data.SqlDbType.VarChar, 500);
                    vTitle.Direction = System.Data.ParameterDirection.Input;
                    vTitle.Value = wistia.Name.Trim();

                    SqlParameter vDescription = sqlCommand.Parameters.Add("@VideoDescription", System.Data.SqlDbType.VarChar, 2048);
                    vDescription.Direction = System.Data.ParameterDirection.Input;
                    vDescription.Value = wistia.Description.Trim();

                    SqlParameter vhid = sqlCommand.Parameters.Add("@VideoHashId", System.Data.SqlDbType.VarChar, 50);
                    vhid.Direction = System.Data.ParameterDirection.Input;
                    vhid.Value = wistia.HashId.Trim();

                    SqlParameter vURL = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
                    vURL.Direction = System.Data.ParameterDirection.Input;
                    vURL.Value = wistia.VideoUrl.Trim();

                    SqlParameter vThumbNail = sqlCommand.Parameters.Add("@VideoThumbNail", System.Data.SqlDbType.VarChar, 1024);
                    vThumbNail.Direction = System.Data.ParameterDirection.Input;
                    vThumbNail.Value = wistia.ThumbNailUrl.Trim();

                    SqlParameter vurltype = sqlCommand.Parameters.Add("@VideoUrlType", System.Data.SqlDbType.VarChar, 50);
                    vurltype.Direction = System.Data.ParameterDirection.Input;
                    vurltype.Value = wistia.VideoUrlType.Trim();

                    SqlParameter vprojid = sqlCommand.Parameters.Add("@ProjectId", System.Data.SqlDbType.VarChar, 50);
                    vprojid.Direction = System.Data.ParameterDirection.Input;
                    vprojid.Value = wistia.ProjectId.Trim();

                    SqlParameter vprojhash = sqlCommand.Parameters.Add("@ProjectHashId", System.Data.SqlDbType.VarChar, 100);
                    vprojhash.Direction = System.Data.ParameterDirection.Input;
                    vprojhash.Value = wistia.ProjectHashId.Trim();

                    SqlParameter vprojname = sqlCommand.Parameters.Add("@ProjectName", System.Data.SqlDbType.VarChar, 100);
                    vprojname.Direction = System.Data.ParameterDirection.Input;
                    vprojname.Value = wistia.ProjectName.Trim();

                    SqlParameter vDuration = sqlCommand.Parameters.Add("@Duration", System.Data.SqlDbType.VarChar, 50);
                    vDuration.Direction = System.Data.ParameterDirection.Input;
                    vDuration.Value = wistia.Duration;


                    SqlParameter pgloads = sqlCommand.Parameters.Add("@PageLoads", System.Data.SqlDbType.Int);
                    pgloads.Direction = System.Data.ParameterDirection.Input;
                    pgloads.Value = Convert.ToInt32(wistia.PageLoads);

                    SqlParameter visitors = sqlCommand.Parameters.Add("@visitors", System.Data.SqlDbType.Int);
                    visitors.Direction = System.Data.ParameterDirection.Input;
                    visitors.Value = wistia.visitors;

                    SqlParameter plays = sqlCommand.Parameters.Add("@AveragePercentWatched", System.Data.SqlDbType.Int);
                    plays.Direction = System.Data.ParameterDirection.Input;
                    plays.Value = wistia.Plays;

                    SqlParameter avgper = sqlCommand.Parameters.Add("@Plays", System.Data.SqlDbType.Int);
                    avgper.Direction = System.Data.ParameterDirection.Input;
                    avgper.Value = wistia.AveragePercentWatched;

                    SqlParameter appid = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                    appid.Direction = System.Data.ParameterDirection.Input;
                    appid.Value = wistia.AppId;

                    SqlParameter langid = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                    langid.Direction = System.Data.ParameterDirection.Input;
                    langid.Value = wistia.LanguageId;

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader != null && sqlDataReader.Read())
                    {
                        result = sqlDataReader["result"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);
                    }
                    sqlDataReader.Close();
                    sqlConnection.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }


        internal static List<WistiaVideoDTO> GetWistiaVideosWithSearch(int AppId, int LanguageId,string searchText)
        {
            List<WistiaVideoDTO> videos = new List<WistiaVideoDTO>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetWistiaVideosWithSearch]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appid = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appid.Direction = System.Data.ParameterDirection.Input;
                appid.Value = AppId;

                SqlParameter langid = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                langid.Direction = System.Data.ParameterDirection.Input;
                langid.Value = LanguageId;

                SqlParameter searchTextPar = sqlCommand.Parameters.Add("@SearchText", System.Data.SqlDbType.VarChar, 500);
                searchTextPar.Direction = System.Data.ParameterDirection.Input;
                searchTextPar.Value = searchText;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    WistiaVideoDTO Video = new WistiaVideoDTO();
                    Video.ID = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    Video.WistiaVideoId = sqlDataReader["WistiaVideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["WistiaVideoId"]);
                    Video.Name = sqlDataReader["VideoName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoName"]);
                    Video.Description = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    Video.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Video.ThumbNailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Video.Duration = sqlDataReader["Duration"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Duration"]);
                    Video.PageLoads = sqlDataReader["PageLoads"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["PageLoads"]);
                    Video.visitors = sqlDataReader["visitors"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["visitors"]);
                    Video.Plays = sqlDataReader["Plays"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Plays"]);
                    Video.AveragePercentWatched = sqlDataReader["AveragePercentWatched"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AveragePercentWatched"]);
                    Video.CreatedDate = sqlDataReader["CreatedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                    Video.AppId = sqlDataReader["AppId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppId"]);
                    videos.Add(Video);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return videos;
        }

        internal static void AddDeviceLanguage(int DeviceId, int languageId)
        {
           
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[AddDeviceLanguage]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter deviceId = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.Int);
                deviceId.Direction = System.Data.ParameterDirection.Input;
                deviceId.Value = DeviceId;

                SqlParameter langid = sqlCommand.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                langid.Direction = System.Data.ParameterDirection.Input;
                langid.Value = languageId;


                sqlCommand.ExecuteNonQuery(); 
                
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            
        }
        internal static SupportMailDTO GetSupportMails(string name)
        {

            SupportMailDTO s = new SupportMailDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSupportMails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 500);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = name;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        s.EmailAddress = (string)myData["EmailAddress"];
                        s.Password = (string)myData["Password"];
                        s.Host = (string)myData["Host"];
                        s.Port = (string)myData["Port"];
                        s.SSL = (string)myData["SSL"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return s;
        }
        internal static List<ShareDetailsDTO> GetShareEmails(int AppId)
        {
            List<ShareDetailsDTO> emails = new List<ShareDetailsDTO>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetShareEmailaddress]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = AppId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    ShareDetailsDTO email = new ShareDetailsDTO();
                    email.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    email.Emailaddress = sqlDataReader["Emailaddress"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Emailaddress"]);
                    email.CreatedDate = sqlDataReader["CreatedDate"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                    email.AppID = sqlDataReader["AppID"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppID"]);

                    emails.Add(email);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return emails;
        }
    }
}