﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models
{
    public class Constants
    {
        public static string DEVICE_REGISTERED = "Device Registered Successfully";
        public static string DEVICE_NOT_REGISTERED = "Device Not Registered Successfully";
        public static string DEVICE_INFO = "Device information retrieved Successfully";
        public static string FEATURE_SAVED = "Feature usage Tracked Successfully";
        public static string FEATURE_NOT_SAVED = "Couldn't be able to track the feature usage, Please try after sometime.";

        public static string NO_RECORDS = "No Records Found";
        public static string DETAILS_GET_SUCCESSFULLY = "Data retrieved successfully";
        public static string Videos_Fetched = "Videos Retrieved Successfully";
        public static string Blogs_Fetched = "Blogs retrived successfully";
        public static string Blogs_Not_Fetched = "Couldn't able to fetch the Blogs, please try after sometime";
        public static string Videos_Not_Fetched = "Couldn't able to fetch the videos, please try after sometime";
        public static string Dosage_calculation = "Dosage calculation has done successfully";
        public static string Dosage_Not_calculation = "Couldn't able to calculate the Dosage now, Please try after sometime";
        public static string DeviceInfo_Fetched = "Device Information retrieved successfully";
        public static string DeviceInfo_Not_Fetched = "Couldn't fetch the Device Information, please try after sometime";
        public static string FTForVideos_Fetched = "Feature Tracking for Videos retrieved successfully";
        public static string FTForVideos_Not_Fetched = "Couldn't fetch the Feature Tracking for Videos, please try after sometime";
        public static string FTForBlogs_Fetched = "Feature Tracking for Blogs retrieved successfully";
        public static string FTForBlogs_Not_Fetched = "Couldn't fetch the Feature Tracking for Blogs, please try after sometime";
        public static string FTForProducts_Fetched = "Feature Tracking for Products retrieved successfully";
        public static string FTForProducts_Not_Fetched = "Couldn't fetch the Feature Tracking for Products, please try after sometime";
        public static string PushNotification_Sent_Successfully = "Push Notifications sent successfully"; 
        public static string Blogs_Added = "Blogs added successfully"; 
        public static string EMAIL_ADDED = "Email added successfully";
        public static string FORMULA_GET_SUCCESSFULLY = "Formulas Get Successfully";
        public static string MAIL_SENT_SUCCESS = "Mail sent successfully";
    }
}