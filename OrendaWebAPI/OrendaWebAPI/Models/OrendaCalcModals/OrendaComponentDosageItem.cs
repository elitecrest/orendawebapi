﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models.OrendaCalcModals
{
    /// <summary>
    /// Model class for component dosageItem
    /// </summary>
    public class OrendaComponentDosageItem
    {
        /// <summary>
        /// Component name
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// Dosage Quantity
        /// </summary>
        public string DosageQty { get; set; }

        /// <summary>
        /// Quantity description
        /// </summary>
        public string QtyDescription { get; set; }
    }
}