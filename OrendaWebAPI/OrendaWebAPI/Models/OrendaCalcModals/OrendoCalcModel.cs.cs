﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models.OrendaCalcModals
{
    public class OrendoCalcModel
    {
        /// <summary>
        /// Water in the pool in gallons
        /// </summary>
        public int GallonsOfWater { get; set; }

        /// <summary>
        /// Current Temparature
        /// </summary>
        public int CurrentTemparature { get; set; }

        /// <summary>
        /// Desired Temparature
        /// </summary>
        public int DesiredTemparature { get; set; }

        /// <summary>
        /// Current pH
        /// </summary>
        public double CurrentpH { get; set; }

        /// <summary>
        /// Desired pH
        /// </summary>
        public double DesiredpH { get; set; }

        /// <summary>
        /// Current Chlorine level
        /// </summary>
        public double CurrentChlorine { get; set; }

        /// <summary>
        /// Desired Chlorine
        /// </summary>
        public double DesiredChlorine { get; set; }

        /// <summary>
        /// Current Calcium
        /// </summary>
        public int CurrentCalcium { get; set; }

        /// <summary>
        /// Desired Calcium
        /// </summary>
        public int DesiredCalcium { get; set; }

        /// <summary>
        /// Current Alkalinity
        /// </summary>
        public int CurrentAlkalinity { get; set; }

        /// <summary>
        /// Desired Alkalinity
        /// </summary>
        public int DesiredAlkalinity { get; set; }

        /// <summary>
        /// Current Stabilizer
        /// </summary>
        public int CurrentStabilizer { get; set; }

        /// <summary>
        /// Desired Stabilizer
        /// </summary>
        public int DesiredStabilizer { get; set; }

        /// <summary>
        /// Current Salt
        /// </summary>
        public int CurrentSalt { get; set; }

        /// <summary>
        /// Desired Salt
        /// </summary>
        public int DesiredSalt { get; set; }

        /// <summary>
        /// Current Phosphates
        /// </summary>
        public int CurrentPhosphates { get; set; }

        /// <summary>
        /// DesiredPhosphates
        /// </summary>
        public int DesiredPhosphates { get; set; }

        /// <summary>
        /// Calculator Type
        /// </summary>
        public string OrendaCalcType { get; set; }
        
    }
}