﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models.OrendaCalcModals
{
    /// <summary>
    /// Orenda calc response modal
    /// </summary>
    public class OrendaCalcResponse
    {
        /// <summary>
        /// Dosage Items
        /// </summary>
        public List<OrendaComponentDosageItem> DosageItems { get; set; }

        /// <summary>
        /// Products info
        /// </summary>
        public List<OrendaCalcProduct> ProductsInfo { get; set; }
    }

}