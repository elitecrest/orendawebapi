﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models.OrendaCalcModals
{
    public class OrendaCalcProductItemEntry
    {
        /// <summary>
        /// Component
        /// </summary>
        public string Component { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        public double Qunantity { get; set; }

        /// <summary>
        /// Measurement
        /// </summary>
        public string Measurement { get; set; }

    }
}