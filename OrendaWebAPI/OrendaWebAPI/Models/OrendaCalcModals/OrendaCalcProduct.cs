﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models.OrendaCalcModals
{
    /// <summary>
    /// Orenda Cal product modal
    /// </summary>
    public class OrendaCalcProduct
    {
        /// <summary>
        /// Name of the product
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Dosage Items
        /// </summary>
        public List<OrendaCalcProductItemEntry> DosageItems { get; set; }
    }
}