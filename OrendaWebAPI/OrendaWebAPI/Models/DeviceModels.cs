﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models
{
    internal static class DeviceModels
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrendaDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static object RegisterDevice(DeviceInfoDTO deviceInfo)
        {
            DeviceInfoDTO deviceInfoDto = null;
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[RegisterDevice]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter deviceId = sqlCommand.Parameters.Add("@DeviceId", System.Data.SqlDbType.NVarChar, 1024);
                deviceId.Direction = System.Data.ParameterDirection.Input;
                deviceId.Value = deviceInfo.DeviceToken.Trim();
                SqlParameter deviceType = sqlCommand.Parameters.Add("@DeviceType", System.Data.SqlDbType.Int);
                deviceType.Direction = System.Data.ParameterDirection.Input;
                deviceType.Value = deviceInfo.DeviceType;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    deviceInfoDto = new DeviceInfoDTO();
                    deviceInfoDto.DeviceId = sqlDataReader["DeviceId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceId"]);
                    deviceInfoDto.DeviceToken = sqlDataReader["DeviceToken"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["DeviceToken"]);
                    deviceInfoDto.DeviceType = sqlDataReader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceType"]);
                    deviceInfoDto.CreatedDate = Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return deviceInfoDto;
        }

        internal static int FeatureTracking(FeatureTrackingDto featuresDto)
        {
            int result = 0;
            try
            {
                SqlConnection sqlConnection = ConnectTODB();

                //check admin user ...
                SqlCommand sqlCommand = new SqlCommand("[FeatureViewTracking]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                sqlCommand.Parameters.AddWithValue("@FeatureId", featuresDto.FeatureId);
                sqlCommand.Parameters.AddWithValue("@DeviceId", featuresDto.DeviceId);
                sqlCommand.Parameters.AddWithValue("@DeviceType", featuresDto.DeviceType);
                sqlCommand.Parameters.AddWithValue("@Value", featuresDto.Value);
                sqlCommand.Parameters.AddWithValue("@OfflineCount", featuresDto.Count);

                SqlDataReader reader = sqlCommand.ExecuteReader();
                while (reader != null && reader.Read())
                {
                    result = reader["result"] == DBNull.Value ? 0 : Convert.ToInt32(reader["result"]);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<DeviceDTO> GetDeviceInfo()
        {
            List<DeviceDTO> devices = new List<DeviceDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetDeviceInformation]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    DeviceDTO device = new DeviceDTO();
                    device.Count = sqlDataReader["deviceCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["deviceCount"]);
                    device.DeviceType = sqlDataReader["deviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["deviceType"]);
                    devices.Add(device);
                }
                
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            foreach(DeviceDTO dev in devices)
            {
                if (dev.DeviceType == 1)
                    dev.DeviceName = "iPhone";
                else
                    dev.DeviceName = "Android";
            }
            return devices;
        }

        internal static List<FeatureTrackingDtoForVideos> GetFTForVideos()
        {
            List<FeatureTrackingDtoForVideos> FTForVs = new List<FeatureTrackingDtoForVideos>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetFeatureTrackingForVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    FeatureTrackingDtoForVideos ft = new FeatureTrackingDtoForVideos();
                    //ft.FeatureId = sqlDataReader["FeatureId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["FeatureId"]);
                    //ft.Value = sqlDataReader["Value"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Value"]);
                    //ft.DeviceId= sqlDataReader["DeviceId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceId"]);
                    //ft.DeviceType = sqlDataReader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceType"]);
                    ft.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? "" : (sqlDataReader["VideoUrl"].ToString());
                    ft.VideoName = sqlDataReader["VideoTitle"] == DBNull.Value ? "" : (sqlDataReader["VideoTitle"].ToString());
                    ft.VideoDesc = sqlDataReader["VideoDescription"] == DBNull.Value ? "" : (sqlDataReader["VideoDescription"].ToString());
                    ft.VideoViewCount = sqlDataReader["VideoViewCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["VideoViewCount"]);

                    FTForVs.Add(ft);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return FTForVs;
        }

        internal static List<FeatureTrackingDtoForBlogs> GetFTForBlogs()
        {
            List<FeatureTrackingDtoForBlogs> FTForBs = new List<FeatureTrackingDtoForBlogs>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetFeatureTrackingForBlogs]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    FeatureTrackingDtoForBlogs ft = new FeatureTrackingDtoForBlogs();
                    //ft.FeatureId = sqlDataReader["FeatureId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["FeatureId"]);
                    //ft.Value = sqlDataReader["Value"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Value"]);
                    //ft.DeviceId = sqlDataReader["DeviceId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceId"]);
                    //ft.DeviceType = sqlDataReader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceType"]);
                    ft.BlogUrl = sqlDataReader["BlogUrl"] == DBNull.Value ? "" : (sqlDataReader["BlogUrl"].ToString());
                    ft.BlogName = sqlDataReader["BlogName"] == DBNull.Value ? "" : (sqlDataReader["BlogName"].ToString());
                    ft.BlogDesc = sqlDataReader["BlogDesc"] == DBNull.Value ? "" : (sqlDataReader["BlogDesc"].ToString());
                    ft.BlogViewCount = sqlDataReader["BlogViewCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["BlogViewCount"]);

                    FTForBs.Add(ft);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return FTForBs;
        }

        internal static List<FeatureTrackingDtoForProducts> GetFTForProducts()
        {
            List<FeatureTrackingDtoForProducts> FTForPs = new List<FeatureTrackingDtoForProducts>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetFeatureTrackingForProducts]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    FeatureTrackingDtoForProducts ft = new FeatureTrackingDtoForProducts();
                    //ft.FeatureId = sqlDataReader["FeatureId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["FeatureId"]);
                    //ft.Value = sqlDataReader["Value"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Value"]);
                    //ft.DeviceId= sqlDataReader["DeviceId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceId"]);
                    //ft.DeviceType = sqlDataReader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceType"]);
                    ft.ProductName = sqlDataReader["Value"] == DBNull.Value ? "" : (sqlDataReader["Value"].ToString());
                    ft.ProductViewCount = sqlDataReader["ProductViewCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ProductViewCount"]);

                    FTForPs.Add(ft);
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return FTForPs;
        }


        internal static List<DeviceDTO> GetDeviceCountByDate(DateTime FromDate,DateTime ToDate)
        {
            List<DeviceDTO> devices = new List<DeviceDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetDeviceBydate]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@FromDate", FromDate);
                sqlCommand.Parameters.AddWithValue("@ToDate", ToDate);


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    DeviceDTO device = new DeviceDTO();
                    device.Count = sqlDataReader["deviceCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["deviceCount"]);
                    device.DeviceType = sqlDataReader["deviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["deviceType"]);
                    devices.Add(device);
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            foreach (DeviceDTO dev in devices)
            {
                if (dev.DeviceType == 1)
                    dev.DeviceName = "iPhone";
                else
                    dev.DeviceName = "Android";
            }
            return devices;
        }
    }
}