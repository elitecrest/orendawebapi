﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
//using Humanizer.DateTimeHumanizeStrategy;

namespace OrendaWebAPI.Models
{
    public class BlogsModels
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrendaDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static List<Blogs> GetBlogs()
        {
            List<Blogs> blogs = new List<Blogs>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetBlogs]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    Blogs blog = new Blogs();
                    blog.BlogId = sqlDataReader["BlogId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["BlogId"]);
                    blog.BlogName = sqlDataReader["BlogName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BlogName"]);
                    blog.BlogDescription = sqlDataReader["BlogDesc"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BlogDesc"]);
                    blog.BlogUrl = sqlDataReader["BlogUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BlogUrl"]);
                    blog.ThumbNailURL = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    blog.PublishedDate = pdate.ToString("dd MMMM yyyy");
                    blogs.Add(blog);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return blogs;
        }


        internal static int AddBlogs(Elitecrest.HtmlExtractor.Models.OrendaBlogEntry blog)
        {
            int Id = 0;
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[AddBlogs]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure; 
               

                SqlParameter blogUrlPar = sqlCommand.Parameters.Add("@BlogUrl", System.Data.SqlDbType.VarChar, 512);
                blogUrlPar.Direction = System.Data.ParameterDirection.Input;
                blogUrlPar.Value = blog.BlogUrl;

                SqlParameter blogNamePar = sqlCommand.Parameters.Add("@BlogName", System.Data.SqlDbType.VarChar, 256);
                blogNamePar.Direction = System.Data.ParameterDirection.Input;
                blogNamePar.Value = blog.BlogName; 

                SqlParameter blogDescPar = sqlCommand.Parameters.Add("@BlogDesc", System.Data.SqlDbType.VarChar, 1024);
                blogDescPar.Direction = System.Data.ParameterDirection.Input;
                blogDescPar.Value = blog.BlogDescription; 


                SqlParameter thumbnailURLPar = sqlCommand.Parameters.Add("@ThumbnailURL", System.Data.SqlDbType.VarChar, 512);
                thumbnailURLPar.Direction = System.Data.ParameterDirection.Input;
                thumbnailURLPar.Value = blog.ThumbNailURL; 
              
                SqlParameter publishedDatePar = sqlCommand.Parameters.Add("@PublishedDate", System.Data.SqlDbType.DateTime);
                publishedDatePar.Direction = System.Data.ParameterDirection.Input;
                publishedDatePar.Value = Convert.ToDateTime(blog.PublishedDate); 

                Id = Convert.ToInt32(sqlCommand.ExecuteScalar());


                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Id;
        }

        internal static void DeleteBlog(string blogName)
        {
           
            try
            {
                SqlConnection sqlConnection = ConnectTODB();

                SqlCommand sqlCommand = new SqlCommand("[DeleteBlogs]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure; 
           

                SqlParameter blogNamePar = sqlCommand.Parameters.Add("@BlogName", System.Data.SqlDbType.VarChar, 256);
                blogNamePar.Direction = System.Data.ParameterDirection.Input;
                blogNamePar.Value = blogName;  

                sqlCommand.ExecuteNonQuery(); 

                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            
        }

        internal static List<Blogs> GetBlogsBySearch(string searchText)
        {
            List<Blogs> Blogs = new List<Blogs>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetBlogsBySearchText]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", System.Data.SqlDbType.VarChar, 500);
                    SearchText.Direction = System.Data.ParameterDirection.Input;
                    SearchText.Value = searchText;
 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        Blogs v = new Blogs();
                        v.BlogId = (int)myData["BlogId"];
                        v.BlogName = (string)myData["BlogName"].ToString();
                        v.BlogDescription = (string)myData["BlogDesc"].ToString();
                        v.BlogUrl = (string)myData["BlogUrl"].ToString();
                        v.ThumbNailURL = (string)myData["ThumbNailURL"].ToString();
                        v.PublishedDate = (string)myData["PublishedDate"].ToString();
                        v.Comments = (int)myData["Comments"];
                        Blogs.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Blogs;
        }

        internal static List<Blogs> GetBlogsBySearchNew(string searchText, int AppId, int LanguageId)
        {
            List<Blogs> Blogs = new List<Blogs>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetBlogsBySearchTextNew]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", System.Data.SqlDbType.VarChar, 500);
                    SearchText.Direction = System.Data.ParameterDirection.Input;
                    SearchText.Value = searchText;

                    SqlParameter appid = cmd.Parameters.Add("@AppId", System.Data.SqlDbType.Int);
                    appid.Direction = System.Data.ParameterDirection.Input;
                    appid.Value = AppId;

                    SqlParameter langid = cmd.Parameters.Add("@LanguageId", System.Data.SqlDbType.Int);
                    langid.Direction = System.Data.ParameterDirection.Input;
                    langid.Value = LanguageId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        Blogs v = new Blogs();
                        v.BlogId = (int)myData["BlogId"];
                        v.BlogName = (string)myData["BlogName"].ToString();
                        v.BlogDescription = (string)myData["BlogDesc"].ToString();
                        v.BlogUrl = (string)myData["BlogUrl"].ToString();
                        v.ThumbNailURL = (string)myData["ThumbNailURL"].ToString();
                        v.PublishedDate = (string)myData["PublishedDate"].ToString();
                        v.Comments = (int)myData["Comments"];
                        Blogs.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Blogs;
        }

     

        internal static List<FormulaDTO> GetFormula()

        {

            List<FormulaDTO> Formula = new List<FormulaDTO>();

            try

            {

                SqlConnection sqlConnection = ConnectTODB();

                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetFormula]", sqlConnection);

                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                while (sqlDataReader.Read())

                {

                    //var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);

                    FormulaDTO Formulas = new FormulaDTO();

                    Formulas.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);

                    Formulas.Name = sqlDataReader["Name"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Name"]);

                    //Formulas.DecodeFormula = sqlDataReader["DecodeFormula"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["DecodeFormula"]);

                    Formulas.BaseFormula = sqlDataReader["BaseFormula"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BaseFormula"]);

                    Formula.Add(Formulas);

                }

                sqlDataReader.Close();

                sqlConnection.Close();

            }


            catch (Exception e)

            {

                string s = e.Message;

                throw;

            }

            return Formula;

        }

        internal static List<FAQDTO> GetFAQs()
        {
            List<FAQDTO> faqs = new List<FAQDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetFAQs]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        FAQDTO v = new FAQDTO();
                        v.Id = (int)myData["Id"];
                        v.Question = (string)myData["Question"].ToString();
                        v.Value = (string)myData["Value"].ToString();
                        faqs.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return faqs;
        }

     
    }
}