﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models
{
    public class VimeoModel
    {
        #region VimeoVideos
        public class Paging
        {
            public object next { get; set; }
            public object previous { get; set; }
            public string first { get; set; }
            public string last { get; set; }
        }

        public class Embed
        {
            public string html { get; set; }
        }

        public class Privacy
        {
            public string view { get; set; }
            public string embed { get; set; }
            public bool download { get; set; }
            public bool add { get; set; }
            public string comments { get; set; }
        }

        public class Size
        {
            public int width { get; set; }
            public int height { get; set; }
            public string link { get; set; }
            public string link_with_play_button { get; set; }
        }

        public class Pictures
        {
            public string uri { get; set; }
            public bool active { get; set; }
            public string type { get; set; }
            public List<Size> sizes { get; set; }
            public string resource_key { get; set; }
        }

        public class Videos
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Connections
        {
            public Videos videos { get; set; }
        }

        public class Metadata
        {
            public Connections connections { get; set; }
        }

        public class Tag
        {
            public string uri { get; set; }
            public string name { get; set; }
            public string tag { get; set; }
            public string canonical { get; set; }
            public Metadata metadata { get; set; }
            public string resource_key { get; set; }
        }

        public class Stats
        {
            public int plays { get; set; }
        }

        public class Comments
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Credits
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Likes
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Pictures2
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Texttracks
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Related
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
        }

        public class Connections2
        {
            public Comments comments { get; set; }
            public Credits credits { get; set; }
            public Likes likes { get; set; }
            public Pictures2 pictures { get; set; }
            public Texttracks texttracks { get; set; }
            public Related related { get; set; }
        }

        public class Watchlater
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public bool added { get; set; }
            public object added_time { get; set; }
        }

        public class Like
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public bool added { get; set; }
            public object added_time { get; set; }
        }

        public class Interactions
        {
            public Watchlater watchlater { get; set; }
            public Like like { get; set; }
        }

        public class Metadata2
        {
            public Connections2 connections { get; set; }
            public Interactions interactions { get; set; }
        }

        public class Size2
        {
            public int width { get; set; }
            public int height { get; set; }
            public string link { get; set; }
        }

        public class Pictures3
        {
            public string uri { get; set; }
            public bool active { get; set; }
            public string type { get; set; }
            public List<Size2> sizes { get; set; }
            public string resource_key { get; set; }
        }

        public class Activities
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
        }

        public class Albums
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Appearances
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Channels
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Feed
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
        }

        public class Followers
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Following
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Groups
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Likes2
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class ModeratedChannels
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Portfolios
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Videos2
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Shared
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Pictures4
        {
            public string uri { get; set; }
            public List<string> options { get; set; }
            public int total { get; set; }
        }

        public class Connections3
        {
            public Activities activities { get; set; }
            public Albums albums { get; set; }
            public Appearances appearances { get; set; }
            public Channels channels { get; set; }
            public Feed feed { get; set; }
            public Followers followers { get; set; }
            public Following following { get; set; }
            public Groups groups { get; set; }
            public Likes2 likes { get; set; }
            public ModeratedChannels moderated_channels { get; set; }
            public Portfolios portfolios { get; set; }
            public Videos2 videos { get; set; }
            public Shared shared { get; set; }
            public Pictures4 pictures { get; set; }
        }

        public class Follow
        {
            public bool added { get; set; }
            public object added_time { get; set; }
            public string uri { get; set; }
        }

        public class Interactions2
        {
            public Follow follow { get; set; }
        }

        public class Metadata3
        {
            public Connections3 connections { get; set; }
            public Interactions2 interactions { get; set; }
        }

        public class Videos3
        {
            public object privacy { get; set; }
        }

        public class Preferences
        {
            public Videos3 videos { get; set; }
        }

        public class User
        {
            public string uri { get; set; }
            public string name { get; set; }
            public string link { get; set; }
            public string location { get; set; }
            public string bio { get; set; }
            public string created_time { get; set; }
            public string account { get; set; }
            public Pictures3 pictures { get; set; }
            public List<object> websites { get; set; }
            public Metadata3 metadata { get; set; }
            public string resource_key { get; set; }
            public Preferences preferences { get; set; }
        }

        public class App
        {
            public string name { get; set; }
            public string uri { get; set; }
        }

        public class Datum
        {
            public string uri { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public string link { get; set; }
            public int duration { get; set; }
            public int width { get; set; }
            public string language { get; set; }
            public int height { get; set; }
            public Embed embed { get; set; }
            public string created_time { get; set; }
            public string modified_time { get; set; }
            public string release_time { get; set; }
            public List<string> content_rating { get; set; }
            public string license { get; set; }
            public Privacy privacy { get; set; }
            public Pictures pictures { get; set; }
            public List<Tag> tags { get; set; }
            public Stats stats { get; set; }
            public Metadata2 metadata { get; set; }
            public User user { get; set; }
            public App app { get; set; }
            public string status { get; set; }
            public string resource_key { get; set; }
            public object embed_presets { get; set; }
        }

        public class VimeoVideos
        {
            public int total { get; set; }
            public int page { get; set; }
            public int per_page { get; set; }
            public Paging paging { get; set; }
            public List<Datum> data { get; set; }
        }
        

        #endregion
    }
}