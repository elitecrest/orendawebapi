﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Elitecrest.HtmlExtractor.Models;

namespace OrendaWebAPI.Models
{
    public class ProductsModel
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrendaDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static List<ProductsDTO> GetOrendaProducts(int language, int appId)
        {
            List<ProductsDTO> Products = new List<ProductsDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetOrendaProducts]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter flg = sqlCommand.Parameters.Add("@language", System.Data.SqlDbType.Int);
                flg.Direction = System.Data.ParameterDirection.Input;
                flg.Value = language;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@appId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    // var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    ProductsDTO Product = new ProductsDTO();
                    Product.ProductId = sqlDataReader["ProductId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ProductId"]);
                    Product.ProductUrl = sqlDataReader["ProductUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductUrl"]);
                    Product.ProductName = sqlDataReader["ProductName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductName"]);
                    //Product.SpanishProductName = sqlDataReader["SpanishProductName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["SpanishProductName"]);
                    Product.ProductDescription = sqlDataReader["Description"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Description"]);
                    Product.ShortDesc = sqlDataReader["ProductShortDesc"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductShortDesc"]);
                    //Product.ThumbnailUrl = pdate.ToString("dd MMMM yyyy");
                    Product.ThumbnailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Product.SDSSHeet = sqlDataReader["SDSSheets"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["SDSSheets"]);
                    Product.Feature = sqlDataReader["Feature"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Feature"]);
                    Product.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Product.CustomProductId = sqlDataReader["CustomProductId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["CustomProductId"]);
                    Product.Title = sqlDataReader["Title"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Title"]);
                    Products.Add(Product);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Products;
        } 
        internal static List<ProductsDTO> GetNaturalPoolProducts(int language, int appId)
        {
            List<ProductsDTO> Products = new List<ProductsDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetOrendaProducts]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter flg = sqlCommand.Parameters.Add("@language", System.Data.SqlDbType.Int);
                flg.Direction = System.Data.ParameterDirection.Input;
                flg.Value = language;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@appId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    // var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    ProductsDTO Product = new ProductsDTO();
                    Product.ProductId = sqlDataReader["ProductId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ProductId"]);
                    Product.ProductUrl = sqlDataReader["ProductUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductUrl"]);
                    Product.ProductName = sqlDataReader["ProductName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductName"]);
                    Product.SpanishProductName = sqlDataReader["SpanishProductName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["SpanishProductName"]);
                    Product.ShortDesc = sqlDataReader["ProductShortDesc"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductShortDesc"]);
                    Product.ProductDescription = sqlDataReader["Description"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Description"]);
                    //Product.ThumbnailUrl = pdate.ToString("dd MMMM yyyy");
                    Product.ThumbnailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Product.SDSSHeet = sqlDataReader["SDSSheets"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["SDSSheets"]);
                    Product.Feature = sqlDataReader["Feature"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Feature"]);
                    Product.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Product.CustomProductId = sqlDataReader["CustomProductId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["CustomProductId"]);
                    Products.Add(Product);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Products;
        }

     

        internal static List<ProductsDTO> GetNextGenerationProducts(int language, int appId)
        {
            List<ProductsDTO> Products = new List<ProductsDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetOrendaProducts]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter flg = sqlCommand.Parameters.Add("@language", System.Data.SqlDbType.Int);
                flg.Direction = System.Data.ParameterDirection.Input;
                flg.Value = language;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@appId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    // var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    ProductsDTO Product = new ProductsDTO();
                    Product.ProductId = sqlDataReader["ProductId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ProductId"]);
                    Product.ProductUrl = sqlDataReader["ProductUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductUrl"]);
                    Product.ProductName = sqlDataReader["ProductName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductName"]);
                    Product.SpanishProductName = sqlDataReader["SpanishProductName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["SpanishProductName"]);
                    Product.ProductDescription = sqlDataReader["Description"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Description"]);
                    Product.ShortDesc = sqlDataReader["ProductShortDesc"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ProductShortDesc"]);
                    //Product.ThumbnailUrl = pdate.ToString("dd MMMM yyyy");
                    Product.ThumbnailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    Product.SDSSHeet = sqlDataReader["SDSSheets"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["SDSSheets"]);
                    Product.Feature = sqlDataReader["Feature"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Feature"]);
                    Product.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    Product.CustomProductId = sqlDataReader["CustomProductId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["CustomProductId"]);
                    Products.Add(Product);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Products;
        }

        internal static List<ResourcesDTO> GetResources(int appId)
        {
            List<ResourcesDTO> resources = new List<ResourcesDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[GetResources]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure; 
             

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@appId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    // var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    ResourcesDTO resource  = new ResourcesDTO();
                    resource.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    resource.Name = sqlDataReader["Name"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Name"]);
                    resource.URL = sqlDataReader["URL"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["URL"]); 
                    resource.AppId = sqlDataReader["AppId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppId"]);
                    resources.Add(resource);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return resources;
        }

        internal static int AddProducts(OrendaProductModel product, int appId)
        {
            int Id = 0;
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[AddOrendaProducts]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter appIdPar = sqlCommand.Parameters.Add("@appId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId; 

                SqlParameter productUrlPar = sqlCommand.Parameters.Add("@ProductUrl", System.Data.SqlDbType.VarChar, 512);
                productUrlPar.Direction = System.Data.ParameterDirection.Input;
                productUrlPar.Value = product.ProductUrl;

                SqlParameter productNamePar = sqlCommand.Parameters.Add("@ProductName", System.Data.SqlDbType.VarChar, 256);
                productNamePar.Direction = System.Data.ParameterDirection.Input;
                productNamePar.Value = product.Name;


                SqlParameter descriptionPar = sqlCommand.Parameters.Add("@Description", System.Data.SqlDbType.VarChar, 1024);
                descriptionPar.Direction = System.Data.ParameterDirection.Input;
                descriptionPar.Value = product.Description;


                SqlParameter shortDescriptionPar = sqlCommand.Parameters.Add("@ShortDescription", System.Data.SqlDbType.VarChar, 1024);
                shortDescriptionPar.Direction = System.Data.ParameterDirection.Input;
                shortDescriptionPar.Value = product.ShortDesc; 


                SqlParameter thumbNailPar = sqlCommand.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 512);
                thumbNailPar.Direction = System.Data.ParameterDirection.Input;
                thumbNailPar.Value = product.ImageUrl;


                SqlParameter sDSSheets = sqlCommand.Parameters.Add("@SDSSheets", System.Data.SqlDbType.VarChar, 1024);
                sDSSheets.Direction = System.Data.ParameterDirection.Input;
                sDSSheets.Value = product.SDSSHeet;

                SqlParameter feature = sqlCommand.Parameters.Add("@Feature", System.Data.SqlDbType.VarChar, 100);
                feature.Direction = System.Data.ParameterDirection.Input;
                feature.Value = product.Feature;

                SqlParameter videoUrl = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
                videoUrl.Direction = System.Data.ParameterDirection.Input;
                videoUrl.Value = product.VideoUrl;


                  Id  = Convert.ToInt32(sqlCommand.ExecuteScalar());
               
               
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return Id;
        }
        internal static List<ResourcesDTO> GetResourcesV2(int appId,int LanguageId)
        {
            List<ResourcesDTO> resources = new List<ResourcesDTO>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[GetResourcesV2]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter appIdPar = sqlCommand.Parameters.Add("@appId", System.Data.SqlDbType.Int);
                appIdPar.Direction = System.Data.ParameterDirection.Input;
                appIdPar.Value = appId;

                SqlParameter lanpar = sqlCommand.Parameters.Add("@language", System.Data.SqlDbType.Int);
                lanpar.Direction = System.Data.ParameterDirection.Input;
                lanpar.Value = LanguageId;

                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    // var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    ResourcesDTO resource = new ResourcesDTO();
                    resource.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    resource.Name = sqlDataReader["Name"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["Name"]);
                    resource.URL = sqlDataReader["URL"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["URL"]);
                    resource.AppId = sqlDataReader["AppId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["AppId"]);
                    resource.LanguageId = sqlDataReader["Language"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Language"]);
                    resources.Add(resource);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return resources;
        }
    }
}