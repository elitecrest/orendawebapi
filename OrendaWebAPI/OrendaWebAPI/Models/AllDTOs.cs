﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models
{
    public partial class DeviceInfoDTO
    {
        public int DeviceId { get; set; }
        public string DeviceToken { get; set; }
        public int DeviceType { get; set; }  // Iphone:1,Android:2
        public DateTime CreatedDate { get; set; }
        public int AppID { get; set; }  // Orenda:1,Natural Pool:2,Next Gen:3
        public string RefDeviceToken { get;   set; }
    }

    public partial class DeviceDTO
    {
        public int Count { get; set; }
        public int DeviceType { get; set; }
        public string DeviceName { get; set; }
    }
    public partial class FeatureTrackingDto
    {
        public int FeatureId { get; set; }
        public int DeviceId { get; set; }
        public int DeviceType { get; set; }
        public string Value { get; set; }
        public int Count { get; set; }
        public int AppID { get; set; }  // Orenda:1,Natural Pool:2,Next Gen:3
    }
    public partial class FeatureTrackingDtoForVideos
    {
        //public int FeatureId { get; set; }
        //public int DeviceId { get; set; }
        //public int DeviceType { get; set; }
        //public int Value { get; set; }
        public string VideoUrl { get; set; }
        public string VideoName { get; set; }
        public string VideoDesc { get; set; }
        public int VideoViewCount { get; set; }
    }
    public partial class FeatureTrackingDtoForBlogs
    {
        //public int FeatureId { get; set; }
        //public int DeviceId { get; set; }
        //public int DeviceType { get; set; }
        //public int Value { get; set; }
        public string BlogName { get; set; }
        public string BlogUrl { get; set; }
        public string BlogDesc { get; set; }
        public int BlogViewCount { get; set; }
    }

    public partial class FeatureTrackingDtoForProducts
    {
        public string ProductName { get; set; }
        public int ProductViewCount { get; set; }
    }
    public partial class CustomResponse
    {
        public CustomResponseStatus Status;
        public Object Response;
        public string Message;
    }

    public enum CustomResponseStatus
    {
        Successful,
        UnSuccessful,
        Exception,
        Checked,
        PASSWORD_MISMATCH
    }

    public class YouTubeChannelDetails
    {
        public int Id { get; set; }
        public string VideoId { get; set; }
        public string VideoTitle { get; set; }
        public string VideoDescription { get; set; }
        public string VideoUrl { get; set; }
        public string ChannelId { get; set; }
        public string ThumbNailUrl { get; set; }
        public DateTime PublishedDate { get; set; }
        public string ChannelTitle { get; set; }
        public string Views { get; set; }
        public int LikesCount { get; set; }
        public int DisLikesCount { get; set; }
    }

    public class DosageDetails
    {
        public int DeviceId { get; set; }
        public int CalculatorType { get; set; } //Residential=1 and Commercial=2
        public int MeasurementVariations { get; set; } //Light = 1, Moderate = 2 and Heavy = 3
        public int MeasurementTypes { get; set; } //Gallons=1 and Litres = 2
        public long Quantity { get; set; }
        public double ChlorineNow { get; set; }
        public double ChlorineTarget { get; set; }
        public double PhNow { get; set; }
        public double PhTarget { get; set; }
        public int CalciumNow { get; set; }
        public int CalciumTarget { get; set; }
        public int AlkalinityNow { get; set; }
        public int AlkalinityTarget { get; set; }
        public int StabilizerNow { get; set; }
        public int StabilizerTarget { get; set; }
        public int SaltNow { get; set; }
        public int SaltTarget { get; set; }
        public int PhosphateNow { get; set; }
        public int PhosphateTarget { get; set; }
        public double IncreaseChlorineCalHypo { get; set; }
        public double IncreaseChlorineBleach { get; set; }
        public double DecreasepH { get; set; }
        public double IncreaseAlkalinity { get; set; }
        public double IncreaseCalcium { get; set; }
        public double IncreaseStabilizer { get; set; }
        public double IncreaseSaltLevel { get; set; }
        public int CV600Purge { get; set; }
        public int CV600Maintenance { get; set; }
        public int SC1000Purge { get; set; }
        public int SC1000Maintenance { get; set; }
        public int PR1000PhosphateLevel { get; set; }
    }

    public class PageInfo
    {
        public int totalResults { get; set; }
        public int resultsPerPage { get; set; }
    }

    public class Statistics
    {
        public string viewCount { get; set; }
        public string likeCount { get; set; }
        public string dislikeCount { get; set; }
        public string favoriteCount { get; set; }
        public string commentCount { get; set; }
    }

    public class Items
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public string id { get; set; }
        public Statistics statistics { get; set; }
    }

    public class RootObject
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public PageInfo pageInfo { get; set; }
        public List<Items> items { get; set; }
    }

    public class Blogs
    {
        public int BlogId { get; set; }
        public string BlogName { get; set; }
        public string BlogDescription { get; set; }
        public string BlogUrl { get; set; }
        public string ThumbNailURL { get; set; }
        public string PublishedDate { get; set; }
        public int Comments { get; set; }
    }
    public class OrendaBlogEntry
    {
        /// <summary>
        /// Blog Name
        /// </summary>
        public string BlogName { get; set; }

        /// <summary>
        /// Blog Description
        /// </summary>
        public string BlogDescription { get; set; }

        /// <summary>
        /// Blog url
        /// </summary>
        public string BlogUrl { get; set; }

        /// <summary>
        /// Thumbnail Url
        /// </summary>
        public string ThumbNailURL { get; set; }

        /// <summary>
        /// Published Date
        /// </summary>
        public string PublishedDate { get; set; }
    }
    public partial class FeatureTrackingListDto
    {
        public List<FeatureTrackingDto> featureList { get; set; }
    }

    public partial class FirebaseDTO
    {
        public string FirebaseAppId { get; set; }
        public string FirebaseSenderId { get; set; }
    }

    public class VimeoVideoDTO
    {
        public int ID { get; set; }
        public string VimeoVideoID { get; set; }
        public string VideoName { get; set; }
        public string VideoDesc { get; set; }
        public string VideoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public int Duration { get; set; }
        public DateTime PublishedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ViewCount { get; set; }
        public int Likes { get; set; }
        public int Comments { get; set; }
        public string VimeoUserName { get; set; }
    }

    public class ProductsDTO
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string SpanishProductName { get; set; }
        public string ProductUrl { get; set; }
        public string VideoUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string ProductDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string SDSSHeet { get; set; }
        public string Feature { get; set; }
        public string ShortDesc { get; set; }
        public string CustomProductId { get; set; }
        public string Title { get;   set; }
    }


    public class ResourcesDTO
    {
        public int  Id { get; set; }
        public string  Name { get; set; }
        public string URL { get; set; }  
        public int AppId { get; set; }  
        public int LanguageId { get; set; }

    }

    public class HelpVideos
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoURL { get; set; }
        public int Active { get; set; }
        public string Format { get; set; }
        public string ThumbnailURL { get; set; } 
        public int AppId { get; set; } 
        public int LanguageId { get; set; } //0-English 1-Spanish 2-French

    }
    //Wistia DTO's
    public class Thumbnail
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
        public string hashed_id { get; set; }
    }

    public class Asset
    {
        public string url { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public Int64 fileSize { get; set; }
        public string contentType { get; set; }
        public string type { get; set; }
    }

    public class RootObjectWistia
    {
        public Int64 id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string created { get; set; }
        public string updated { get; set; }
        public double duration { get; set; }
        public string hashed_id { get; set; }
        public string description { get; set; }
        public double progress { get; set; }
        public string status { get; set; }
        public Thumbnail thumbnail { get; set; }
        public Project project { get; set; }
        public List<Asset> assets { get; set; }
        public string embedCode { get; set; }
    }
    public class WistiaVideoDTO
    {
        public int ID { get; set; }
        public string WistiaVideoId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HashId { get; set; }
        public string VideoUrl { get; set; }
        public string ThumbNailUrl { get; set; }
        public string VideoUrlType { get; set; }
        public string ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectHashId { get; set; }
        public string Duration { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int AppId { get; set; }
        //View Count Data
        public int PageLoads { get; set; }
        public int visitors { get; set; }
       // public float PercentOfVisitorsClickingPlay { get; set; }
        public int Plays { get; set; }
        public int AveragePercentWatched { get; set; }
        public int LanguageId { get; set; }
        public int OrderId { get; set; }
    }
    public class Stats
    {
        public int pageLoads { get; set; }
        public int visitors { get; set; }
        public int percentOfVisitorsClickingPlay { get; set; }
        public int plays { get; set; }
        public int averagePercentWatched { get; set; }
    }

    public class StatsRootObject
    {
        public int id { get; set; }
        public string hashed_id { get; set; }
        public string name { get; set; }
        public Stats stats { get; set; }
    }

    public partial class PushLogDTO
    {
        public int TotalCount { get; set; }
        public int SuccessCount { get; set; }
        public string Message { get; set; }
        public int AppId { get; set; }
    }

    public partial class FormulaDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public string DecodeFormula { get; set; }
        public string BaseFormula { get; set; }
    }
    public class FAQDTO
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Value { get; set; }
    }
    public partial class SupportMailDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SSL { get; set; }

    }
    public partial class DosageCalData
    {
        public string CurrentTemp { get; set; }
        public string DesiredTemp { get; set; }
        public string CurrentChlorine { get; set; }
        public string DesiredChlorine { get; set; }
        public string CurrentPH { get; set; }
        public string DesiredPH { get; set; }
        public string CurrentCalcium { get; set; }
        public string DesiredCalcium { get; set; }
        public string CurrentAlkalinity { get; set; }
        public string DesiredAlkalinity { get; set; }
        public string CurrentStabilizer { get; set; }
        public string DesiredStabilizer { get; set; }
        public string CurrentSalt { get; set; }
        public string DesiredSalt { get; set; }
        public string CurrentPhospate { get; set; }
        public string DesiredPhospate { get; set; }
        public string CurrentLSI { get; set; }
        public string DesiredLSI { get; set; }
        public string CalculatorType { get; set; }

        public string PoolType { get; set; }
        public string PoolQuantity { get; set; }

        public string IncreaseChlorineValue1 { get; set; }
        public string IncreaseChlorineValue2 { get; set; }
        public string IncreaseChlorineTextByVaule1 { get; set; }
        public string IncreaseChlorineTextByVaule2 { get; set; }

        public string IncreasePH { get; set; } //null handle
        public string DecreasePH { get; set; } //null handle
        public string IncreaseAlkalinity { get; set; } //null handle
        public string DecreaseAlkalinity { get; set; }//null handle
        public string IncreaseCalcium { get; set; }
        public string IncreaseStabilizer { get; set; } //null handle
        public string IncreaseSalt { get; set; }//null handle

        public string CV600Purge { get; set; }
        public string CV600WeeklyMaintainence { get; set; }

        public string SC1000Purge { get; set; }
        public string SC1000ProductText { get; set; }
        public string SC1000ByWeeklyMaintainence { get; set; }  //only changes

        public string CEClarifierPurge { get; set; }
        public string CEClarifierByWeeklyMaintainence { get; set; }

        public string AsNeeded { get; set; }
        public string Emailaddress { get; set; }
        public int LanguageId { get; set; }  //0.english,1.spanish,2.french
        public string Measurement { get; set; }


        public string IncreaseChlorineContent1 { get; set; }
        public string IncreaseChlorineContent2 { get; set; }

        public string IncreasePHContent { get; set; }
        public string DecreasePHContent { get; set; }

        public string IncreaseAlkalinityContent { get; set; }
        public string DecreaseAlkalinityContent { get; set; }

        public string IncreaseSaltContent { get; set; }
        public string IncreaseCalciumContent { get; set; }
        public string IncreaseStabilizerContent { get; set; }

    }

    public partial class ShareDetailsDTO
    {
        public int Id { get; set; }
        public string Emailaddress { get; set; }
        public DateTime CreatedDate { get; set; }
        public int AppID { get; set; }
    }
}