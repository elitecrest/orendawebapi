﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace OrendaWebAPI.Models
{
    public class Videos
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrendaDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static int AddVideosFromYouTube(List<YouTubeChannelDetails> youtube)
        {
            var result = 0;
            try
            {
                foreach (var uTube in youtube)
                {
                    SqlConnection sqlConnection = ConnectTODB();
                    SqlCommand sqlCommand = new SqlCommand("[dbo].[GetYouTubeVideos]", sqlConnection);
                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    SqlParameter vID = sqlCommand.Parameters.Add("@VideoId", System.Data.SqlDbType.VarChar, 100);
                    vID.Direction = System.Data.ParameterDirection.Input;
                    vID.Value = uTube.VideoId.Trim();
                    SqlParameter vTitle = sqlCommand.Parameters.Add("@VideoTitle", System.Data.SqlDbType.VarChar, 100);
                    vTitle.Direction = System.Data.ParameterDirection.Input;
                    vTitle.Value = uTube.VideoTitle.Trim();
                    SqlParameter vDescription = sqlCommand.Parameters.Add("@VideoDescription", System.Data.SqlDbType.VarChar, 1024);
                    vDescription.Direction = System.Data.ParameterDirection.Input;
                    vDescription.Value = uTube.VideoDescription.Trim();
                    SqlParameter vURL = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
                    vURL.Direction = System.Data.ParameterDirection.Input;
                    vURL.Value = uTube.VideoUrl.Trim();
                    SqlParameter vThumbNail = sqlCommand.Parameters.Add("@VideoThumbNail", System.Data.SqlDbType.VarChar, 1024);
                    vThumbNail.Direction = System.Data.ParameterDirection.Input;
                    vThumbNail.Value = uTube.ThumbNailUrl.Trim();
                    SqlParameter pDate = sqlCommand.Parameters.Add("@PublishedDate", System.Data.SqlDbType.DateTime);
                    pDate.Direction = System.Data.ParameterDirection.Input;
                    pDate.Value = uTube.PublishedDate;
                    SqlParameter channelId = sqlCommand.Parameters.Add("@ChannelId", System.Data.SqlDbType.VarChar, 256);
                    channelId.Direction = System.Data.ParameterDirection.Input;
                    channelId.Value = uTube.ChannelId.Trim();
                    SqlParameter channelDesc = sqlCommand.Parameters.Add("@ChannelDescription", System.Data.SqlDbType.VarChar, 1024);
                    channelDesc.Direction = System.Data.ParameterDirection.Input;
                    channelDesc.Value = uTube.ChannelTitle.Trim();
                    SqlParameter viewcount = sqlCommand.Parameters.Add("@ViewsCount", System.Data.SqlDbType.Int);
                    viewcount.Direction = System.Data.ParameterDirection.Input;
                    viewcount.Value = Convert.ToInt32(uTube.Views);
                    SqlParameter Likes = sqlCommand.Parameters.Add("@LikesCount", System.Data.SqlDbType.Int);
                    Likes.Direction = System.Data.ParameterDirection.Input;
                    Likes.Value = uTube.LikesCount;
                    SqlParameter disLikes = sqlCommand.Parameters.Add("@DisLikesCount", System.Data.SqlDbType.Int);
                    disLikes.Direction = System.Data.ParameterDirection.Input;
                    disLikes.Value = uTube.DisLikesCount;
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader != null && sqlDataReader.Read())
                    {
                        result = sqlDataReader["result"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);
                    }
                    sqlDataReader.Close();
                    sqlConnection.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<YouTubeChannelDetails> GetVideos()
        {
            List<YouTubeChannelDetails> youtube = new List<YouTubeChannelDetails>();
            try
            {

                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetVideos]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    YouTubeChannelDetails uTube = new YouTubeChannelDetails();
                    uTube.Id = sqlDataReader["Id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["Id"]);
                    uTube.VideoId = sqlDataReader["VideoId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoId"]);
                    uTube.VideoTitle = sqlDataReader["VideoTitle"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoTitle"]);
                    uTube.VideoDescription = sqlDataReader["VideoDescription"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoDescription"]);
                    uTube.ChannelId = sqlDataReader["ChannelId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ChannelId"]);
                    uTube.ChannelTitle = sqlDataReader["ChannelTitle"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ChannelTitle"]);
                    uTube.VideoUrl = sqlDataReader["VideoUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["VideoUrl"]);
                    uTube.ThumbNailUrl = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    uTube.PublishedDate = sqlDataReader["PublishedDate"] == DBNull.Value ? DateTime.UtcNow.ToLocalTime() : Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    uTube.LikesCount = sqlDataReader["LikesCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["LikesCount"]);
                    uTube.DisLikesCount = sqlDataReader["DisLikesCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DisLikesCount"]);
                    RootObject statistics = new RootObject();
                    string url = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + uTube.VideoId + "&key=AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo";
                    JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                    serializer2.MaxJsonLength = 1000000000;
                    HttpClient client1 = new HttpClient();
                    client1.DefaultRequestHeaders.Accept.Clear();
                    client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response1 = client1.GetAsync(url).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        Stream st2 = response1.Content.ReadAsStreamAsync().Result;
                        StreamReader reader2 = new StreamReader(st2);
                        string content1 = reader2.ReadToEnd();
                        statistics = JsonDeserialize<RootObject>(content1);
                    }
                    if (statistics.items.Count > 0)
                    {
                        uTube.Views = Convert.ToString(statistics.items[0].statistics.viewCount) + " Views"; 
                    }
                    else
                    {
                        uTube.Views = string.Empty;
                    }
                    youtube.Add(uTube);

                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return youtube;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        internal static List<WistiaVideoDTO> GetVideosBySearch(string searchText)
        {
            List<WistiaVideoDTO> WistiaVideoDTO = new List<WistiaVideoDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetWistiaVideosBySearchText]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", System.Data.SqlDbType.VarChar, 500);
                    SearchText.Direction = System.Data.ParameterDirection.Input;
                    SearchText.Value = searchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        WistiaVideoDTO v = new WistiaVideoDTO();
                        v.ID = (int)myData["Id"];
                        v.WistiaVideoId = (string)myData["WistiaVideoId"].ToString();
                        v.Name = (string)myData["VideoName"].ToString();
                        v.Description = (string)myData["VideoDescription"].ToString();
                        v.HashId = (string)myData["VideoHashId"].ToString();
                        v.VideoUrl = (string)myData["VideoUrl"].ToString();
                        v.ThumbNailUrl = (string)myData["ThumbNailUrl"].ToString();
                        v.VideoUrlType = (string)myData["VideoUrlType"].ToString();
                        v.ProjectId = (string)myData["ProjectId"].ToString();
                        v.ProjectHashId = (string)myData["ProjectHashId"].ToString();
                        v.ProjectName = (string)myData["ProjectName"].ToString();
                        v.Duration = (string)myData["Duration"].ToString();
                        v.AppId = (int)myData["AppId"];
                        v.PageLoads = (int)myData["PageLoads"];
                        v.visitors = (int)myData["visitors"];
                        v.Plays = (int)myData["Plays"];
                        v.AveragePercentWatched = (int)myData["AveragePercentWatched"];
                        WistiaVideoDTO.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return WistiaVideoDTO;
        }


    }
}