﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models
{
    public class DosageModels
    {
        internal static void CalculateChlorine(double ChlorineNow, double ChlorineTarget, long Quantity, out double calHypo, out double bleach)
        {
            calHypo = 0.00021 * Quantity * (ChlorineTarget - ChlorineNow);
            bleach = 0.00106 * Quantity * (ChlorineTarget - ChlorineNow);
        }

        internal static double CalciumHardness(int CalciumNow, int CalciumTarget, long WaterVolume)
        {
            var temp = CalciumNow > CalciumTarget ? (CalciumNow - CalciumTarget) : (CalciumTarget - CalciumNow);
            return (0.000195 * WaterVolume * temp);
        }

        internal static double CalculateAlkalinity(int AlkalinityNow, int AlkalinityTarget, long WaterVolume)
        {
            var result = 0.00;
            if(AlkalinityTarget> AlkalinityNow)
            {
                result = (0.00023 * WaterVolume * (AlkalinityTarget - AlkalinityNow));
            }
            else if(AlkalinityNow > AlkalinityTarget)
            {
                result = (0.000256 * WaterVolume * (AlkalinityNow - AlkalinityTarget));
            }
            return result;
        }

        internal static double CalculateSaltLevel(int SaltNow, int SaltTarget, long WaterVolume)
        {
            var temp = SaltNow > SaltTarget ? (SaltNow - SaltTarget) : (SaltTarget - SaltNow);
            return (0.00008363 * WaterVolume * temp);
        }

        internal static double CalculatePhLevel(double PhNow, double PhTarget, long WaterVolume)
        {
            var temp = PhNow > PhTarget ? (PhNow - PhTarget) : (PhTarget - PhNow);
            return (0.000315 * (100 * 0.096) * WaterVolume * temp);
        }

        internal static double CalculateStabilizer(int StabilizerNow, int StabilizerTarget, long WaterVolume)
        {
            var temp = StabilizerNow > StabilizerTarget ? (StabilizerNow - StabilizerTarget) : (StabilizerTarget - StabilizerNow);
            return (0.00013 * WaterVolume * temp);
        }
    }
}