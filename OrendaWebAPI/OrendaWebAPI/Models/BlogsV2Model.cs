﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace OrendaWebAPI.Models
{
    public class BlogsV2Model
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["OrendaDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static List<Blogs> GetBlogsV2(int AppID)
        {
            List<Blogs> blogs = new List<Blogs>();
            try
            {
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[dbo].[GetBlogsV2]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AppID", AppID);
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    var pdate = Convert.ToDateTime(sqlDataReader["PublishedDate"]);
                    Blogs blog = new Blogs();
                    blog.BlogId = sqlDataReader["BlogId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["BlogId"]);
                    blog.BlogName = sqlDataReader["BlogName"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BlogName"]);
                    blog.BlogDescription = sqlDataReader["BlogDesc"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BlogDesc"]);
                    blog.BlogUrl = sqlDataReader["BlogUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["BlogUrl"]);
                    blog.ThumbNailURL = sqlDataReader["ThumbNailUrl"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["ThumbNailUrl"]);
                    blog.PublishedDate = pdate.ToString("dd MMMM yyyy");
                    blogs.Add(blog);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }

            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return blogs;
        }
    }
}