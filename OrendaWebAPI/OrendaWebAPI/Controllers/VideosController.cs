﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace OrendaWebAPI.Controllers
{
    public class VideosController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        [HttpGet]
        public CustomResponse GetYouTubeVideos()
        {
            int result = 0;
            try
            {
                var yt = new YouTubeService(new BaseClientService.Initializer() { ApiKey = "AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo" });
                var channelsListRequest = yt.Channels.List("contentDetails");
                channelsListRequest.ForUsername = "OrendaTechnologies";
                var channelsListResponse = channelsListRequest.Execute();
                int VideoCount = 1;
                foreach (var channel in channelsListResponse.Items)
                {
                    var uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;

                    var playlistItemsListRequest = yt.PlaylistItems.List("snippet");
                    playlistItemsListRequest.PlaylistId = uploadsListId;
                    playlistItemsListRequest.MaxResults = 50;
                    var playlistItemsListResponse = playlistItemsListRequest.Execute();
                    List<YouTubeChannelDetails> youtube = new List<YouTubeChannelDetails>();
                    RootObject statistics = new RootObject();
                    foreach (var playlistItem in playlistItemsListResponse.Items)
                    {
                        RootObject root = new RootObject();
                        YouTubeChannelDetails video = new YouTubeChannelDetails();
                        video.VideoId = playlistItem.Snippet.ResourceId.VideoId;
                        string url = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + video.VideoId + "&key=AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo";
                        JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                        serializer2.MaxJsonLength = 1000000000;
                        HttpClient client1 = new HttpClient();
                        client1.DefaultRequestHeaders.Accept.Clear();
                        client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage response1 = client1.GetAsync(url).Result;
                        if (response1.IsSuccessStatusCode)
                        {
                            Stream st2 = response1.Content.ReadAsStreamAsync().Result;
                            StreamReader reader2 = new StreamReader(st2);
                            string content1 = reader2.ReadToEnd();
                            statistics = JsonDeserialize<RootObject>(content1);
                        }
                        video.VideoTitle = playlistItem.Snippet.Title;
                        video.VideoDescription = playlistItem.Snippet.Description;
                        video.ChannelId = playlistItem.Snippet.ChannelId;
                        video.ChannelTitle = playlistItem.Snippet.ChannelTitle;
                        video.VideoUrl = "https://www.youtube.com/embed/" + playlistItem.Snippet.ResourceId.VideoId;
                        video.ThumbNailUrl = playlistItem.Snippet.Thumbnails.Medium.Url.ToString();
                        video.PublishedDate = Convert.ToDateTime(playlistItem.Snippet.PublishedAt.ToString());
                        video.Views = Convert.ToString(statistics.items[0].statistics.viewCount);
                        video.LikesCount = Convert.ToInt32(statistics.items[0].statistics.likeCount);
                        video.DisLikesCount = Convert.ToInt32(statistics.items[0].statistics.dislikeCount);
                        youtube.Add(video);
                    }
                    if (youtube.Count > 0)
                    {
                        result = Videos.AddVideosFromYouTube(youtube);
                    }

                }
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = result;
                customResponse.Message = Constants.FEATURE_SAVED;
            }
            catch (Exception e)
            {
                Console.WriteLine("Some exception occured" + e);
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetVideos()
        {
            try
            {
                var response = Videos.GetVideos();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [HttpGet]
        public CustomResponse GetVideosBySearch(string searchText)
        {
            try
            {

                List<WistiaVideoDTO> Blogs = Videos.GetVideosBySearch(searchText);
                if (Blogs.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = Blogs;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }
    }
}
