﻿using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace OrendaWebAPI.Controllers
{
    public class DeviceController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();

        [System.Web.Http.HttpPost]
        public CustomResponse RegisterDevice(DeviceInfoDTO deviceInfoDto)
        {
            try
            {
                int uId = 0;

                var currentDevice = DeviceModels.RegisterDevice(deviceInfoDto);

                if (currentDevice != null)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.DEVICE_REGISTERED;
                    customResponse.Response = currentDevice;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                    customResponse.Message = Constants.DEVICE_NOT_REGISTERED;
                }

                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpPost]
        public CustomResponse FeatureTracking(List<FeatureTrackingDto> featuresDtos)
        {
            try
            {

                foreach (FeatureTrackingDto featuresDto in featuresDtos)
                {
                    var response = DeviceModels.FeatureTracking(featuresDto);
                    if (response > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = response;
                        customResponse.Message = Constants.FEATURE_SAVED;

                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = null;
                        customResponse.Message = Constants.FEATURE_NOT_SAVED;
                    }
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetDeviceInformation()
        {
            try
            {
                var response = DeviceModels.GetDeviceInfo();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
        [HttpGet]
        public CustomResponse GetFeatureTrackingForBlogs()
        {
            try
            {
                var response = DeviceModels.GetFTForBlogs();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForBlogs_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForBlogs_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }
            return customResponse;
        }
        [HttpGet]
        public CustomResponse GetFeatureTrackingForProducts()
        {
            try
            {
                var response = DeviceModels.GetFTForProducts();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForProducts_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForProducts_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
        [HttpGet]
        public CustomResponse GetFeatureTrackingForVideos()
        {
            try
            {
                var response = DeviceModels.GetFTForVideos();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
        [HttpPost]
        public CustomResponse FeatureTrackingV1(FeatureTrackingListDto featureTrackingDto)
        {
            try
            {
                List<FeatureTrackingDto> featureTracking = featureTrackingDto.featureList;
                foreach (FeatureTrackingDto featuresDto in featureTracking)
                {
                    var response = DeviceModels.FeatureTracking(featuresDto);
                    if (response > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = response;
                        customResponse.Message = Constants.FEATURE_SAVED;

                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = null;
                        customResponse.Message = Constants.FEATURE_NOT_SAVED;
                    }
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetDeviceCountByDate(DateTime FromDate, DateTime ToDate)
        {
            try
            {
                var response = DeviceModels.GetDeviceCountByDate(FromDate, ToDate);

                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

    }
}
