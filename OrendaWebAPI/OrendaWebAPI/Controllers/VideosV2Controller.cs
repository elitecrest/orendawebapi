﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using static OrendaWebAPI.Models.VimeoModel;

namespace OrendaWebAPI.Controllers
{
    public class VideosV2Controller : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        [HttpGet]
        public CustomResponse GetYouTubeVideosV2()
        {
            int result = 0;
            try
            {
                var yt = new YouTubeService(new BaseClientService.Initializer() { ApiKey = "AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo" });
                var channelsListRequest = yt.Channels.List("contentDetails");
                channelsListRequest.ForUsername = "pureplanetscience";
                var channelsListResponse = channelsListRequest.Execute();
                int VideoCount = 1;
                foreach (var channel in channelsListResponse.Items)
                {
                    var uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;

                    var playlistItemsListRequest = yt.PlaylistItems.List("snippet");
                    playlistItemsListRequest.PlaylistId = uploadsListId.Trim();
                    // playlistItemsListRequest.Key = "AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo";
                    playlistItemsListRequest.MaxResults = 50;

                    var playlistItemsListResponse = playlistItemsListRequest.Execute();
                    List<YouTubeChannelDetails> youtube = new List<YouTubeChannelDetails>();
                    RootObject statistics = new RootObject();
                    foreach (var playlistItem in playlistItemsListResponse.Items)
                    {
                        RootObject root = new RootObject();
                        YouTubeChannelDetails video = new YouTubeChannelDetails();
                        video.VideoId = playlistItem.Snippet.ResourceId.VideoId;
                        string url = "https://www.googleapis.com/youtube/v3/videos?part=statistics&id=" + video.VideoId + "&key=AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo";
                        JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                        serializer2.MaxJsonLength = 1000000000;
                        HttpClient client1 = new HttpClient();
                        client1.DefaultRequestHeaders.Accept.Clear();
                        client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage response1 = client1.GetAsync(url).Result;
                        if (response1.IsSuccessStatusCode)
                        {
                            Stream st2 = response1.Content.ReadAsStreamAsync().Result;
                            StreamReader reader2 = new StreamReader(st2);
                            string content1 = reader2.ReadToEnd();
                            statistics = JsonDeserialize<RootObject>(content1);
                        }
                        video.VideoTitle = playlistItem.Snippet.Title;
                        video.VideoDescription = playlistItem.Snippet.Description;
                        video.ChannelId = playlistItem.Snippet.ChannelId;
                        video.ChannelTitle = playlistItem.Snippet.ChannelTitle;
                        video.VideoUrl = "https://www.youtube.com/embed/" + playlistItem.Snippet.ResourceId.VideoId;
                        video.ThumbNailUrl = playlistItem.Snippet.Thumbnails.Medium.Url.ToString();
                        video.PublishedDate = Convert.ToDateTime(playlistItem.Snippet.PublishedAt.ToString());
                        video.Views = Convert.ToString(statistics.items[0].statistics.viewCount);
                        video.LikesCount = Convert.ToInt32(statistics.items[0].statistics.likeCount);
                        video.DisLikesCount = Convert.ToInt32(statistics.items[0].statistics.dislikeCount);
                        youtube.Add(video);
                    }
                    if (youtube.Count > 0)
                    {
                        result = VideosV2Model.AddVideosFromYouTubeV2(youtube);
                    }

                }
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = result;
                customResponse.Message = Constants.FEATURE_SAVED;
            }
            catch (Exception e)
            {
                Console.WriteLine("Some exception occured" + e);
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetVideosV2()
        {
            try
            {
                var response = VideosV2Model.GetCustomAppVideos();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }


        [HttpGet]
        public CustomResponse GetVimeoVideosFromOrendauser()
        {
            int result = 0;
            try
            {
                WebClient myDownloader = new WebClient();
                myDownloader.Encoding = System.Text.Encoding.UTF8;
                myDownloader.Headers.Add("Authorization", "Bearer " + "5f752155244a91d63646991240a807fa");
                myDownloader.Headers.Add(HttpRequestHeader.ContentType, "application/vnd.vimeo.video+json");
                myDownloader.Headers.Add(HttpRequestHeader.Host, "api.vimeo.com");

                string jsonResponse = myDownloader.DownloadString("https://api.vimeo.com/users/66392608/videos");
                JavaScriptSerializer jss = new JavaScriptSerializer();

                VimeoVideos UsersList = jss.Deserialize<VimeoVideos>(jsonResponse);

                List<Datum> alldata = UsersList.data;
                List<VimeoVideoDTO> Videos = new List<VimeoVideoDTO>();
                foreach (var data in alldata)
                {
                    VimeoVideoDTO video = new VimeoVideoDTO();

                    var url = new Uri(data.link);
                    var lastSegment = url.Segments.Last();
                    video.VimeoVideoID = lastSegment;
                    video.VideoName = data.name;
                    video.VideoDesc = data.description;
                    video.VideoUrl = data.link;
                    video.ThumbnailUrl = data.pictures.sizes[1].link;
                    video.Duration = data.duration;
                    video.PublishedDate = Convert.ToDateTime(data.created_time);
                    video.ModifiedDate = Convert.ToDateTime(data.modified_time);
                    video.ViewCount = data.stats.plays;
                    video.Likes = Convert.ToInt32(data.metadata.connections.likes.total);
                    video.Comments = Convert.ToInt32(data.metadata.connections.comments.total);
                    video.VimeoUserName = data.user.name;
                    Videos.Add(video);
                }
                if (Videos.Count > 0)
                {
                    result = VideosV2Model.AddVideosFromVimeo(Videos);
                }
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = result;
                customResponse.Message = Constants.FEATURE_SAVED;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Some exception occured" + ex);
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetVimeoVideos()
        {
            try
            {
                var response = VideosV2Model.GetVimeoVideos();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse InsertShareFromEmailaddress(string emailaddress, int appId)
        {
            try
            {
                var response = VideosV2Model.InsertShareEmailaddress(emailaddress, appId);
                if (response > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.EMAIL_ADDED;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpPost]
        public CustomResponse AddHelpVideos(HelpVideos helpVideos)
        {
            try
            {
                var response = VideosV2Model.AddHelpVideos(helpVideos);
                if (response > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.EMAIL_ADDED;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetHelpVideos(int appId, int languageId)
        {
            try
            {
                var response = VideosV2Model.GetHelpVideos(appId, languageId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpPost]
        public CustomResponse UpdateActiveStatus(HelpVideos helpVideos)
        {
            try
            {
                var response = VideosV2Model.UpdateActiveStatus(helpVideos);
                if (response > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetActiveHelpVideo(int appId, int languageId)
        {
            try
            {
                var response = VideosV2Model.GetActiveHelpVideo(appId, languageId);
                if (response.Id > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
        [HttpGet]
        public CustomResponse GetWistiaFromOrenda()
        {
            int result = 0;
            try
            {
                string username = "eric@orendatech.com";
                string password = "e907e0dbbf8f8823e734c7907736b4417679fda985e93ea7a25b2808a199ae26";           
                WebClient myDownloader = new WebClient();
                myDownloader.Encoding = System.Text.Encoding.UTF8;
                string jsonResponse = myDownloader.DownloadString("https://api.wistia.com/v1/medias.json?api_password=e907e0dbbf8f8823e734c7907736b4417679fda985e93ea7a25b2808a199ae26");
                JavaScriptSerializer jss = new JavaScriptSerializer();
                List<RootObjectWistia> UsersList = jss.Deserialize<List<RootObjectWistia>>(jsonResponse);
                var itemToRemove = UsersList.Single(r => r.name == "How to use the Orenda App and LSI Calculator");
                UsersList.Remove(itemToRemove);
                var SpanishitemToRemove = UsersList.Single(r => r.name == "Guía de Orenda aplicaciones en español");
                UsersList.Remove(SpanishitemToRemove);
                List<Project> proj = new List<Project>();
                foreach (var p in UsersList)
                {   
                    proj.Add(p.project);
                }           
                List<WistiaVideoDTO> vidoes = new List<WistiaVideoDTO>();
                foreach (var User in UsersList)
                {
                    WistiaVideoDTO video = new WistiaVideoDTO();
                    video.WistiaVideoId = Convert.ToString(User.id);
                    video.Name = User.name;
                    video.CreatedDate = Convert.ToDateTime(User.created);
                    video.UpdatedDate = Convert.ToDateTime(User.updated);
                    video.Duration = Convert.ToString(User.duration);
                    video.HashId = User.hashed_id;                
                    video.Description = User.description;
                    //video.VideoUrl = User.assets[1].url;
                    video.VideoUrl = "https://orendatech.wistia.com/medias/" + video.HashId + "?embedType=async&videoFoam=true&videoWidth=640";
                    video.VideoUrlType = User.assets[1].type;
                    video.ThumbNailUrl = User.thumbnail.url;
                    video.ProjectId = Convert.ToString(User.project.id);
                    video.ProjectHashId = Convert.ToString(User.project.hashed_id);
                    if (video.ProjectHashId == "4ojl20acqh")
                    {
                        video.AppId = 1;
                    }
                    else if (video.ProjectHashId == "7qlydn248h")
                    {
                        video.AppId = 2;
                    }
                    else if (video.ProjectHashId == "nf968nya1u")
                    {
                        video.AppId = 3;
                    }
                    video.ProjectName = Convert.ToString(User.project.name);
                    WebClient myDownloader1 = new WebClient();
                    myDownloader1.Encoding = System.Text.Encoding.UTF8;
                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                    myDownloader1.Headers.Add("Authorization", "Basic " + svcCredentials);
                    string Basestatusurl = "https://api.wistia.com/v1/medias/";
                     string remaining= Basestatusurl + video.HashId + "/stats.json";
                    string jsonResponse1 = myDownloader1.DownloadString(remaining);
                    JavaScriptSerializer jss1 = new JavaScriptSerializer();
                    StatsRootObject VideoStatus = jss1.Deserialize<StatsRootObject>(jsonResponse1);
                    video.AveragePercentWatched = VideoStatus.stats.averagePercentWatched;
                    video.PageLoads = VideoStatus.stats.pageLoads;
                    video.visitors = VideoStatus.stats.visitors;
                    video.Plays = VideoStatus.stats.plays;
                    vidoes.Add(video);
                }
                if (vidoes.Count >0)
                {
                    result = VideosV2Model.AddWistiaVideos(vidoes);
                }
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = result;
                customResponse.Message = Constants.Videos_Fetched;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Some exception occured" + ex);
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetWistiaVideos(int AppId)
        {
            try
            {
                var response = VideosV2Model.GetWistiaVideos(AppId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetWistiaFromOrendaV2()
        {
            int result = 0;
            try
            {
                var dbVideos = VideosV2Model.GetWistiaVideosFromDB();
                string username = "eric@orendatech.com";
                string password = "e907e0dbbf8f8823e734c7907736b4417679fda985e93ea7a25b2808a199ae26";
                WebClient myDownloader = new WebClient();
                myDownloader.Encoding = System.Text.Encoding.UTF8;
                string jsonResponse = myDownloader.DownloadString("https://api.wistia.com/v1/medias.json?api_password=e907e0dbbf8f8823e734c7907736b4417679fda985e93ea7a25b2808a199ae26");
                JavaScriptSerializer jss = new JavaScriptSerializer();
                List<RootObjectWistia> UsersList = jss.Deserialize<List<RootObjectWistia>>(jsonResponse);
                var itemToRemove = UsersList.Single(r => r.name == "How to use the Orenda App and LSI Calculator");
                UsersList.Remove(itemToRemove);
                var SpanishitemToRemove = UsersList.Single(r => r.name == "Guía de Orenda aplicaciones en español");
                UsersList.Remove(SpanishitemToRemove);
                List<Project> proj = new List<Project>();
                foreach (var p in UsersList)
                {
                    proj.Add(p.project);
                }
                List<WistiaVideoDTO> vidoes = new List<WistiaVideoDTO>();
                foreach (var User in UsersList)
                {
                    WistiaVideoDTO video = new WistiaVideoDTO();
                    video.WistiaVideoId = Convert.ToString(User.id);
                    video.Name = User.name;
                    video.CreatedDate = Convert.ToDateTime(User.created);
                    video.UpdatedDate = Convert.ToDateTime(User.updated);
                    video.Duration = Convert.ToString(User.duration);
                    video.HashId = User.hashed_id;
                    video.Description = User.description;
                    video.VideoUrl = "https://orendatech.wistia.com/medias/" + video.HashId + "?embedType=async&videoFoam=true&videoWidth=640";
                    video.VideoUrlType = User.assets[1].type;
                    video.ThumbNailUrl = User.thumbnail.url;
                    video.ProjectId = Convert.ToString(User.project.id);
                    video.ProjectHashId = Convert.ToString(User.project.hashed_id);
                    video.ProjectName = Convert.ToString(User.project.name);
                    WebClient myDownloader1 = new WebClient();
                    myDownloader1.Encoding = System.Text.Encoding.UTF8;
                    string svcCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(username + ":" + password));
                    myDownloader1.Headers.Add("Authorization", "Basic " + svcCredentials);
                    string Basestatusurl = "https://api.wistia.com/v1/medias/";
                    string remaining = Basestatusurl + video.HashId + "/stats.json";
                    string jsonResponse1 = myDownloader1.DownloadString(remaining);
                    JavaScriptSerializer jss1 = new JavaScriptSerializer();
                    StatsRootObject VideoStatus = jss1.Deserialize<StatsRootObject>(jsonResponse1);
                    video.AveragePercentWatched = VideoStatus.stats.averagePercentWatched;
                    video.PageLoads = VideoStatus.stats.pageLoads;
                    video.visitors = VideoStatus.stats.visitors;
                    video.Plays = VideoStatus.stats.plays;
                    if (video.ProjectHashId == "4ojl20acqh")  //Proj Name="Orenda Videos(English)"
                    {
                        video.AppId = 1;
                        video.LanguageId = 0;  //Set For Eng Videos in Orenda
                        vidoes.Add(video);
                    }
                    else if (video.ProjectHashId == "br61rso9zz")   //Proj Name="Orenda Videos(Spanish)"
                    {
                        video.AppId = 1;
                        video.LanguageId = 1;  //Set For Spanish Videos in Orenda
                        vidoes.Add(video);
                    }
                    else if (video.ProjectHashId == "7qlydn248h")   //Proj Name="Natural Pool Products(English)"
                    {
                        video.AppId = 2;
                        video.LanguageId = 0;   //Set Default eng for all videos in natural pool and next gen app
                        vidoes.Add(video);
                    }
                    else if (video.ProjectHashId == "nf968nya1u")   //Proj Name="Next Generation Videos(English)"
                    {
                        video.AppId = 3;
                        video.LanguageId = 0; //Set Default eng for all videos in natural pool and next gen app
                        vidoes.Add(video);
                    }
                }
                if (vidoes.Count > 0)
                {
                    result = VideosV2Model.AddWistiaVideosV2(vidoes);
                }
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = result;
                customResponse.Message = Constants.Videos_Fetched;

               DeleteNonExistingVideos(UsersList, dbVideos);

                //Project Id's in Orenda Wistia Channel'
                // 3099160      nf968nya1u   Next Generation Videos
                //3109315       4ojl20acqh   Orenda Videos
                //3111694       7qlydn248h   Natural Pool Products
                //3367998      3uh1ls52jf   Hubspot Email Video Content
                //3634566       2ub11bu74w  Orenda Private content
                //3634584       br61rso9zz  Orenda Español Videos

            }
            catch (Exception ex)
            {
                Console.WriteLine("Some exception occured" + ex);
            }
            return customResponse;
        }

        private void DeleteNonExistingVideos(List<RootObjectWistia> Videos, List<WistiaVideoDTO> DBVideos)
        {
            bool exists = true;
            foreach (var b in DBVideos)
            {
                exists = Videos.Any(x => x.name == b.Name);
                if (!exists)
                {
                    VideosV2Model.DeleteWistiaVideo(b.Name);
                }
            }
        }

        [HttpGet]
        public CustomResponse GetWistiaVideosV2(int AppId, int LanguageId)
        {
            try
            {
                var response = VideosV2Model.GetWistiaVideosV2(AppId, LanguageId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpGet]
        public CustomResponse GetWistiaVideosWithSearch(int AppId, int LanguageId,string searchText)
        {
            try
            {
                
                   //var helpVideos = VideosV2Model.GetActiveHelpVideo(AppId, LanguageId); 
                   var response = VideosV2Model.GetWistiaVideosWithSearch(AppId, LanguageId, searchText);
                    //response = AddHelpVideoToWistia(helpVideos, response);
                    //response = response.OrderByDescending(x => x.OrderId).ToList();
                 

                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }

            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        private List<WistiaVideoDTO> AddHelpVideoToWistia(HelpVideos helpVideos, List<WistiaVideoDTO> response)
        {
            
            WistiaVideoDTO outVideos = new WistiaVideoDTO();
            outVideos.VideoUrl = helpVideos.VideoURL;
            outVideos.ThumbNailUrl = helpVideos.ThumbnailURL;
            outVideos.Name = helpVideos.Name;
            outVideos.AppId = helpVideos.AppId;
            outVideos.LanguageId = helpVideos.LanguageId;
            outVideos.OrderId = 1;
            response.Add(outVideos);
            return response;
        }

        [HttpGet]
        public CustomResponse AddDeviceLanguage(int DeviceId, int LanguageId)
        {
            try
            {
                VideosV2Model.AddDeviceLanguage(DeviceId, LanguageId);

                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = DeviceId;
                customResponse.Message = Constants.Videos_Fetched;


            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpPost]
        public CustomResponse SendMail(DosageCalData dosage)
        {
            SupportMailDTO support = VideosV2Model.GetSupportMails("SupportEmail");
            MailMessage Msg = new MailMessage();
            try
            {
                if (dosage.CalculatorType == "1")
                {
                    if (dosage.LanguageId == 0)
                    {
                        dosage.CalculatorType = "Residential";
                        dosage.SC1000ProductText = "Bi-Weekly Maintainance";
                    }
                    else if (dosage.LanguageId == 1)
                    {
                        dosage.CalculatorType = "Residencial";
                        dosage.SC1000ProductText = "Mantenimiento bisemanal";
                    }
                    else
                    {
                        dosage.CalculatorType = "Résidentiel";
                        dosage.SC1000ProductText = "Maintenance bi-hebdomadaire";
                    }
                }
                else
                {
                    if (dosage.LanguageId == 0)
                    {
                        dosage.CalculatorType = "Commercial";
                        dosage.SC1000ProductText = "Weekly Maintainance";
                    }
                    else if (dosage.LanguageId == 1)
                    {
                        dosage.CalculatorType = "Comercial";
                        dosage.SC1000ProductText = "Mantenimiento semanal";
                    }
                    else
                    {
                        dosage.CalculatorType = "Commercial";
                        dosage.SC1000ProductText = "Entretien hebdomadaire";
                    }
                }
                if (dosage.LanguageId == 0)
                {
                    if (dosage.PoolType == "gallons")
                    {
                        // dosage.Measurement = "Ounces";
                        dosage.Measurement = "Ounces";
                        dosage.IncreaseChlorineTextByVaule1 = "oz";
                        dosage.IncreaseChlorineTextByVaule2 = "oz";
                    }
                    else
                    {
                        // dosage.Measurement = "Liters";
                        dosage.Measurement = "mL";
                        dosage.IncreaseChlorineTextByVaule1 = "g";
                        dosage.IncreaseChlorineTextByVaule2 = "mL";
                    }
                }
                else if (dosage.LanguageId == 1)
                {
                    if (dosage.PoolType == "galones")
                    {
                        dosage.Measurement = "onzas";
                        dosage.IncreaseChlorineTextByVaule1 = "oz";
                        dosage.IncreaseChlorineTextByVaule2 = "oz";
                    }
                    else
                    {
                        // dosage.Measurement = "litros";
                        dosage.Measurement = "mL";
                        dosage.IncreaseChlorineTextByVaule1 = "g";
                        dosage.IncreaseChlorineTextByVaule2 = "mL";
                    }
                }
                else
                {
                    if (dosage.PoolType == "gallons")
                    {
                        dosage.Measurement = "onces";
                        dosage.IncreaseChlorineTextByVaule1 = "oz";
                        dosage.IncreaseChlorineTextByVaule2 = "oz";
                    }
                    else
                    {
                        // dosage.Measurement = "litres";
                        dosage.Measurement = "mL";
                        dosage.IncreaseChlorineTextByVaule1 = "g";
                        dosage.IncreaseChlorineTextByVaule2 = "mL";
                    }
                }
                StreamReader reader = null;
                if (dosage.LanguageId == 0)
                {
                    reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EngMail.html"));

                }
                else if (dosage.LanguageId == 1)
                {
                    reader = new StreamReader(HttpContext.Current.Server.MapPath("~/SpanMail.html"));
                }
                else
                {
                    reader = new StreamReader(HttpContext.Current.Server.MapPath("~/FrenMail.html"));
                }
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$CalType$$", dosage.CalculatorType);
                myString = myString.Replace("$$PoolQuan$$", dosage.PoolQuantity);
                myString = myString.Replace("$$PoolMeasure$$", dosage.PoolType);  //


                myString = myString.Replace("$$CurrentTemp$$", dosage.CurrentTemp);
                myString = myString.Replace("$$DesiredTemp$$", dosage.DesiredTemp);

                myString = myString.Replace("$$CurrentChlorine$$", dosage.CurrentChlorine);
                myString = myString.Replace("$$DesiredChlorine$$", dosage.DesiredChlorine);

                myString = myString.Replace("$$CurrentPH$$", dosage.CurrentPH);
                myString = myString.Replace("$$DesiredPH$$", dosage.DesiredPH);

                myString = myString.Replace("$$CurrentCalcium$$", dosage.CurrentCalcium);
                myString = myString.Replace("$$DesiredCalcium$$", dosage.DesiredCalcium);

                myString = myString.Replace("$$CurrentAlkalinity$$", dosage.CurrentAlkalinity);
                myString = myString.Replace("$$DesiredAlkalinity$$", dosage.DesiredAlkalinity);

                myString = myString.Replace("$$CurrentStabilizer$$", dosage.CurrentStabilizer);
                myString = myString.Replace("$$DesiredStabilizer$$", dosage.DesiredStabilizer);

                myString = myString.Replace("$$CurrentSalt$$", dosage.CurrentSalt);
                myString = myString.Replace("$$DesiredSalt$$", dosage.DesiredSalt);

                myString = myString.Replace("$$CurrentPhospate$$", dosage.CurrentPhospate);
                myString = myString.Replace("$$DesiredPhospate$$", dosage.DesiredPhospate);

                myString = myString.Replace("$$CurrentLSI$$", dosage.CurrentLSI);
                myString = myString.Replace("$$DesiredLSI$$", dosage.DesiredLSI);

                myString = myString.Replace("$$CV600Purge$$", dosage.CV600Purge);
                myString = myString.Replace("$$CV600WeeklyMaintainence$$", dosage.CV600WeeklyMaintainence);

                myString = myString.Replace("$$SC1000Purge$$", dosage.SC1000Purge);
                myString = myString.Replace("$$SC1000ByWeeklyMaintainence$$", dosage.SC1000ByWeeklyMaintainence);
                myString = myString.Replace("$$SC1000ProductText$$", dosage.SC1000ProductText);

                myString = myString.Replace("$$AsNeeded$$", dosage.AsNeeded);

                myString = myString.Replace("$$CEClarifierPurge$$", dosage.CEClarifierPurge);
                myString = myString.Replace("$$CEClarifierByWeeklyMaintainence$$", dosage.CEClarifierByWeeklyMaintainence);

                myString = myString.Replace("$$Measurement$$", dosage.Measurement);

                Msg.From = new MailAddress(support.EmailAddress);


                myString = myString.Replace("$$IncreaseChlorineValue1$$", dosage.IncreaseChlorineValue1);
                myString = myString.Replace("$$IncreaseChlorineValue2$$", dosage.IncreaseChlorineValue2);
                myString = myString.Replace("$$IncreaseChlorineTextByVaule1$$", dosage.IncreaseChlorineTextByVaule1);
                myString = myString.Replace("$$IncreaseChlorineTextByVaule2$$", dosage.IncreaseChlorineTextByVaule2);

                myString = myString.Replace("$$IncreasePH$$", dosage.IncreasePH);
                myString = myString.Replace("$$DecreasePH$$", dosage.DecreasePH);
                myString = myString.Replace("$$IncreaseAlkalinity$$", dosage.IncreaseAlkalinity);
                myString = myString.Replace("$$DecreaseAlkalinity$$", dosage.DecreaseAlkalinity);

                myString = myString.Replace("$$IncreaseCalcium$$", dosage.IncreaseCalcium);
                myString = myString.Replace("$$IncreaseSalt$$", dosage.IncreaseSalt);
                myString = myString.Replace("$$IncreaseStabilizer$$", dosage.IncreaseStabilizer);


                myString = myString.Replace("$$IncreaseChlorineContent1$$", dosage.IncreaseChlorineContent1);
                myString = myString.Replace("$$IncreaseChlorineContent2$$", dosage.IncreaseChlorineContent2);

                myString = myString.Replace("$$IncreasePHContent$$", dosage.IncreasePHContent);
                myString = myString.Replace("$$DecreasePHContent$$", dosage.DecreasePHContent);

                myString = myString.Replace("$$IncreaseAlkalinityContent$$", dosage.IncreaseAlkalinityContent);
                myString = myString.Replace("$$DecreaseAlkalinityContent$$", dosage.DecreaseAlkalinityContent);

                myString = myString.Replace("$$IncreaseCalciumContent$$", dosage.IncreaseCalciumContent);
                myString = myString.Replace("$$IncreaseStabilizerContent$$", dosage.IncreaseStabilizerContent);
                myString = myString.Replace("$$IncreaseSaltContent$$", dosage.IncreaseSaltContent);

                Msg.To.Add(dosage.Emailaddress);
                Msg.Subject = " Orenda Dosage Calculator";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient();
                smtpMail.Host = support.Host;
                smtpMail.Port = Convert.ToInt32(support.Port);
                smtpMail.EnableSsl = Convert.ToBoolean(support.SSL);
                smtpMail.UseDefaultCredentials = false;
                smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpMail.Credentials = new NetworkCredential(Convert.ToString(support.EmailAddress), Convert.ToString(support.Password));
                smtpMail.Timeout = 1000000;
                smtpMail.Send(Msg);
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Response = true;
                customResponse.Message = "Mail Sent Successfully!!!";
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpGet]
        public CustomResponse GetShareEmails(int AppId)
        {
            try
            {
                var response = VideosV2Model.GetShareEmails(AppId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Videos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = null;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
    }
}
