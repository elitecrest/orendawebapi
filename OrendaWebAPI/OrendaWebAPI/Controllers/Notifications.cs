﻿using OrendaWebAPI.Models;
using System;
using System.Collections.Generic; 
using System.Web.Http;


namespace OrendaWebAPI.Controllers
{ 
    public class NotificationsController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();

        [HttpGet]
        public CustomResponse PushNotification(int AppId,string message)
        {
            try
            {
                List<string> deviceNames = VideosV2Model.GetDeviceDetails(AppId);
                foreach (var device in deviceNames)
                { 
                    bool Status = VideosV2Model.PushNotification(device, message, AppId);
                    if (Status == true)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Message = Constants.PushNotification_Sent_Successfully;
                    }
                }
                return customResponse;
            }
            catch (Exception ex)
            { 
                return customResponse;
            }
        }

        [HttpGet]
        public CustomResponse GetDeviceDetails(int AppId)
        {
            try
            {
                List<string> deviceNames = VideosV2Model.GetDeviceDetails(AppId);
                //foreach (var device in deviceNames)
                //{
                //    bool Status = VideosV2Model.PushNotification(device, message, AppId);
                //    if (Status == true)
                //    {
                //        customResponse.Status = CustomResponseStatus.Successful;
                //        customResponse.Message = Constants.PushNotification_Sent_Successfully;
                //    }
                //}
                customResponse.Response = deviceNames;
                customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                customResponse.Status = CustomResponseStatus.Successful;
                return customResponse;
            }
            catch (Exception ex)
            {
                return customResponse;
            }
        }

        [HttpGet]
        public CustomResponse GetFirebaseDetails(int AppId)
        {
            try
            {
                 FirebaseDTO  firebaseDetails = VideosV2Model.GetFirebaseCredentials(AppId);
                //foreach (var device in deviceNames)
                //{
                //    bool Status = VideosV2Model.PushNotification(device, message, AppId);
                //    if (Status == true)
                //    {
                //        customResponse.Status = CustomResponseStatus.Successful;
                //        customResponse.Message = Constants.PushNotification_Sent_Successfully;
                //    }
                //}
                customResponse.Response = firebaseDetails;
                customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                customResponse.Status = CustomResponseStatus.Successful;
                return customResponse;
            }
            catch (Exception ex)
            {
                return customResponse;
            }
        }

        [HttpPost]
        public CustomResponse AddPushLog(PushLogDTO pushDto)
        {
            try
            {
                var res = VideosV2Model.AddPushLog(pushDto);
                if (res > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                    customResponse.Response = res;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;

                }

                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }
    }
}
