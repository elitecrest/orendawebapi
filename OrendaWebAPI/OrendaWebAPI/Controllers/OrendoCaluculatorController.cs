﻿using OrendaWebAPI.Models;
using OrendaWebAPI.Models.OrendaCalcModals;
using System.Collections.Generic;
using System.Web.Http;


namespace OrendaWebAPI.Controllers
{
    public class OrendoCaluculatorController : ApiController
    {


        #region Api

        [HttpPost]
        public OrendaCalcResponse CaluculateResult(OrendoCalcModel model)
        {
            var res = new OrendaCalcResponse();
            res.DosageItems = new List<OrendaComponentDosageItem>();

            //chlorine
            var chlorineRes = OrendoCalcHelpers.CaluculateChlorine(model.GallonsOfWater, model.CurrentChlorine, model.DesiredChlorine);

            foreach (var item in chlorineRes)
            {
                res.DosageItems.Add(new OrendaComponentDosageItem
                {
                    Component = "Increase Chlorine*",
                    DosageQty = item.Value.ToString(),
                    QtyDescription = item.Key
                });
            }

            // pH
            if (model.CurrentAlkalinity <= 150)
            {
                res.DosageItems.Add(new OrendaComponentDosageItem
                {
                    Component = model.CurrentpH > model.DesiredpH ? "Decrease pH**" : "Increase pH**",
                    DosageQty = OrendoCalcHelpers.CaluculatepH(model.GallonsOfWater, model.CurrentpH, model.DesiredpH, model.CurrentAlkalinity).ToString(),
                    QtyDescription = model.CurrentpH > model.DesiredpH ? "oz 31.45% Baume Muriatic Acid (by volume,column pour)" : "oz of soda ash (by volume)."
                });

            }
            else
            {
                res.DosageItems.Add(new OrendaComponentDosageItem
                {
                    Component =   "pH",
                    DosageQty = "--",
                    QtyDescription = "N/A"
                });
            }
            // alKalinity 
                res.DosageItems.Add(new OrendaComponentDosageItem
                {
                    Component = model.CurrentAlkalinity > model.DesiredAlkalinity ? "Decrease Alkalinity**" : "Increase Alkalinity**",
                    DosageQty = OrendoCalcHelpers.CaluculateAlkalinity(model.GallonsOfWater, model.CurrentAlkalinity, model.DesiredAlkalinity, true).ToString(),
                    QtyDescription = model.CurrentAlkalinity > model.DesiredAlkalinity ? "oz 31.45% Baume Muriatic Acid (by volume)" : "lbs of Sodium Bicarb."
                }); 
           
             
            // calcium
            res.DosageItems.Add(new OrendaComponentDosageItem
            {
                Component = "Increase Calcium",
                DosageQty = OrendoCalcHelpers.CaluculateCalcium(model.GallonsOfWater, model.CurrentCalcium, model.DesiredCalcium, true).ToString(),
                QtyDescription = "lbs of Calcium Chloride Dihydrate 77%-80% (by weight)"
            });

            // stabilizer
            res.DosageItems.Add(new OrendaComponentDosageItem
            {
                Component = "Increase Stabilizer",
                DosageQty = OrendoCalcHelpers.CaluculateCyanuricAcid(model.GallonsOfWater, model.CurrentStabilizer, model.DesiredStabilizer, true).ToString(),
                QtyDescription = "lbs of Cyanuric acid (by weight)"
            });

            // salt
            res.DosageItems.Add(new OrendaComponentDosageItem
            {
                Component = "Increase Salt",
                DosageQty = OrendoCalcHelpers.CaluculateSaltLevel(model.GallonsOfWater, model.CurrentSalt, model.DesiredSalt).ToString(),
                QtyDescription = "lbs of Salt"
            });
            res.ProductsInfo = OrendoCalcHelpers.CalcOrendaProductsVolumes(model.GallonsOfWater, (model.CurrentPhosphates - model.DesiredPhosphates), model.OrendaCalcType);
            //res.ProductsInfo = OrendoCalcHelpers.CalcOrendaProductsVolumes(model.GallonsOfWater, model.DesiredPhosphates);
            return res;
        }

        #endregion Api

    }
}