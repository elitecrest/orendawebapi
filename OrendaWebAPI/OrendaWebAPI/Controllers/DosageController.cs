﻿using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrendaWebAPI.Controllers
{
    public class DosageController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        [HttpPost]
        public CustomResponse DosageCalculation(DosageDetails dosage)
        {
            try
            {
                dosage.IncreaseCalcium = DosageModels.CalciumHardness(dosage.CalciumNow, dosage.CalciumTarget, dosage.Quantity);
                dosage.IncreaseSaltLevel = DosageModels.CalculateSaltLevel(dosage.SaltNow, dosage.SaltTarget, dosage.Quantity);
                dosage.DecreasepH = DosageModels.CalculatePhLevel(dosage.PhNow, dosage.PhTarget, dosage.Quantity);
                dosage.IncreaseAlkalinity = DosageModels.CalculateAlkalinity(dosage.AlkalinityNow, dosage.AlkalinityTarget, dosage.Quantity);
                double calHypo, bleach;
                DosageModels.CalculateChlorine(dosage.ChlorineNow, dosage.ChlorineTarget, dosage.Quantity, out calHypo, out bleach);
                dosage.IncreaseChlorineCalHypo = calHypo;
                dosage.IncreaseChlorineBleach = bleach;
                dosage.IncreaseStabilizer = DosageModels.CalculateStabilizer(dosage.StabilizerNow, dosage.StabilizerTarget, dosage.Quantity);
                dosage.CV600Purge = 320;
                dosage.CV600Maintenance = 150;
                dosage.SC1000Purge = 320;
                dosage.SC1000Maintenance = 150;
                dosage.PR1000PhosphateLevel = 320;
                if (dosage != null)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = dosage;
                    customResponse.Message = Constants.Dosage_calculation;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = dosage;
                    customResponse.Message = Constants.Dosage_Not_calculation;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
    }
}
