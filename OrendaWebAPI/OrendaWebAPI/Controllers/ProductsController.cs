﻿using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Elitecrest.HtmlExtractor.Models;
using Elitecrest.HtmlExtractor;
using System.Collections;
 

namespace OrendaWebAPI.Controllers
{
    public class ProductsController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();

        [HttpGet]
        public CustomResponse GetOrendaProducts(int language,int appId)
        {
            try
            {
                var response = ProductsModel.GetOrendaProducts(language,appId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetNaturalPoolProducts(int language, int appId)
        {
            try
            {
                var response = ProductsModel.GetNaturalPoolProducts(language, appId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetNextGenerationProducts(int language, int appId)
        {
            try
            {
                var response = ProductsModel.GetNextGenerationProducts(language, appId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetResources(int appId)
        {
            try
            {
                var response = ProductsModel.GetResources(appId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetProductsFromURL()
        {
            try
            {
                var OrendaProductsParser = new OrendaProductsParser(); 
                // act
                var orendaProducts = OrendaProductsParser.ParseProducts("http://orendatech.com/orenda-products/");
                var naturalProducts = OrendaProductsParser.ParseProducts("https://naturalpoolproducts.com/products/");
                var nextProducts = OrendaProductsParser.ParseProducts("https://nextgws.com/products/");

                foreach (var i in orendaProducts)
                {
                    var response = ProductsModel.AddProducts(i, 1);
                }
                foreach (var j in naturalProducts)
                {
                    var response1 = ProductsModel.AddProducts(j, 2);
                }
                foreach (var k in nextProducts)
                {
                    var response2 = ProductsModel.AddProducts(k, 3);
                }
              
                customResponse.Status = CustomResponseStatus.Successful;
                customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;


            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetResourcesV2(int appId,int LanguageId)
        {
            try
            {
                var response = ProductsModel.GetResourcesV2(appId,LanguageId);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
    }
}