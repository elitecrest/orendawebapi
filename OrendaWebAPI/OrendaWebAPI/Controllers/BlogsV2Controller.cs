﻿using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OrendaWebAPI.Controllers
{
    public class BlogsV2Controller : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        [HttpGet]
        public CustomResponse GetBlogsV2(int AppID)
        {
            try
            {
                var response = BlogsV2Model.GetBlogsV2(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Blogs_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Blogs_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
    }
}
