﻿using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace OrendaWebAPI.Controllers
{
    public class DeviceV2Controller : ApiController
    {
        CustomResponse customResponse = new CustomResponse();

        [System.Web.Http.HttpPost]
        public CustomResponse RegisterDeviceV2(DeviceInfoDTO deviceInfoDto)
        {

            try
            {
                int uId = 0;

                var currentDevice = DeviceV2Model.RegisterDeviceV2(deviceInfoDto);

                if (currentDevice != null)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.DEVICE_REGISTERED;
                    customResponse.Response = currentDevice;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                    customResponse.Message = Constants.DEVICE_NOT_REGISTERED;
                }

                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpPost]
        public CustomResponse FeatureTrackingV1_V2(FeatureTrackingListDto featureTrackingDto)
        {
            try
            {
                List<FeatureTrackingDto> featureTracking = featureTrackingDto.featureList;
                foreach (FeatureTrackingDto featuresDto in featureTracking)
                {
                    var response = DeviceV2Model.FeatureTrackingV2(featuresDto);
                    if (response > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = response;
                        customResponse.Message = Constants.FEATURE_SAVED;

                    }
                    else
                    {
                        customResponse.Status = CustomResponseStatus.UnSuccessful;
                        customResponse.Response = null;
                        customResponse.Message = Constants.FEATURE_NOT_SAVED;
                    }
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetDeviceInformationV2(int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetDeviceInfoV2(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetFeatureTrackingForBlogsV2(int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetFTForBlogs(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForBlogs_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForBlogs_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }
            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetFeatureTrackingForProductsV2(int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetFTForProductsV2(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForProducts_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForProducts_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpGet]
        public CustomResponse GetFeatureTrackingForVideosV2(int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetFTForVideosV2(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetDeviceCountByDateV2(DateTime FromDate, DateTime ToDate, int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetDeviceCountByDateV2(FromDate, ToDate, AppID);

                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.DeviceInfo_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }


        [HttpPost]
        public CustomResponse RegisterFirebaseDevice(DeviceInfoDTO deviceInfoDto)
        {

            try
            {
                int uId = 0;

                var currentDevice = DeviceV2Model.RegisterFirebaseDevice(deviceInfoDto);

                if (currentDevice != null)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Message = Constants.DEVICE_REGISTERED;
                    customResponse.Response = currentDevice;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                    customResponse.Message = Constants.DEVICE_NOT_REGISTERED;
                }

                return customResponse;
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
                return customResponse;// javaScriptSerializer.Serialize(Result);
            }
        }

        [HttpGet]
        public CustomResponse GetFeatureTrackingForVimeoVideos(int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetFeatureTrackingForVimeoVideos(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse GetFeatureTrackingForWistiaVideos(int AppID)
        {
            try
            {
                var response = DeviceV2Model.GetFeatureTrackingForWistiaVideos(AppID);
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.FTForVideos_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }
    }
}
