﻿using Elitecrest.HtmlExtractor.OrendaBlogsParser;
using OrendaWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Elitecrest.HtmlExtractor.Models;

namespace OrendaWebAPI.Controllers
{
    public class BlogsController : ApiController
    {
        CustomResponse customResponse = new CustomResponse();
        [HttpGet]
        public CustomResponse GetBlogs()
        {
            try
            {
                var response = BlogsModels.GetBlogs();
                if (response.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Blogs_Fetched;

                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Response = response;
                    customResponse.Message = Constants.Blogs_Not_Fetched;
                }
            }
            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        [HttpGet]
        public CustomResponse AddBlogs()
        {
            try
            {
                List<Blogs> blogList = new List<Blogs>();
                var OrendaBlogsParser = new OrendaBlogParser(new OrendaBlogPageParser());

                List<Elitecrest.HtmlExtractor.Models.OrendaBlogEntry> blogs = OrendaBlogsParser.Parse("http://orendatech.com/category/blog/");

                var dbBlogs = BlogsModels.GetBlogs();

                string firstPublishedDate = dbBlogs[0].PublishedDate;
                foreach (var i in blogs)
                {
                    //if (Convert.ToDateTime(firstPublishedDate) < Convert.ToDateTime(i.PublishedDate))
                    //{
                    var id = BlogsModels.AddBlogs(i);
                    if (id > 0)
                    {
                        customResponse.Status = CustomResponseStatus.Successful;
                        customResponse.Response = id;
                        customResponse.Message = Constants.Blogs_Added;

                    }
                    //}
                    else
                    {
                        break;
                    }

                }
                // }
                DeleteNonExistingBlogs(blogs, dbBlogs);
            }


            catch (Exception e)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Response = null;
                customResponse.Message = e.Message;
            }

            return customResponse;
        }

        private void DeleteNonExistingBlogs(List<Elitecrest.HtmlExtractor.Models.OrendaBlogEntry> blogs, List<Blogs> dbBlogs)
        {
            bool exists = true;
            foreach (var b in dbBlogs)
            {
                exists = blogs.Any(x => x.BlogName == b.BlogName);
                if (!exists)
                {
                    BlogsModels.DeleteBlog(b.BlogName);
                }
            }
        }

        [HttpGet]

        public CustomResponse GetFormula()

        {

            try

            {

                var response = BlogsModels.GetFormula();

                if (response.Count > 0)

                {

                    customResponse.Status = CustomResponseStatus.Successful;

                    customResponse.Response = response;

                    customResponse.Message = Constants.FORMULA_GET_SUCCESSFULLY;


                }

                else

                {

                    customResponse.Status = CustomResponseStatus.UnSuccessful;

                    customResponse.Response = response;

                    customResponse.Message = Constants.NO_RECORDS;

                }

            }

            catch (Exception e)

            {

                customResponse.Status = CustomResponseStatus.Exception;

                customResponse.Response = null;

                customResponse.Message = e.Message;

            }


            return customResponse;

        }

        [HttpGet]
        public CustomResponse GetBlogsBySearch(string searchText)
        {
            try
            {

                List<Blogs> Blogs = BlogsModels.GetBlogsBySearch(searchText);
                if (Blogs.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = Blogs;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }


        [HttpGet]
        public CustomResponse GetFAQs()
        {
            try
            {

                List<FAQDTO> faqs = BlogsModels.GetFAQs();
                if (faqs.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = faqs;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }


        [HttpGet]
        public CustomResponse GetBlogsBySearchNew(string searchText, int AppId, int LanguageId)
        {
            try
            {

                List<Blogs> Blogs = BlogsModels.GetBlogsBySearchNew(searchText, AppId, LanguageId);
                if (Blogs.Count > 0)
                {
                    customResponse.Status = CustomResponseStatus.Successful;
                    customResponse.Response = Blogs;
                    customResponse.Message = Constants.DETAILS_GET_SUCCESSFULLY;
                }
                else
                {
                    customResponse.Status = CustomResponseStatus.UnSuccessful;
                    customResponse.Message = Constants.NO_RECORDS;
                }
            }
            catch (Exception ex)
            {
                customResponse.Status = CustomResponseStatus.Exception;
                customResponse.Message = ex.Message;
            }
            return customResponse;
        }
    }
}