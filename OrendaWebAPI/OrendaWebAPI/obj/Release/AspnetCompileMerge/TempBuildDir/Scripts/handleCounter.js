/**
 * handle counter
 */
; (function () {
    'use strict';
    $.fn.handleCounter = function (options) {
        var $input,
            $btnMinus,
            $btnPlugs,
            minimum,
            counterValue,
            maximize,
            writable,
            onChange,
            onMinimum,
            accelerateCounterMinBoundary,
            accelerateCounterIncrement,
            onMaximize;
        var $handleCounter = this
        $btnMinus = $handleCounter.find('.counter-minus')
        $input = $handleCounter.find('input')
        $btnPlugs = $handleCounter.find('.counter-plus')
        var defaultOpts = {
            writable: true,
            minimum: 1,
            counterValue: 1,
            accelerateCounterMinBoundary:0,
            accelerateCounterIncrement:0,
            maximize: null,
            onChange: function () { },
            onMinimum: function () { },
            onMaximize: function () { }
        }
        var settings = $.extend({}, defaultOpts, options)
        minimum = settings.minimum
        maximize = settings.maximize
        writable = settings.writable
        onChange = settings.onChange
        onMinimum = settings.onMinimum
        onMaximize = settings.onMaximize
        accelerateCounterMinBoundary = settings.accelerateCounterMinBoundary
        accelerateCounterIncrement = settings.accelerateCounterIncrement
        counterValue = settings.counterValue
        if (!$.isNumeric(minimum)) {
            minimum = defaultOpts.minimum
        }
        if (!$.isNumeric(maximize)) {
            maximize = defaultOpts.maximize
        }
        var inputVal = $input.val()
        if (isNaN(parseFloat(inputVal))) {
            inputVal = $input.val(0).val()
        }
        if (!writable) {
            $input.prop('disabled', true)
        }

        changeVal(inputVal)
        $input.val(inputVal)
        $btnMinus.click(function () {
            var num = parseFloat($input.val())
            if (num > minimum) {
                var _counterVal = parseFloat(((accelerateCounterMinBoundary > 0 && num > accelerateCounterMinBoundary) ? accelerateCounterIncrement : counterValue));
                var newVal = (num - _counterVal);
                if (newVal.toString().indexOf(".") > 0)
                {
                    newVal = newVal.toFixed(1);
                }
                $input.val(newVal)
                changeVal(newVal)
            }
        })
        $btnPlugs.click(function () {
            var num = parseFloat($input.val())
            if (maximize === null || num < maximize) {
                var _counterVal = parseFloat(((accelerateCounterMinBoundary > 0 && num >= accelerateCounterMinBoundary) ? accelerateCounterIncrement : counterValue));
                var newVal = (num + _counterVal);
                if (newVal.toString().indexOf(".") > 0) {
                    newVal = newVal.toFixed(1);
                }
                $input.val(newVal)
                changeVal(newVal)
            }
        })
        var keyUpTime
        $input.keyup(function () {
            clearTimeout(keyUpTime)
            keyUpTime = setTimeout(function () {
                var num = $input.val()
                if (num === '') {
                    num = minimum
                    $input.val(minimum)
                }
                var reg = new RegExp("^[\\d]*$")
                if (isNaN(parseFloat(num)) || !reg.test(num)) {
                    $input.val($input.data('num'))
                    changeVal($input.data('num'))
                } else if (num < minimum) {
                    $input.val(minimum)
                    changeVal(minimum)
                } else if (maximize !== null && num > maximize) {
                    $input.val(maximize)
                    changeVal(maximize)
                } else {
                    changeVal(num)
                }
            }, 300)
        })
        $input.focus(function () {
            var num = $input.val()
            if (num === 0) $input.select()
        })

        function changeVal(num) {
            $input.data('num', num)
            $btnMinus.prop('disabled', false)
            $btnPlugs.prop('disabled', false)
            if (num <= minimum) {
                $btnMinus.prop('disabled', true)
                onMinimum.call(this, num)
            } else if (maximize !== null && num >= maximize) {
                $btnPlugs.prop('disabled', true)
                onMaximize.call(this, num)
            }
            onChange.call(this, num)
        }
        return $handleCounter
    };
})(jQuery)