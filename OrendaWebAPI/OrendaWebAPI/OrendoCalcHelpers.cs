﻿using OrendaWebAPI.Models.OrendaCalcModals;
using System.Collections.Generic;

namespace OrendaWebAPI
{
    public static class OrendoCalcHelpers
    {
        #region Caluculations

        /// <summary>
        /// Caluculates Calcium dosage
        /// </summary>
        /// <param name="waterVolume"></param>
        /// <param name="currentlevel"></param>
        /// <param name="desiredLevel"></param>
        /// <param name="byPounds"></param>
        /// <returns></returns>
        public static double CaluculateCalcium(int waterVolume, double currentlevel, double desiredLevel, bool byPounds)
        {
            double res = 0;
           // IF(Y > X, ((Y - X) * (WaterVolume * 4 * 4 * 8 * 29.572702 / 1000) / 1000) * (147.0154 / 100.0892) / 28.3495231, 0)
            //desiredLevel - Y , currentlevel -X
            if (desiredLevel > currentlevel)
            {
                //  res = .000195 * waterVolume * (desiredLevel - currentlevel);
                 res = ((desiredLevel - currentlevel) * (waterVolume * 4 * 4 * 8 * 29.572702 / 1000) / 1000) * (147.0154 / 100.0892) / 28.3495231;

            }

            if (byPounds)
            {
                res = res / 16;
            }

            return res;
        }

        /// <summary>
        /// Caluculates Alkalinity dosage
        /// </summary>
        /// <param name="waterVolume"></param>
        /// <param name="currentlevel"></param>
        /// <param name="desiredLevel"></param>
        /// <param name="byPounds"></param>
        /// <returns></returns>
        public static double CaluculateAlkalinity(int waterVolume, double currentlevel, double desiredLevel, bool byPounds)
        {
            double res = 0;

            if (currentlevel < desiredLevel)
            {
                // using bicorbonate
                res = .000235 * waterVolume * (desiredLevel - currentlevel);
                res = res / 16;
            }
            else if (currentlevel > desiredLevel)
            {
                // using acid
                res = .000256 * waterVolume * (currentlevel - desiredLevel);
            }
            //if (byPounds)
            //{
            //    res = res / 16;
            //}

            return res;
        }

        /// <summary>
        /// Caluculates Chlorine dosage
        /// </summary>
        /// <param name="waterVolume"></param>
        /// <param name="currentlevel"></param>
        /// <param name="desiredLevel"></param>
        /// <param name="byPounds"></param>
        /// <returns></returns>
        public static Dictionary<string, double> CaluculateChlorine(int waterVolume, double currentlevel, double desiredLevel)
        {
            var res = new Dictionary<string, double>();

            //if (currentlevel < desiredLevel)
            //{
             
                var hypores = .000205 * waterVolume * (desiredLevel - currentlevel);
                var bleachRes = .00105 * waterVolume * (desiredLevel - currentlevel);
            if(hypores < 0) { hypores = 0; }
            if(bleachRes < 0) { bleachRes = 0; }
                res.Add("oz 65% Cal Hypo (by weight) OR", hypores);
                res.Add("oz 12% Bleach (by volume)", bleachRes);
            //}

            return res;
        }

        /// <summary>
        /// Caluculates ph dosage
        /// </summary>
        /// <param name="waterVolume"></param>
        /// <param name="currentlevel"></param>
        /// <param name="desiredLevel"></param>
        /// <param name="byPounds"></param>
        /// <returns></returns>
        public static double CaluculatepH(int waterVolume, double currentlevel, double desiredLevel, double currentAlkalinity)
        {
            double res = 0;
            double sodaAsh = 0;

            if (currentlevel < desiredLevel)
            {
                if (currentAlkalinity <= 70)
                {
                    sodaAsh = GetSodaAshResult(waterVolume, currentlevel, desiredLevel, currentAlkalinity); //(0.000235 * waterVolume * (desiredLevel - currentlevel));
                }
                else
                {
                    sodaAsh = GetSodaAshResult2(waterVolume, currentlevel, desiredLevel, currentAlkalinity); //(0.000235 * waterVolume * (desiredLevel - currentlevel));
                }
                res = waterVolume * 0.0001;
                res = res * sodaAsh;
                return res; //  = res / 16;
            }
            else
                return (0.0003648 * (currentAlkalinity * 0.096) * waterVolume * (currentlevel - desiredLevel));


        }

        /// <summary>
        /// Caluculates salt level
        /// </summary>
        /// <param name="waterVolume"></param>
        /// <param name="currentlevel"></param>
        /// <param name="desiredLevel"></param>
        /// <param name="byPounds"></param>
        /// <returns></returns>
        public static double CaluculateSaltLevel(int waterVolume, int currentlevel, int desiredLevel)
        {
            double res = 0;

            if (currentlevel < desiredLevel)
            {
                res = (.00008363 * waterVolume * (desiredLevel - currentlevel)) / 10;
            }

            return res;
        }

        /// <summary>
        /// Caluculates Cyanuric Acid
        /// </summary>
        /// <param name="waterVolume"></param>
        /// <param name="currentlevel"></param>
        /// <param name="desiredLevel"></param>
        /// <param name="byPounds"></param>
        /// <returns></returns>
        public static double CaluculateCyanuricAcid(int waterVolume, int currentlevel, int desiredLevel, bool byPounds)
        {
            var res = 0.0;

            if (currentlevel < desiredLevel)
            {
                res = .00013349 * waterVolume * (desiredLevel - currentlevel);
            }

            if (byPounds)
            {
                res = res / 16;
            }

            return res;
        }

        /// <summary>
        /// Caluculates Orenda product dosages
        /// </summary>
        /// <param name="gallonsOfWater"></param>
        /// <returns></returns>
        public static List<OrendaCalcProduct> CalcOrendaProductsVolumes(double gallonsOfWater, double phasphateLevel,string OrendaCalcType)
        /// public static List<OrendaCalcProduct> CalcOrendaProductsVolumes(double gallonsOfWater, double phasphateLevel)
        {
            var sc1000PurgePerGallon = 0.0032;
            var sc1000BiWeeklyMaintenance = 0.0006;

            var sc1000WeeklyMaintenance = 0.0003;

            var cv600cv700PurgePerGallon = 0.0032;
            var cv600cv700WeeklyMaintenance = 0.0005;

            var cv600cv700WeeklyMaintenanceComm = 0.001;

            var CEPurgePerGallon = 0.0004;
            var CEWeeklyMaintenance = .0001;


            var products = new List<OrendaCalcProduct>();
            if (OrendaCalcType == "2")
            {
                // CV-600/CV-700 Enzyme
                products.Add(new OrendaCalcProduct
                {
                    ProductName = "CV-600/CV-700 Enzyme",
                    DosageItems = new List<OrendaCalcProductItemEntry> {
                    new OrendaCalcProductItemEntry {
                        Component="Purge",
                        Measurement="Ounces",
                        Qunantity=gallonsOfWater*cv600cv700PurgePerGallon
                    },
                    new OrendaCalcProductItemEntry {
                        Component="Weekly Maint.",
                        Measurement="Ounces",
                        Qunantity=gallonsOfWater*cv600cv700WeeklyMaintenance
                    }
                }
                });

            }
            else
            { // CV-600/CV-700 Enzyme
                products.Add(new OrendaCalcProduct
                {
                    ProductName = "CV-600/CV-700 Enzyme",
                    DosageItems = new List<OrendaCalcProductItemEntry> {
                    new OrendaCalcProductItemEntry {
                        Component="Purge",
                        Measurement="Ounces",
                        Qunantity=gallonsOfWater*cv600cv700PurgePerGallon
                    },
                    new OrendaCalcProductItemEntry {
                        Component="Weekly Maint.",
                        Measurement="Ounces",
                        Qunantity=gallonsOfWater*cv600cv700WeeklyMaintenanceComm
                    }
                }
                });
            }

            if (OrendaCalcType == "2")
            {
                // SC-1000
                products.Add(new OrendaCalcProduct
                {
                    ProductName = "SC-1000 Scale & Metal Control",
                    DosageItems = new List<OrendaCalcProductItemEntry> {
                    new OrendaCalcProductItemEntry {
                        Component="Purge",
                        Qunantity=sc1000PurgePerGallon*gallonsOfWater,
                        Measurement="Ounces"
                    },
                     new OrendaCalcProductItemEntry {
                        Component="Bi-Weekly Maint.",
                        Qunantity=sc1000BiWeeklyMaintenance*gallonsOfWater,
                        Measurement="Ounces"
                    }
                }
                });

            }
            else
            {   // SC-1000
                products.Add(new OrendaCalcProduct
                {
                    ProductName = "SC-1000 Scale & Metal Control",
                    DosageItems = new List<OrendaCalcProductItemEntry> {
                    new OrendaCalcProductItemEntry {
                        Component="Purge",
                        Qunantity=sc1000PurgePerGallon*gallonsOfWater,
                        Measurement="Ounces"
                    },
                     new OrendaCalcProductItemEntry {
                        Component="Weekly Maint.",
                        Qunantity= sc1000WeeklyMaintenance*gallonsOfWater,
                        Measurement="Ounces"
                    }
                }
                });
            }
            // Pr-10000
            products.Add(new OrendaCalcProduct
            {
                ProductName = "PR-10,000 Phosphate Remover",
                DosageItems = new List<OrendaCalcProductItemEntry> {
                    new OrendaCalcProductItemEntry {
                        Component="As needed",
                        Measurement="Ounces",
                        Qunantity=GetPhasphateRemoverDosing(gallonsOfWater,phasphateLevel,OrendaCalcType)
                    }
                }
            });

            // CE-Clarifier 
            products.Add(new OrendaCalcProduct
            {
                ProductName = "CE-Clarifier",
                DosageItems = new List<OrendaCalcProductItemEntry> {
                    new OrendaCalcProductItemEntry {
                        Component="Purge",
                        Measurement="Ounces",
                        Qunantity=CEPurgePerGallon*gallonsOfWater
                    },
                     new OrendaCalcProductItemEntry {
                        Component="Weekly Maint",
                        Measurement="Ounces",
                        Qunantity=CEWeeklyMaintenance*gallonsOfWater
                    }
                }
            });

            return products;
        }

        /// <summary>
        /// Caluculates phasphate removing dosage
        /// </summary>
        /// <param name="gallonsOfWater"></param>
        /// <param name="PhasphateLevel"></param>
        /// <returns></returns>
        private static double GetPhasphateRemoverDosing(double gallonsOfWater, double PhasphateLevel,string OrendaCalcType)
        {
            if (OrendaCalcType == "2")
            {
                if (PhasphateLevel >= 0 && PhasphateLevel <= 500)
                {
                    return gallonsOfWater * .0002;
                }
                else if (PhasphateLevel > 500 && PhasphateLevel <= 1000)
                {
                    return gallonsOfWater * .0004;
                }
                else if (PhasphateLevel >  1000 && PhasphateLevel <= 1500)
                {
                    return gallonsOfWater * .0006;
                }
                else if (PhasphateLevel >  1500 && PhasphateLevel <= 2000)
                {
                    return gallonsOfWater * .0008;
                }
                else if (PhasphateLevel > 2000 && PhasphateLevel <= 2500)
                {
                    return gallonsOfWater * .001;
                }
            }
            else
            if (OrendaCalcType == "1")
            {
                return gallonsOfWater * .0004;
            }
                return 0;
        }


        private static double GetSodaAshResult(int waterVolume, double PH1, double PH2, double currentAlkalinity)
        {
            double result = 0.0;


            if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 5.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 8.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 9.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 3.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 4.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 7.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 7.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 9.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 9.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 9.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 9.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 9.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 9.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 1.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 3.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 4.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 5.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 6.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 7.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 7.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 8.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 1.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 2.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 5.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 5.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 6.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 6.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 6.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 4.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 5.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 5.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 5.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 5.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 1.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 4.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 4.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 4.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 0.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 1.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 3.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 0.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 1.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 2.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 3.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 0.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 1.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 2.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 2.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 2.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 0.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 1;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 1.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 1.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 0.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 0.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 1.1;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 1.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 1.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 0.4;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 0.7;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 1;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 0.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 0.6;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 0.8;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 0.3;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 0.5;
            }
            else if (currentAlkalinity == 0 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 0.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 6.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 9.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 6;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 9.1;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 7.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 9;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 5.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 7.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 9.1;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 9.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 9.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 10 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 1.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 5.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 6.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 7.1;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 7.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 8.2;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 4.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 6.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 1.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 5.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 5.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 1.1;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 2;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 4.2;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 0.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 0.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 1.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 0.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 0.6;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 1;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 0.5;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 0.9;
            }
            else if (currentAlkalinity == 10 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 0.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 9.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 7.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 3.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 9;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 3;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 7.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 9.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 20 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 2.6;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 6.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 9.2;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 2.2;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 6.8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 7.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 9.1;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 9.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 9.8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 1.8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 3.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 5.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 6.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 7.1;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 7.6;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 5.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 6;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 1.3;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 5.1;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 1.1;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 2;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 3.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 0.9;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 2.9;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 0.8;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 0.7;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 1.3;
            }
            else if (currentAlkalinity == 20 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 0.6;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 5.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 9.6;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 7.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 30 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 7.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 8.8;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 6;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 9.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 5;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 7;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 7.7;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 3.1;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 4.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 5.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 6.6;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 1.4;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 2.6;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 5.1;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 2.2;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 3;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 1;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 0.9;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 30 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 0.8;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 4.8;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 8.8;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 40 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 4.1;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 7.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 3.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 8.8;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 2.9;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 5.4;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 7.4;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 9;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 2.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 6.2;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 7.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 9.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 8.1;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 5.4;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 6.2;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 1.1;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 40 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 1;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 9.8;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 7.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 6.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 5.6;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 50 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 4.8;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 8.9;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 4.1;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 7.6;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 3.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 8.8;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 2.9;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 5.3;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 8.9;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 7.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 9.6;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 2;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 7.2;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 1.3;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 2.5;
            }
            else if (currentAlkalinity == 50 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 1.2;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 8.7;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 7.6;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 6.5;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 60 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 5.6;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 8.7;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 7.4;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 3.4;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 6.2;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 7.1;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 8.7;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 4.3;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 6;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 7.4;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 2;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 5.1;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 6.4;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 1.5;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 60 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 1.4;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 64;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 70;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 70;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 70;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 7.4;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 70 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 5.4;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 7;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 9.6;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 5.9;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 8.1;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 4.9;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 6.8;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 9.7;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 4.2;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 5.8;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 7.2;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 5.1;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 3.2;
            }
            else if (currentAlkalinity == 70 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 1.6;
            }
           

            return result;

        }
        private static double GetSodaAshResult2(int waterVolume, double PH1, double PH2, double currentAlkalinity)
        {
            double result = 0.0;
              if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 71;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 77;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 78;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 78;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 78;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 76;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 64;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 66;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 66;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 66;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 8.3;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 80 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 7.1;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 6;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 5.1;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 9.4;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 4.3;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 7.9;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 6.6;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 9;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 3;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 7.6;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 9.4;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 2.5;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 6.5;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 8.1;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 2.2;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 5.7;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 80 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 1.7;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 82;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 85;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 87;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 87;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 85;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 64;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 71;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 73;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 73;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 9.2;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 90 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 7.8;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 5.6;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 8.7;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 7.2;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 9;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 2.4;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 4.5;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 90 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 1.9;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 75;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 81;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 87;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 90;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 93;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 94;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 93;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 70;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 77;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 81;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 64;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 66;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 100 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 6.2;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 9.5;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 8;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 3.7;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 9.2;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 3.1;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 5.7;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 7.9;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 9.8;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 2.6;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 4.9;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 6.9;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 100 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 2.1;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 73;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 82;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 89;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 94;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 98;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 101;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 103;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 104;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 103;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 103;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 101;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 63;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 71;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 76;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 81;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 84;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 88;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 88;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 87;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 73;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 110 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 9.4;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 8;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 5.7;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 4.7;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 8.7;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 4;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 6.2;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 2.9;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 5.3;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 7.5;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 2.5;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 4.8;
            }
            else if (currentAlkalinity == 110 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 2.3;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 88;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 96;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 102;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 106;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 109;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 111;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 112;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 112;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 111;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 109;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 76;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 83;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 87;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 91;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 93;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 93;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 66;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 71;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 75;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 77;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 63;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 120 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 8.6;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 7.3;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 6.1;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 5.1;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 9.4;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 4.3;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 7.9;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 6.7;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 9.3;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 3.1;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 5.8;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 8.1;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 5.2;
            }
            else if (currentAlkalinity == 120 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 2.5;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 73;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 85;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 103;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 109;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 114;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 117;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 120;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 120;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 120;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 119;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 117;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 63;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 82;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 89;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 94;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 98;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 100;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 102;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 102;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 102;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 100;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 63;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 70;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 76;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 83;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 85;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 85;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 38;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 70;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 60;
            }
            else if (currentAlkalinity == 130 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 48;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 9.2;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 40;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 7.8;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 28;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 6.6;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 20;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 4.6;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 8.5;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 3.9;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 7.2;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 6.2;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 8.8;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 2.9;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 5.5;
            }
            else if (currentAlkalinity == 130 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 2.7;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 24;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 78;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 91;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 101;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 110;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 117;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 122;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 126;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 128;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 129;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 129;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 127;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 125;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 79;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 88;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 101;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 105;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 107;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 109;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 109;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 109;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 107;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 68;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 75;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 81;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 86;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 89;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 91;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 92;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 92;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 91;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 64;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 73;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 75;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 77;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 77;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 77;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 59;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 61;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 63;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 64;
            }
            else if (currentAlkalinity == 140 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 30;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 42;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 51;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 9.9;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 8.4;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 35;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 7;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 5.9;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 21;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 4.9;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 9;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 15;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 18;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 4.2;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 7.7;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 3.6;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 6.6;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 9.4;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 3.1;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 5.9;
            }
            else if (currentAlkalinity == 140 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 2.8;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 6.6)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 6.7)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 6.8)
            {
                return result = 66;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 6.9)
            {
                return result = 83;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.0)
            {
                return result = 96;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.1)
            {
                return result = 108;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.2)
            {
                return result = 117;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.3)
            {
                return result = 125;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.4)
            {
                return result = 130;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.5)
            {
                return result = 134;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.6)
            {
                return result = 136;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.7)
            {
                return result = 137;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.8)
            {
                return result = 137;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 7.9)
            {
                return result = 136;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.5 && PH2 == 8.0)
            {
                return result = 134;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 6.7)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 6.8)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 6.9)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.0)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.1)
            {
                return result = 84;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.2)
            {
                return result = 93;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.3)
            {
                return result = 101;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.4)
            {
                return result = 107;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.5)
            {
                return result = 111;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.6)
            {
                return result = 114;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.7)
            {
                return result = 116;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.8)
            {
                return result = 116;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 7.9)
            {
                return result = 116;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.6 && PH2 == 8.0)
            {
                return result = 114;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 6.8)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 6.9)
            {
                return result = 36;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.0)
            {
                return result = 50;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.1)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.2)
            {
                return result = 72;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.3)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.4)
            {
                return result = 87;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.5)
            {
                return result = 91;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.6)
            {
                return result = 95;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.7)
            {
                return result = 97;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.8)
            {
                return result = 98;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 7.9)
            {
                return result = 98;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.7 && PH2 == 8.0)
            {
                return result = 97;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 6.9)
            {
                return result = 17;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.0)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.1)
            {
                return result = 43;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.2)
            {
                return result = 54;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.3)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.4)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.5)
            {
                return result = 74;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.6)
            {
                return result = 78;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.7)
            {
                return result = 80;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.8)
            {
                return result = 82;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 7.9)
            {
                return result = 82;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.8 && PH2 == 8.0)
            {
                return result = 82;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.1)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.2)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.3)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.4)
            {
                return result = 53;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.5)
            {
                return result = 58;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.6)
            {
                return result = 62;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.7)
            {
                return result = 65;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.8)
            {
                return result = 67;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 7.9)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 150 && PH1 == 6.9 && PH2 == 8.0)
            {
                return result = 69;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.1)
            {
                return result = 12;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.2)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.3)
            {
                return result = 32;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.4)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.5)
            {
                return result = 45;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.6)
            {
                return result = 49;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.7)
            {
                return result = 52;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.8)
            {
                return result = 55;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 7.9)
            {
                return result = 56;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.0 && PH2 == 8.0)
            {
                return result = 57;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.2)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.3)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.4)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.5)
            {
                return result = 33;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.6)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.7)
            {
                return result = 41;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.8)
            {
                return result = 44;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 7.9)
            {
                return result = 46;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.1 && PH2 == 8.0)
            {
                return result = 47;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.3)
            {
                return result = 8.9;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.4)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.5)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.6)
            {
                return result = 27;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.7)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.8)
            {
                return result = 34;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 7.9)
            {
                return result = 37;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.2 && PH2 == 8.0)
            {
                return result = 39;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 7.4)
            {
                return result = 7.5;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 7.5)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 7.6)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 7.7)
            {
                return result = 23;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 7.8)
            {
                return result = 26;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 7.9)
            {
                return result = 29;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.3 && PH2 == 8.0)
            {
                return result = 31;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.4 && PH2 == 7.5)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.4 && PH2 == 7.6)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.4 && PH2 == 7.7)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.4 && PH2 == 7.8)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.4 && PH2 == 7.9)
            {
                return result = 22;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.4 && PH2 == 8.0)
            {
                return result = 25;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.5 && PH2 == 7.6)
            {
                return result = 5.3;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.5 && PH2 == 7.7)
            {
                return result = 9.6;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.5 && PH2 == 7.8)
            {
                return result = 13;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.5 && PH2 == 7.9)
            {
                return result = 16;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.5 && PH2 == 8.0)
            {
                return result = 19;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.6 && PH2 == 7.7)
            {
                return result = 4.4;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.6 && PH2 == 7.8)
            {
                return result = 8.2;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.6 && PH2 == 7.9)
            {
                return result = 11;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.6 && PH2 == 8.0)
            {
                return result = 14;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.7 && PH2 == 7.8)
            {
                return result = 3.8;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.7 && PH2 == 7.9)
            {
                return result = 7.1;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.7 && PH2 == 8.0)
            {
                return result = 10;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.8 && PH2 == 7.9)
            {
                return result = 3.3;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.8 && PH2 == 8.0)
            {
                return result = 6.3;
            }
            else if (currentAlkalinity == 150 && PH1 == 7.9 && PH2 == 8.0)
            {
                return result = 3;
            }
            return result;
        }

        #endregion Caluculations
    }
}