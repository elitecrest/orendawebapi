﻿
$(document).ready(function () {

    $('#orendatxtgallonsofwater').on('keyup', (function () {
        var x = $(this).val();
        $(this).val(x.toString().replace(/,/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    }));

    changeSwithOption($('#chkorendoCalcType'));

    $('#chkorendoCalcType').click(function () {
        changeSwithOption(this);
    });


    function changeSwithOption(element) {
        if ($(element).attr('checked') === 'checked') {
            $('#hdnchkorendoCalcType').val(1);
            $(element).removeAttr('checked');
            $('#commercialCalcType').css('margin-left', '3px');
            applySwithStyles($('#commercialCalcType'), $('#resedentialCalcType'));
        }
        else {
            // resedentialCalcType
            $(element).attr('checked', 'checked');
            $('#hdnchkorendoCalcType').val(2);
            $('#resedentialCalcType').css('margin-right', '3px');
            applySwithStyles($('#resedentialCalcType'), $('#commercialCalcType'));
        }
    }

    function applySwithStyles(active, inactive) {
        active.css('border-radius', '20px');
        active.css('background-color', 'white');
        active.css('transition', '.4s');
        active.css('color', '#336699');


        inactive.css('border-radius', '');
        inactive.css('background-color', '');
        inactive.css('transition', '');
        inactive.css('color', 'white');
    }

    $('#btnGetDosage').click(function () {
        var Currentalkanity = $('#orendatxtcurrentalkalinity').val();
        var desiredalkanity = $('#orendatxtdesiredalkalinity').val();
        if (Currentalkanity > 150) {
            var r = confirm('Lower Alkalinity (150 or below) before raising pH');
        }
        if ((r == true && Currentalkanity > 150) || Currentalkanity <= 150) {
            var model = {
                "GallonsOfWater": $('#orendatxtgallonsofwater').val().replace(/,/g, ''),
                "CurrentTemparature": $('#orendatxtcurrenttemparature').val(),
                "DesiredTemparature": $('#orendatxtdesiredtemparature').val(),
                "CurrentChlorine": $('#orendatxtcurrentchlorine').val(),
                "DesiredChlorine": $('#orendatxtdesiredchlorine').val(),
                "CurrentpH": parseFloat($('#orendatxtcurrentpH').val()).toFixed(1),
                "DesiredpH": parseFloat($('#orendatxtdesiredtpH').val()).toFixed(1),
                "CurrentCalcium": $('#orendatxtcurrentcalcium').val(),
                "DesiredCalcium": $('#orendatxtdesiredcalcium').val(),
                "CurrentAlkalinity": $('#orendatxtcurrentalkalinity').val(),
                "DesiredAlkalinity": $('#orendatxtdesiredalkalinity').val(),
                "CurrentStabilizer": $('#orendatxtcurrentstabilizer').val(),
                "DesiredStabilizer": $('#orendatxtdesiredstabilizer').val(),
                "CurrentSalt": $('#orendatxtcurrentsalttds').val(),
                "DesiredSalt": $('#orendatxtdesiredsalttds').val(),
                "CurrentPhosphates": $('#orendatxtcurrentphosphates').val(),
                "DesiredPhosphates": $('#orendatxtdesiredphosphates').val(),
                "OrendaCalcType": $('#hdnchkorendoCalcType').val(),
            };

            if (model.GallonsOfWater == null || model.GallonsOfWater == "") {
                alert("Please enter quantity");
                return false;
            }
            // checked - commercial, unchecked - residential


            $.post('/OrendoCaluculator/CaluculateResult', model, function (res) {

                if (res !== null) {
                    //alert(model.OrendaCalcType);
                    var dosages = '';
                    if (res.DosageItems !== null) {
                        $.each(res.DosageItems, function (i, e) {
                            if (e.DosageQty !== "0") {
                                dosages += '<tr> <td class="orendoresulttabletdFirstColumn">' + e.Component + '</td> <td class="orendoresulttabletdSecondColumn">' + e.DosageQty.substr(0, 5) + '</td> <td class="orendoresulttabletdThirdColumn">' + e.QtyDescription + '</td> </tr>';
                                //alert(dosages);
                            }
                        });
                        $('#orendaCalcResult').html(dosages);
                        $('#orendaResultsWrapper').removeAttr('hidden');
                    }

                    if (res.ProductsInfo !== null) {
                        var products = '';
                        $.each(res.ProductsInfo, function (i, e) {
                            products += '<table id="' + e.ProductName + 'Table" class="productResultTable"> <thead> <tr> <td colspan="3"> ' + e.ProductName + ' </td> </tr> </thead> <tbody>';
                            $.each(e.DosageItems, function (ii, ee) {
                                products += '<tr> <td class="orendoresulttabletdFirstColumn">' + ee.Component + '</td> <td class="orendoresulttabletdSecondColumn">' + ee.Qunantity.toFixed(2) + '</td> <td class="orendoresulttabletdThirdColumn">' + ee.Measurement + '</td> </tr>';
                                //alert(products);
                            });
                            products += '</tbody> </table>';
                        });
                        products += '<table class="productResultTable"><tr> <td colspan="3"> * This dose does not account for the pH influence of chlorine added. </td></tr><tr><td colspan="3">';
                        products += '**Both pH and Alkalinity are affected when using acid, bicarb, or soda ash. Increasing pH dose can be off for large adjustment. </td></tr></table>';
                        $('#orendaDosageDetailsTablesWrapper').html(products);
                    }

                }
           
            });


            $('#orendaCalc').attr('hidden', true);
            var lsiCurrent = $('#btnlsiCurrent');
            var lsiDesired = $('#btnlsiDesired');
        }
     
 
        
    });

    $('#actualTemparature').handleCounter({
        minimum: 32,
        maximize: 104,
    });
    $('#actualpH').handleCounter({
        counterValue: .1,
        minimum: 6.5,
        maximize: 8.5,
    });
    $('#actualChlorine').handleCounter({
        counterValue: .5,
        maximize: 10.0,
    });
    $('#actualCalcium').handleCounter({
        counterValue: 10,
        maximize: 1200,
    });
    $('#actualAlkalinity').handleCounter({
        counterValue: 10,
        maximize: 400,
    });
    $('#actualStabilizer').handleCounter({
        counterValue: 10,
        maximize: 600,
        accelerateCounterMinBoundary: 200,
        accelerateCounterIncrement: 50
    });
    $('#actualSaltTds').handleCounter({
        counterValue: 100,
        maximize: 10000,
        accelerateCounterMinBoundary: 4000,
        accelerateCounterIncrement: 500
    });
    $('#actualPhosphates').handleCounter({
        counterValue: 100,
        maximize: 2500,
    });

    $('#desiredTemparature').handleCounter({
        minimum: 32,
        maximize: 104,
    });

    $('#desiredpH').handleCounter({
        counterValue: .1,
        minimum: 6.5,
        maximize: 8.0,

    });
    $('#desiredChlorine').handleCounter({
        counterValue: .5,
        maximize: 10.0,
    });
    $('#desiredCalcium').handleCounter({
        counterValue: 10,
        maximize: 1200,
    });
    $('#desiredAlkalinity').handleCounter({
        counterValue: 10,
        maximize: 400,
    });
    $('#desiredStabilizer').handleCounter({
        counterValue: 10,
        maximize: 600,
        accelerateCounterMinBoundary: 200,
        accelerateCounterIncrement: 50
    });
    $('#desiredSaltTds').handleCounter({
        counterValue: 100,
        maximize: 10000,
        accelerateCounterMinBoundary: 4000,
        accelerateCounterIncrement: 500
    });
    $('#desiredPhosphates').handleCounter({
        counterValue: 100,
        maximize: 2500,
        
    });

    $('#btnBack').click(function () {
        $('#orendaResultsWrapper').attr('hidden', true);
        $('#orendaCalc').removeAttr('hidden');
    });

    /// LSI calc actual
    $('.actualTempvalues').click(function () { 

        if ($('#orendatxtdesiredtemparature').val() !== $('#orendatxtcurrenttemparature').val()) {
            $('#orendatxtdesiredtemparature').val($('#orendatxtcurrenttemparature').val());
            $('.desiredvalues').click();
        }
        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrenttemparature'), $('#orendatxtdesiredtemparature'));
    });
    $('.actualChlorinevalues').click(function () { 
        if ($('#orendatxtdesiredchlorine').val() !== $('#orendatxtcurrentchlorine').val()) {
            $('#orendatxtdesiredchlorine').val($('#orendatxtcurrentchlorine').val());
            $('.desiredvalues').click();
        }
        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentchlorine'), $('#orendatxtdesiredchlorine'));
    });
    $('.actualpHvalues').click(function () {
        if ($('#orendatxtdesiredtpH').val() !== $('#orendatxtcurrentpH').val()) {
            if ($('#orendatxtdesiredtpH').val() <= 7.9) {
                $('#orendatxtdesiredtpH').val($('#orendatxtcurrentpH').val());
            }
            else if ($('#orendatxtcurrentpH').val() <= 8)
            {
                $('#orendatxtdesiredtpH').val($('#orendatxtcurrentpH').val());
            }
            $('.desiredvalues').click();
        }
        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentpH'), $('#orendatxtdesiredtpH'));
    });
    $('.actualCalciumvalues').click(function () {
        if ($('#orendatxtdesiredcalcium').val() !== $('#orendatxtcurrentcalcium').val()) {
            $('#orendatxtdesiredcalcium').val($('#orendatxtcurrentcalcium').val());
            $('.desiredvalues').click();
        }

        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentcalcium'), $('#orendatxtdesiredcalcium'));
    });
    $('.actualAlkalinityvalues').click(function () {

        if ($('#orendatxtdesiredalkalinity').val() !== $('#orendatxtcurrentalkalinity').val()) {
            $('#orendatxtdesiredalkalinity').val($('#orendatxtcurrentalkalinity').val());
            $('.desiredvalues').click();
        }

        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentalkalinity'), $('#orendatxtdesiredalkalinity'));
    });
    $('.actualStabilizervalues').click(function () {
        if ($('#orendatxtdesiredstabilizer').val() !== $('#orendatxtcurrentstabilizer').val()) {
            $('#orendatxtdesiredstabilizer').val($('#orendatxtcurrentstabilizer').val());
            $('.desiredvalues').click();
        }

        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentstabilizer'), $('#orendatxtdesiredstabilizer'));

    });
    $('.actualSalttdsvalues').click(function () { 
        if ($('#orendatxtdesiredsalttds').val() !== $('#orendatxtcurrentsalttds').val()) {
            $('#orendatxtdesiredsalttds').val($('#orendatxtcurrentsalttds').val());
            $('.desiredvalues').click();
        }

        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentsalttds'), $('#orendatxtdesiredsalttds'));
    });
    $('.actualPhosphatesvalues').click(function () {
        if ($('#orendatxtdesiredphosphates').val() !== $('#orendatxtcurrentphosphates').val()) {
            $('#orendatxtdesiredphosphates').val($('#orendatxtcurrentphosphates').val());
            $('.desiredvalues').click();
        }

        var lsi = calcLsi($('#orendatxtcurrenttemparature').val(), $('#orendatxtcurrentpH').val(), $('#orendatxtcurrentalkalinity').val(), $('#orendatxtcurrentcalcium').val(), $('#orendatxtcurrentstabilizer').val(), $('#orendatxtcurrentsalttds').val());
        changeLsiButtonColor($('#btnlsiCurrent'), lsi.toFixed(2));
        $('#btnlsiCurrent').val(lsi.toFixed(2));

        disableDesiredActinCommands($('#orendatxtcurrentphosphates'), $('#orendatxtdesiredphosphates'));
    }); 
        

    /// LSI calc desired
    $('.desiredvalues').click(function () {
        var lsi = calcLsi($('#orendatxtdesiredtemparature').val(), $('#orendatxtdesiredtpH').val(), $('#orendatxtdesiredalkalinity').val(), $('#orendatxtdesiredcalcium').val(), $('#orendatxtdesiredstabilizer').val(), $('#orendatxtdesiredsalttds').val()).toFixed(2);
        changeLsiButtonColor($('#btnlsiDesired'), lsi);
        $('#btnlsiDesired').val(lsi);
    });

    function disableDesiredActinCommands(actualIdentifier, desiredIDentifier) {

        var actualElementMinusBtn = ($(actualIdentifier).siblings()[0]);
        var actualElementPlusBtn = ($(actualIdentifier).siblings()[1]);
     
        $(desiredIDentifier).siblings()[0].disabled = actualElementMinusBtn.disabled;
        $(desiredIDentifier).siblings()[1].disabled = actualElementPlusBtn.disabled;
    }

    $.fn.digits = function () {
        return this.each(function () {
            $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        })
    }
    //Bindu
    function getStabilizerValue(stabilizerVal) {
        if (stabilizerVal < 200) {
            return 10;
        }
        else {
            return 20;
        }

    }
    //Bindu
    function getSaltValue(saltVal) {
        if (saltVal < 5000) {
            return 100;
        }
        else {
            return 500;
        }

    }

    function calcLsi(temp, ph, alkalinity, calcium, cyanuric, tdssalt) {
        var tempVal = -0.0000005 * (temp * temp * temp) + 0.00006 * (temp * temp) + 0.0117 * temp - 0.4116;
        //alert(tempVal);
        var pHVal = parseFloat(ph).toFixed(1);
        // alert(pHVal);
        var alkalinityval = getalkalinityLsiVal(pHVal, cyanuric, alkalinity);
        // alert(alkalinityval);
        var calciumVal = (calcium == 0) ? 0 : (0.4341 * Math.log(calcium) - 0.3926);
        // alert(calciumVal);
        var saltVal = getSaltLsiVal(tdssalt);
        //alert(saltVal);
        var res = (parseFloat(tempVal) + parseFloat(pHVal) + parseFloat(alkalinityval) + parseFloat(calciumVal));
        // alert(res);
        return (parseFloat(res) - parseFloat(saltVal));
    }

    function getalkalinityLsiVal(pHVal, cyanuric, alkalinity) {
        var resalkalinity;
        var randomNum;
        if (pHVal == 6.5)
            randomNum = 0.11;
        else if (pHVal == 6.6)
            randomNum = 0.132;
        else if (pHVal == 6.7)
            randomNum = 0.154;
        else if (pHVal == 6.8)
            randomNum = 0.176;
        else if (pHVal == 6.9)
            randomNum = 0.198;
        else if (pHVal == 7.0)
            randomNum = 0.22;
        else if (pHVal == 7.1)
            randomNum = 0.24;
        else if (pHVal == 7.2)
            randomNum = 0.26;
        else if (pHVal == 7.3)
            randomNum = 0.28;
        else if (pHVal == 7.4)
            randomNum = 0.30;
        else if (pHVal == 7.5)
            randomNum = 0.315;
        else if (pHVal == 7.6)
            randomNum = 0.33;
        else if (pHVal == 7.7)
            randomNum = 0.34;
        else if (pHVal == 7.8)
            randomNum = 0.35;
        else if (pHVal == 7.9)
            randomNum = 0.535;
        else if (pHVal == 8.0)
            randomNum = 0.36;
        else if (pHVal == 8.1)
            randomNum = 0.36333;
        else if (pHVal == 8.2)
            randomNum = 0.366667;
        else if (pHVal == 8.3)
            randomNum = 0.37;
        else if (pHVal == 8.4)
            randomNum = 0.373333;
        else if (pHVal == 8.5)
            randomNum = 0.38;

        cyanuric = cyanuric * randomNum;

        var diff = alkalinity - cyanuric;
        var logb;
        if (diff <= 0) {
            resalkalinity = 0;
        }
        else {
            logb = Math.log(diff);
            resalkalinity = 0.4341 * logb - 0.0074;
        }

        return resalkalinity;
    }



    function getSaltLsiVal(salt) {
        if (salt <= 1000)
            return 12.1;
        else if (salt > 1000 && salt < 1100)
            return 12.19;
        else if (salt >= 1100 && salt < 1200)
            return 12.199;
        else if (salt >= 1200 && salt < 1300)
            return 12.208;
        else if (salt >= 1300 && salt < 1400)
            return 12.217;
        else if (salt >= 1400 && salt < 1500)
            return 12.226;
        else if (salt >= 1500 && salt < 1600)
            return 12.235;
        else if (salt >= 1600 && salt < 1700)
            return 12.244;
        else if (salt >= 1700 && salt < 1800)
            return 12.253;
        else if (salt >= 1800 && salt < 1900)
            return 12.262;
        else if (salt >= 1900 && salt < 2000)
            return 12.271;
        else if (salt >= 2000 && salt < 2100)
            return 12.29;
        else if (salt >= 2100 && salt < 2200)
            return 12.3;
        else if (salt >= 2200 && salt < 2300)
            return 12.3;
        else if (salt >= 2300 && salt < 2400)
            return 12.31;
        else if (salt >= 2400 && salt < 2500)
            return 12.31;
        else if (salt >= 2500 && salt < 2600)
            return 12.32;
        else if (salt >= 2600 && salt < 2700)
            return 12.33;
        else if (salt >= 2700 && salt < 2800)
            return 12.33;
        else if (salt >= 2800 && salt < 2900)
            return 12.34;
        else if (salt >= 2900 && salt < 3000)
            return 12.34;
        else if (salt >= 3000 && salt < 3100)
            return 12.35;
        else if (salt >= 3100 && salt < 3200)
            return 12.36;
        else if (salt >= 3200 && salt < 3300)
            return 12.36;
        else if (salt >= 3300 && salt < 3400)
            return 12.37;
        else if (salt >= 3400 && salt < 3500)
            return 12.37;
        else if (salt >= 3500 && salt < 3600)
            return 12.38;
        else if (salt >= 3600 && salt < 3700)
            return 12.39;
        else if (salt >= 3700 && salt < 3800)
            return 12.39;
        else if (salt >= 3800 && salt < 3900)
            return 12.40;
        else if (salt >= 3900 && salt < 4500)
            return 12.4;
        else if (salt >= 4500)
            return 12.41;
    }

    function changeLsiButtonColor(btn, lsi) {
        if (lsi < -0.30)
        { 
            btn.removeClass('lsiButtonGreen');
            btn.removeClass('lsiButtonPurple');
            btn.removeClass('lsiButtonYellow');
            btn.addClass('lsiButtonRed');
        }
        else if (lsi > 0.30) {
            
            btn.removeClass('lsiButtonGreen');
            btn.removeClass('lsiButtonRed');
            btn.removeClass('lsiButtonYellow');
            btn.addClass('lsiButtonPurple');
        }
        else if (lsi >= 0.00 && lsi <= 0.30) { 
            btn.removeClass('lsiButtonRed');
            btn.removeClass('lsiButtonPurple');
            btn.removeClass('lsiButtonYellow');
            btn.addClass('lsiButtonGreen');
        }
        else if (lsi >= -0.30 && lsi <= -0.01) {
            
            btn.removeClass('lsiButtonRed');
            btn.removeClass('lsiButtonPurple');
            btn.removeClass('lsiButtonGreen');
            btn.addClass('lsiButtonYellow');
        }

    }
});
 