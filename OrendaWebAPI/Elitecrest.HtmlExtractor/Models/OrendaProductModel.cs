﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elitecrest.HtmlExtractor.Models
{
    /// <summary>
    /// Model class for Orenda product
    /// </summary>
    public class OrendaProductModel
    {
        public int ProductId { get; set; }
        /// <summary>
        /// Product Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Product Image Url
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Product Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ProductUrl
        /// </summary>
        public string ProductUrl { get; set; }

        /// <summary>
        /// VideoUrl
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// SDSSHeet
        /// </summary>
        public string SDSSHeet { get; set; }

        /// <summary>
        /// ShortDesc
        /// </summary>
        public string ShortDesc { get; set; }

        /// <summary>
        /// Feature
        /// </summary>
        public string Feature { get; set; }

      
    }
}
