﻿namespace Elitecrest.HtmlExtractor.Models
{
    /// <summary>
    /// Model class for Orenda Blog
    /// </summary>
    public class OrendaBlogEntry
    {
        /// <summary>
        /// Blog Name
        /// </summary>
        public string BlogName { get; set; }

        /// <summary>
        /// Blog Description
        /// </summary>
        public string BlogDescription { get; set; }

        /// <summary>
        /// Blog url
        /// </summary>
        public string BlogUrl { get; set; }

        /// <summary>
        /// Thumbnail Url
        /// </summary>
        public string ThumbNailURL { get; set; }

        /// <summary>
        /// Published Date
        /// </summary>
        public string PublishedDate { get; set; }
    }
}
