﻿using Elitecrest.HtmlExtractor.Common;
using Elitecrest.HtmlExtractor.Models;
using HtmlAgilityPack;
using System.Collections.Generic;

namespace Elitecrest.HtmlExtractor.OrendaBlogsParser
{
    /// <summary>
    /// Parser to parse blogs given page
    /// </summary>
    public class OrendaBlogPageParser : IWebsiteParser
    {
        /// <summary>
        /// Parses all blogs in given page
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public override dynamic Parse(string url)
        {
            List<OrendaBlogEntry> blogs = new List<OrendaBlogEntry>();

            var html = DownloadContentFromUrl(url);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            HtmlNodeCollection nodeCollection = doc.DocumentNode.SelectNodes("//div[@class='avia-content-slider-inner']");

            // iterate through each article
            var entries = nodeCollection[0].SelectNodes("//article");

            for (int articleIterator = 0; articleIterator < entries.Count; articleIterator++)
            {
                var article = entries[articleIterator];

                var anchor = article.ChildNodes[0];
                var _entry = new OrendaBlogEntry
                {
                    BlogDescription = entries[articleIterator].InnerText,
                    BlogName = article.ChildNodes[1].ChildNodes[0].ChildNodes[0].InnerText,
                    BlogUrl = anchor.Attributes["href"].Value,
                    PublishedDate = article.ChildNodes[1].ChildNodes[1].ChildNodes[2].InnerText,
                    ThumbNailURL = anchor.ChildNodes[0].Attributes["src"].Value
                };
                blogs.Add(_entry);
            }

            return blogs;
        }
    }
}
