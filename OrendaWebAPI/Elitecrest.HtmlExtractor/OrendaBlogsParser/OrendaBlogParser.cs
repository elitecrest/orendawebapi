﻿using Elitecrest.HtmlExtractor.Common;
using Elitecrest.HtmlExtractor.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;

namespace Elitecrest.HtmlExtractor.OrendaBlogsParser
{
    /// <summary>
    /// Parser class for OrendaBlogs
    /// </summary>
    public class OrendaBlogParser : IWebsiteParser
    {

        #region Fields

        /// <summary>
        /// Blog page url
        /// </summary>
        private string pageUrl = "http://orendatech.com/category/blog/page/{0}/";

        /// <summary>
        /// Parser for entire blog page
        /// </summary>
        private readonly IWebsiteParser blogPageParser;

        #endregion Fields

        #region Constructors

        /// <summary>
        /// Initializes a new instance of <see cref="OrendaBlogParser"/> class.
        /// </summary>
        /// <param name="blogPageParser"></param>
        public OrendaBlogParser(IWebsiteParser blogPageParser)
        {
            this.blogPageParser = blogPageParser;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Downloads and Parses content from given url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public override dynamic Parse(string url)
        {
            List<OrendaBlogEntry> blogs = new List<OrendaBlogEntry>();

            // parse main page and get total number of pages
            var html = DownloadContentFromUrl(url);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

           // HtmlNodeCollection nodeCollection = doc.DocumentNode.SelectNodes("//nav[@class='pagination']//a");

            //  int pageCount = nodeCollection == null ? 1 : nodeCollection.Count + 1;
            var pageCount = this.GetPageCount(doc);
            for (int iterator = 1; iterator <= pageCount; iterator++)
            {
                // get all blogs and add to blogs collection
                blogs.AddRange(this.blogPageParser.Parse(string.Format(pageUrl, iterator)));
            }

            return blogs;
        }
        private int GetPageCount(HtmlDocument doc)
        {
            // pagination-meta method
            HtmlNodeCollection nodeCollection = doc.DocumentNode.SelectNodes("//nav[@class='pagination']//span[@class='pagination-meta']");
            if (nodeCollection != null && nodeCollection.Count > 0 && this.ValidateMetaText(nodeCollection[0].InnerHtml))
            {
                var innerHtml = nodeCollection[0].InnerHtml;
                var pCount = innerHtml.Substring(innerHtml.IndexOf("of") + 2);
                return Convert.ToInt32(pCount.Trim(' '));
            }
            else
            {
                nodeCollection = doc.DocumentNode.SelectNodes("//nav[@class='pagination']//a");
            }

            int pageCount = nodeCollection == null ? 1 : nodeCollection.Count + 1;

            return pageCount;
        }

        private bool ValidateMetaText(string text)
        {
            if (text == null || text == string.Empty)
                return false;
            else if (text.Contains("of"))
            {
                return true;
            }

            return false;
        }
        #endregion Methods
    }
}
