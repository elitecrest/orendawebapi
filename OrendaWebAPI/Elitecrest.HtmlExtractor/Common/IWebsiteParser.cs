﻿using System.Net;

namespace Elitecrest.HtmlExtractor.Common
{
    /// <summary>
    /// Contract for website parser 
    /// </summary>
    public abstract class IWebsiteParser
    {
        #region Fields
        /// <summary>
        /// WebClient Instance
        /// </summary>
        protected static WebClient webClient = new WebClient();

        #endregion Fields

        #region Methods

        /// <summary>
        /// Downloads and Parses content from given url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public abstract dynamic Parse(string url);

        /// <summary>
        /// Downloads Html content from given Url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string DownloadContentFromUrl(string url)
        {
            return webClient.DownloadString(url);
        }

        #endregion Methods
    }
}
