﻿using Elitecrest.HtmlExtractor.Common;
using Elitecrest.HtmlExtractor.Models;
using System.Collections.Generic;

namespace Elitecrest.HtmlExtractor
{
    /// <summary>
    /// Orenda Products Parser
    /// </summary>
    public class OrendaProductsParser
    {
        /// <summary>
        /// ProductsList parser reference
        /// </summary>
        private readonly IWebsiteParser productsListParser;

        /// <summary>
        /// Products parser reference
        /// </summary>
        private readonly IWebsiteParser productDetailParser;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of <see cref="OrendaProductsListParser"/> class
        /// </summary>
        /// <param name="parser"></param>
        public OrendaProductsParser(IWebsiteParser productsListparser, IWebsiteParser productDetailParser)
        {
            this.productsListParser = productsListparser;
            this.productDetailParser = productDetailParser;
        }

        /// <summary>
        /// Initializes a new instance of <see cref="OrendaProductsListParser"/> class
        /// </summary>
        public OrendaProductsParser() : this(new OrendaProductsListParser(), new OrendaProductDetailsParser())
        {

        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Parses all products from given url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public List<OrendaProductModel> ParseProducts(string url)
        {
            var products = new List<OrendaProductModel>();

            var productUrls = this.productsListParser.Parse(url);

            foreach (var producturl in productUrls)
            {
                products.Add(this.productDetailParser.Parse(producturl));
            }

            return products;
        }

        #endregion Methods
    }
}
