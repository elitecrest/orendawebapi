﻿using Elitecrest.HtmlExtractor.Common;
using HtmlAgilityPack;
using System.Collections.Generic;

namespace Elitecrest.HtmlExtractor
{
    /// <summary>
    /// Orendo ProductsList parser
    /// </summary>
    public class OrendaProductsListParser : IWebsiteParser
    {
        #region Methods

        /// <summary>
        /// Downloads and Parses content from given url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public override dynamic Parse(string url)
        {
            List<string> productUrls = new List<string>();

            var html = DownloadContentFromUrl(url);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            HtmlNodeCollection nodeCollection = doc.DocumentNode.SelectNodes("//ul[@class='nested_nav']//li");

            foreach (var a in nodeCollection)
            {
                productUrls.Add(a.FirstChild.Attributes["href"].Value);
            }
            return productUrls;
        }

        #endregion Methods
    }
}
