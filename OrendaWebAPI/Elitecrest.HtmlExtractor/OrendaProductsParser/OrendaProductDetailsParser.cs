﻿using System;
using Elitecrest.HtmlExtractor.Common;
using System.Net;
using System.Collections.Generic;
using HtmlAgilityPack;
using Elitecrest.HtmlExtractor.Models;

namespace Elitecrest.HtmlExtractor
{
    /// <summary>
    /// Orendo ProductsList parser
    /// </summary>
    public class OrendaProductDetailsParser : IWebsiteParser
    {
        #region Methods

        /// <summary>
        /// Downloads and Parses product info from given url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public override dynamic Parse(string url)
        {
            OrendaProductModel productInfo = new OrendaProductModel();

            var html = DownloadContentFromUrl(url);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            // parse product name
            HtmlNodeCollection nodeCollection = doc.DocumentNode.SelectNodes("//h1[@class='main-title entry-title']");

            // TODO : HTML DECODE all values
            productInfo.Name = nodeCollection[0].FirstChild.InnerHtml;

            // parse product image
            var imageNode = doc.DocumentNode.SelectNodes("//img[@id='Image']");

            //if (null == imageNode)
            //{
            //    imageNode = doc.DocumentNode.SelectNodes("//header//a//img");
            //}
            productInfo.ImageUrl = imageNode[0].Attributes["src"].Value;

            // product description
            HtmlNodeCollection descriptionDiv = doc.DocumentNode.SelectNodes("//div[@id='Description']");
            productInfo.Description = descriptionDiv[0].InnerHtml;
            return productInfo;
        }

        #endregion Methods
    }
}
