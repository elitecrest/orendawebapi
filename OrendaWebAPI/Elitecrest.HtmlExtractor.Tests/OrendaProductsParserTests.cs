﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Elitecrest.HtmlExtractor;

namespace Elitecrest.HtmlExtractor.Tests
{
    [TestClass]
    public class OrendaProductsParserTests
    {
        [TestMethod]
        public void ParseProductsTest()
        {
            // arrange
            var OrendaProductsParser = new OrendaProductsParser();

            // act
            var products = OrendaProductsParser.ParseProducts("http://orendatech.com/orenda-products/");

            //assert
            Assert.AreNotEqual(0, products.Count);

            Assert.IsNotNull(products[0].Name);
            Assert.IsNotNull(products[1].Name);
            Assert.IsNotNull(products[2].Name);
            Assert.IsNotNull(products[3].Name);
            Assert.IsNotNull(products[4].Name);

            Assert.IsNotNull(products[0].Description);
            Assert.IsNotNull(products[1].Description);
            Assert.IsNotNull(products[2].Description);
            Assert.IsNotNull(products[3].Description);
            Assert.IsNotNull(products[4].Description);

            Assert.IsNotNull(products[0].ImageUrl);
            Assert.IsNotNull(products[1].ImageUrl);
            Assert.IsNotNull(products[2].ImageUrl);
            Assert.IsNotNull(products[3].ImageUrl);
            Assert.IsNotNull(products[4].ImageUrl);

        }
    }
}
