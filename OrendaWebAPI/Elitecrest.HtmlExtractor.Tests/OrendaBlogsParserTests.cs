﻿
using Elitecrest.HtmlExtractor.OrendaBlogsParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Elitecrest.HtmlExtractor.Tests
{
    [TestClass]
    /// <summary>
    /// Test class for <see cref="OrendaBlogsParser"/>
    /// </summary>
    public class OrendaBlogsParserTests
    {
        /// <summary>
        /// Test method to parse blogs
        /// </summary>
        [TestMethod]
        public void ParseBlogsTest()
        {
            // arrange
            var OrendaBlogsParser = new OrendaBlogParser(new OrendaBlogPageParser());

            // act
            var products = OrendaBlogsParser.Parse("http://orendatech.com/category/blog/");

            //assert ,TODO : assertions needs to be improved

           // Assert.IsNotNull(products);
            //Assert.IsNotNull(products[0].BlogName);
            //Assert.IsNotNull(products[0].BlogDescription);
            //Assert.IsNotNull(products[0].BlogUrl);
            //Assert.IsNotNull(products[0].ThumbNailURL);
        }
    }
}
