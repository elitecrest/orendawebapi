﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="OrendaWorkerRole" generation="1" functional="0" release="0" Id="277a4077-6812-4770-97d8-0f6f1a1b90bf" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="OrendaWorkerRoleGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="OrendaRole:APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="">
          <maps>
            <mapMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/MapOrendaRole:APPINSIGHTS_INSTRUMENTATIONKEY" />
          </maps>
        </aCS>
        <aCS name="OrendaRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/MapOrendaRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="OrendaRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/MapOrendaRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <maps>
        <map name="MapOrendaRole:APPINSIGHTS_INSTRUMENTATIONKEY" kind="Identity">
          <setting>
            <aCSMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/OrendaRole/APPINSIGHTS_INSTRUMENTATIONKEY" />
          </setting>
        </map>
        <map name="MapOrendaRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/OrendaRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapOrendaRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/OrendaRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="OrendaRole" generation="1" functional="0" release="0" software="E:\WorkingCopies\Orenda_WEBAPI_250118\OrendaWebAPI\OrendaWorkerRole\csx\Release\roles\OrendaRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;OrendaRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;OrendaRole&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/OrendaRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/OrendaRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/OrendaWorkerRole/OrendaWorkerRoleGroup/OrendaRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="OrendaRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="OrendaRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="OrendaRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>