using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using System.Data.SqlClient;
using System.Configuration;
using Google.Apis.YouTube.v3;
using Google.Apis.Services;
using OrendaWebAPI.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace OrendaRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);
        //private string connectionURL = "http://OrendaStagingApi.azurewebsites.net/";
        private string connectionURL = "http://orendaapitest.azurewebsites.net/";

        internal static SqlConnection ConnectTODB()
        {
            string connectionString = "Server=tcp:orendadb.database.windows.net,1433;Database=orendaprod;User ID=orenda;Password=elite@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            //string connectionString = "Server=tcp:mvb1q2vzna.database.windows.net,1433;Database=OrendaStaging;User ID=orendatest;Password=copy@123;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        public override void Run()
        {
            Trace.TraceInformation("OrendaRole is running");

            try
            {
                while (true)
                {
                    DeleteExistingVideos();
                    GetYouTubeVideosandadding();
                    DeleteExistingVideosInCustomApps();
                    GetYouTubeVideosandaddingForCustomApps();
                    GetVimeoVideos();
                    GetBlogs();
                    GetWistiaVideos();
                    Thread.Sleep(TimeSpan.FromDays(10));
                    Trace.TraceInformation("Working", "Information");
                }
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }
        private void GetYouTubeVideosandadding()
        {
            HttpClient client = new HttpClient();  
            string baseURL = connectionURL;
            client.BaseAddress = new Uri(baseURL);
            string Api = "/Videos/GetYouTubeVideos";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }
        }
        private void GetWistiaVideos()
        {
            HttpClient client = new HttpClient();
            string baseURL = connectionURL;
            client.BaseAddress = new Uri(baseURL);
            string Api = "/VideosV2/GetWistiaFromOrendaV2";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }
        }

        private void GetYouTubeVideosandaddingForCustomApps()
        {
            HttpClient client = new HttpClient();
            string baseURL = connectionURL;
            client.BaseAddress = new Uri(baseURL);
            string Api = "/VideosV2/GetYouTubeVideosV2";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }
        }

        private void GetVimeoVideos()
        {
            HttpClient client = new HttpClient();
            string baseURL = connectionURL;
            client.BaseAddress = new Uri(baseURL);
            string Api = "/VideosV2/GetVimeoVideosFromOrendauser";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }
        }

        //private void GetYouTubeVideos()
        //{
        //    int result = 0;
        //    try
        //    {
        //        var yt = new YouTubeService(new BaseClientService.Initializer() { ApiKey = "AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo" });
        //        var channelsListRequest = yt.Channels.List("contentDetails");
        //        channelsListRequest.ForUsername = "OrendaTechnologies";
        //        var channelsListResponse = channelsListRequest.Execute();
        //        int VideoCount = 1;
        //        foreach (var channel in channelsListResponse.Items)
        //        {
        //            var uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;
        //            var nextPageToken = "";
        //            while (nextPageToken != null)
        //            {
        //                var playlistItemsListRequest = yt.PlaylistItems.List("snippet");
        //                playlistItemsListRequest.PlaylistId = uploadsListId;
        //                playlistItemsListRequest.MaxResults = 50;
        //                playlistItemsListRequest.PageToken = nextPageToken;
        //                // Retrieve the list of videos uploaded to the authenticated user's channel.  
        //                var playlistItemsListResponse = playlistItemsListRequest.Execute();
        //                List<YouTubeChannelDetails> youtube = new List<YouTubeChannelDetails>();
        //                foreach (var playlistItem in playlistItemsListResponse.Items)
        //                {
        //                    YouTubeChannelDetails video = new YouTubeChannelDetails();
        //                    video.VideoId =  playlistItem.Snippet.ResourceId.VideoId;
        //                    video.VideoTitle = playlistItem.Snippet.Title;
        //                    video.VideoDescription = playlistItem.Snippet.Description;
        //                    video.ChannelId = playlistItem.Snippet.ChannelId;
        //                    video.ChannelTitle = playlistItem.Snippet.ChannelTitle;
        //                    video.VideoUrl =  "https://www.youtube.com/embed/" + playlistItem.Snippet.ResourceId.VideoId;
        //                    video.ThumbNailUrl = playlistItem.Snippet.Thumbnails.Medium.Url.ToString();
        //                    video.PublishedDate = Convert.ToDateTime(playlistItem.Snippet.PublishedAt.ToString());
        //                    youtube.Add(video);
        //                }
        //                if(youtube.Count > 0)
        //                {
        //                    foreach (var uTube in youtube)
        //                    {
        //                        SqlConnection sqlConnection = ConnectTODB();
        //                        SqlCommand sqlCommand = new SqlCommand("[dbo].[GetYouTubeVideos]", sqlConnection);
        //                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
        //                        SqlParameter vID = sqlCommand.Parameters.Add("@VideoId", System.Data.SqlDbType.VarChar, 100);
        //                        vID.Direction = System.Data.ParameterDirection.Input;
        //                        vID.Value = uTube.VideoId.Trim();
        //                        SqlParameter vTitle = sqlCommand.Parameters.Add("@VideoTitle", System.Data.SqlDbType.VarChar, 100);
        //                        vTitle.Direction = System.Data.ParameterDirection.Input;
        //                        vTitle.Value = uTube.VideoTitle.Trim();
        //                        SqlParameter vDescription = sqlCommand.Parameters.Add("@VideoDescription", System.Data.SqlDbType.VarChar, 1024);
        //                        vDescription.Direction = System.Data.ParameterDirection.Input;
        //                        vDescription.Value = uTube.VideoDescription.Trim();
        //                        SqlParameter vURL = sqlCommand.Parameters.Add("@VideoUrl", System.Data.SqlDbType.VarChar, 1024);
        //                        vURL.Direction = System.Data.ParameterDirection.Input;
        //                        vURL.Value = uTube.VideoUrl.Trim();
        //                        SqlParameter vThumbNail = sqlCommand.Parameters.Add("@VideoThumbNail", System.Data.SqlDbType.VarChar, 1024);
        //                        vThumbNail.Direction = System.Data.ParameterDirection.Input;
        //                        vThumbNail.Value = uTube.ThumbNailUrl.Trim();
        //                        SqlParameter pDate = sqlCommand.Parameters.Add("@PublishedDate", System.Data.SqlDbType.DateTime);
        //                        pDate.Direction = System.Data.ParameterDirection.Input;
        //                        pDate.Value = uTube.PublishedDate;
        //                        SqlParameter channelId = sqlCommand.Parameters.Add("@ChannelId", System.Data.SqlDbType.VarChar, 256);
        //                        channelId.Direction = System.Data.ParameterDirection.Input;
        //                        channelId.Value = uTube.ChannelId.Trim();
        //                        SqlParameter channelDesc = sqlCommand.Parameters.Add("@ChannelDescription", System.Data.SqlDbType.VarChar, 1024);
        //                        channelDesc.Direction = System.Data.ParameterDirection.Input;
        //                        channelDesc.Value = uTube.ChannelTitle.Trim();
        //                        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
        //                        while (sqlDataReader != null && sqlDataReader.Read())
        //                        {
        //                            result = sqlDataReader["DeviceId"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["result"]);
        //                        }
        //                    }
        //                }
        //                //nextPageToken = playlistItemsListResponse.NextPageToken;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Some exception occured" + e);
        //    }
        //}

        public void DeleteExistingVideos()
        {
            int result = 0;
            SqlConnection sqlConnection = ConnectTODB();
            SqlCommand sqlCommand = new SqlCommand("[dbo].[DeleteAllVideos]", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader != null && sqlDataReader.Read())
            {
                result = sqlDataReader["DeletedCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeletedCount"]);
            }
        }

        public void DeleteExistingVideosInCustomApps()
        {
            int result = 0;
            SqlConnection sqlConnection = ConnectTODB();
            SqlCommand sqlCommand = new SqlCommand("[dbo].[DeleteAllCustomAppVideos]", sqlConnection);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader != null && sqlDataReader.Read())
            {
                result = sqlDataReader["DeletedCount"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeletedCount"]);
            }
        }

        private void GetBlogs()
        {
            HttpClient client = new HttpClient();
            string baseURL = connectionURL;
            client.BaseAddress = new Uri(baseURL);
            string Api = "/Blogs/AddBlogs";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("OrendaRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("OrendaRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("OrendaRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
