﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OrendaWebAPI.Controllers;
using OrendaWebAPI.Models;

namespace OrendaWebAPI.Tests.Controllers
{
    /// <summary>
    /// Summary description for DeviceControllerTests
    /// </summary>
    [TestClass]
    public class DeviceControllerTests
    {
        public DeviceControllerTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetDeviceInfo()
        {
            DeviceController deviceCtrl = new DeviceController();
            var devices = deviceCtrl.GetDeviceInformation();

            Assert.IsNotNull(devices);
        }

        [TestMethod]
        public void GetFTForVideos()
        {
            DeviceController deviceCtrl = new DeviceController();
            var ftVs = deviceCtrl.GetFeatureTrackingForVideos();

            Assert.IsNotNull(ftVs);
        }

        [TestMethod]
        public void GetFTForBlogs()
        {
            DeviceController deviceCtrl = new DeviceController();
            var ftBs = deviceCtrl.GetFeatureTrackingForBlogs();

            Assert.IsNotNull(ftBs);
        }

        [TestMethod]
        public void GetFTForProducts()
        {
            DeviceController deviceCtrl = new DeviceController();
            var ftPs = deviceCtrl.GetFeatureTrackingForProducts();

            Assert.IsNotNull(ftPs);
        }

        [TestMethod]
        public void FeatureTracking()
        {
            DeviceController devCtrl = new DeviceController();
            List<FeatureTrackingDto> dtos = new List<FeatureTrackingDto>();
            FeatureTrackingDto ftdto = new FeatureTrackingDto();
            ftdto.FeatureId = 1002;
            ftdto.DeviceId = 58;
            ftdto.DeviceType = 1;
            ftdto.Value = "CV-700";
            ftdto.Count = 1;

            FeatureTrackingDto ftdto2 = new FeatureTrackingDto();
            ftdto2.FeatureId = 1002;
            ftdto2.DeviceId =3;
            ftdto2.DeviceType = 1;
            ftdto2.Value = "CV-800";
            ftdto2.Count = 1;
            dtos.Add(ftdto);
            dtos.Add(ftdto2);

            CustomResponse res =   devCtrl.FeatureTracking(dtos);

            Assert.AreEqual(res.Status, CustomResponseStatus.Successful);

        }
    }
}
